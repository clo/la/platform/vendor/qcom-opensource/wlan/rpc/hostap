#
# Copyright (C) 2008 The Android Open Source Project
#
# This software may be distributed, used, and modified under the terms of
# BSD license:
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name(s) of the above-listed copyright holder(s) nor the
#    names of its contributors may be used to endorse or promote products
#    derived from this software without specific prior written permission.
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

LOCAL_DIR_PATH:= $(call my-dir)

CONFIG_RPC_CERTIFICATE := n
CONFIG_RPC_VENDOR_AIDL := n

L_CFLAGS = -DCONFIG_RPC_SAE_PK
L_CFLAGS += -DCONFIG_RPC_DPP
L_CFLAGS += -DCONFIG_RPC_FILS
L_CFLAGS += -DCONFIG_RPC_WPS
L_CFLAGS += -DCONFIG_RPC_MBO

ifeq ($(CONFIG_RPC_CERTIFICATE), y)
L_CFLAGS += -DCONFIG_RPC_CERTIFICATE
endif

L_CPPFLAGS = -Wall -Werror
L_CPPFLAGS += -Wno-unused-function
L_CPPFLAGS += -Wno-unused-parameter
L_CPPFLAGS += -Wno-unused-variable

ifeq ($(ENABLE_SOMEIP), true)
include $(CLEAR_VARS)
LOCAL_PATH := $(LOCAL_DIR_PATH)
LOCAL_MODULE := libqti_supplicant_rpc_impl
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_VENDOR_MODULE := true
LOCAL_CFLAGS := $(L_CFLAGS)
LOCAL_CPPFLAGS := $(L_CPPFLAGS)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_SRC_FILES := \
    src/aidl_rpc_manager.cpp \
    src/sta_iface_rpc.cpp \
    src/sta_network_rpc.cpp \
    src/supplicant_rpc.cpp \
    src/supplicant_rpc_client.cpp \
    src/supplicant_rpc_event.cpp
LOCAL_SHARED_LIBRARIES := \
    android.hardware.wifi.supplicant-V2-ndk \
    libbinder_ndk \
    libbase \
    libcrypto \
    liblog \
    libqti_rpc_common_util \
    libqti_someip_util \
    libqti-wifi-supplicant-message \
    libssl \
    libutils \
    libvsomeip3
ifeq ($(CONFIG_RPC_CERTIFICATE), y)
LOCAL_SRC_FILES += \
    src/certificate_utils.cpp \
    src/supplicant_rpc_certificate.cpp
LOCAL_SHARED_LIBRARIES += \
    android.system.keystore2-V1-ndk
endif
ifeq ($(CONFIG_RPC_VENDOR_AIDL), y)
LOCAL_CFLAGS += -DCONFIG_RPC_VENDOR_AIDL
LOCAL_SRC_FILES += \
    src/supplicant_rpc_vendor.cpp \
    src/supplicant_rpc_vendor_event.cpp \
    src/vendor_sta_iface_rpc.cpp
LOCAL_SHARED_LIBRARIES += \
    libqti-supplicant-vendor-message \
    vendor.qti.hardware.wifi.supplicant-V2-ndk
endif
LOCAL_EXPORT_C_INCLUDE_DIRS := \
    $(LOCAL_PATH)/include
include $(BUILD_SHARED_LIBRARY)
endif
