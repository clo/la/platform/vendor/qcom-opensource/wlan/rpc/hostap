/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <non_standard_cert_msg.h>
#include <supplicant_message_def.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#include "supplicant_rpc_certificate.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_event.h"

namespace {

using aidl::android::hardware::wifi::supplicant::AidlRpcManager;

static void SupplicantRpcHanleGetCertificate(uint8_t* data, size_t length)
{
	if (!data || length < SUPPLICANT_EVENT_PAYLOAD_MIN_SIZE)
		return;

	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager)
		return;

	std::string alias;
	if (!NonStandardCertParseGetBlobReq(
		data + SUPPLICANT_EVENT_PAYLOAD_MIN_SIZE,
		length - SUPPLICANT_EVENT_PAYLOAD_MIN_SIZE, alias))
		return;

	if (auto cert = manager->getCertificate(alias.c_str())) {
		std::vector<uint8_t> payload;
		NonStandardCertSerializeGetBlobCfm(cert.value(), payload);
		SupplicantSendRequest(NON_STANDARD_CERT_GET_BLOB_REQ, payload);
	} else {
		ALOGE("Supplicant rpc get certificate fail");
	}
}

static void SupplicantRpcHanleListAliases(uint8_t* data, size_t length)
{
	if (!data || length < SUPPLICANT_EVENT_PAYLOAD_MIN_SIZE)
		return;

	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager)
		return;

	std::string prefix;
	if (!NonStandardCertParseListAliasesReq(
		data + SUPPLICANT_EVENT_PAYLOAD_MIN_SIZE,
		length - SUPPLICANT_EVENT_PAYLOAD_MIN_SIZE, prefix))
		return;

	if (auto results = manager->listAliases(prefix.c_str())) {
		std::vector<uint8_t> payload;
		NonStandardCertSerializeListAliasesCfm(results.value(),
			payload);
		SupplicantSendRequest(NON_STANDARD_CERT_LIST_ALIASES_REQ,
			payload);
	} else {
		ALOGE("Supplicant rpc list aliases fail");
	}
}
} // root namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

bool SupplicantRpcInitCertificateRequests()
{
	SupplicantRegisterEventHandler(
		NON_STANDARD_CERT_GET_BLOB_REQ + 0x8400,
		&SupplicantRpcHanleGetCertificate);
	SupplicantRegisterEventHandler(
		NON_STANDARD_CERT_LIST_ALIASES_REQ + 0x8400,
		&SupplicantRpcHanleListAliases);

	return true;
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
