/*
 * WPA Supplicant - Manager for Aidl interface objects
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <algorithm>
#include <functional>
#include <iostream>
#include <regex>

#include <android-base/logging.h>
#include <android/binder_process.h>
#include <android/binder_manager.h>
#include <aidl/android/hardware/wifi/supplicant/IpVersion.h>

#include <signal.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#ifdef CONFIG_RPC_CERTIFICATE
#include "certificate_utils.h"
#endif
#include "supplicant_rpc_client.h"

namespace {
/**
 * Creates a unique key for the network using the provided |ifname| and
 * |network_id| to be used in the internal map of |ISupplicantNetwork| objects.
 * This is of the form |ifname|_|network_id|. For ex: "wlan0_1".
 *
 * @param ifname Name of the corresponding interface.
 * @param network_id ID of the corresponding network.
 */
const std::string getNetworkObjectMapKey(
	const std::string &ifname, int network_id)
{
	return ifname + "_" + std::to_string(network_id);
}

template <class ObjectType>
int addAidlObjectToMap(const std::string &key,
	const std::shared_ptr<ObjectType> &object,
	std::map<const std::string, std::shared_ptr<ObjectType>> &object_map)
{
	// Return failure if we already have an object for that |key|.
	if (object_map.find(key) != object_map.end())
		return 1;
	object_map[key] = object;
	if (!object_map[key].get())
		return 1;
	return 0;
}

template <class ObjectType>
int removeAidlObjectFromMap(
	const std::string &key,
	std::map<const std::string, std::shared_ptr<ObjectType>> &object_map)
{
	// Return failure if we dont have an object for that |key|.
	const auto& object_iter = object_map.find(key);
	if (object_iter == object_map.end())
		return 1;
	object_iter->second->invalidate();
	object_map.erase(object_iter);
	return 0;
}

/**
 * Add callback to the corresponding list after linking to death on the
 * corresponding aidl object reference.
 */
template <class CallbackType>
int registerForDeathAndAddCallbackAidlObjectToList(
	AIBinder_DeathRecipient* death_notifier,
	const std::shared_ptr<CallbackType> &callback,
	std::vector<std::shared_ptr<CallbackType>> &callback_list)
{
	binder_status_t status = AIBinder_linkToDeath(callback->asBinder().get(),
		death_notifier, nullptr /* cookie */);
	if (status != STATUS_OK) {
		ALOGE("Error registering for death notification.");
		return 1;
	}

	callback_list.push_back(callback);
	return 0;
}

template <class CallbackType>
int addCallbackAidlObjectToMap(
	AIBinder_DeathRecipient* death_notifier,
	const std::string &name, const std::shared_ptr<CallbackType> &callback,
	std::map<const std::string,
		 std::vector<std::shared_ptr<CallbackType>>> &callbacks_map)
{
	if (name.empty())
		return 1;

	auto callback_map_iter = callbacks_map.find(name);
	if (callback_map_iter == callbacks_map.end())
		return 1;

	auto& callback_list = callback_map_iter->second;
	return registerForDeathAndAddCallbackAidlObjectToList<CallbackType>(
		death_notifier, callback, callback_list);
}

template <class CallbackType>
int removeAllCallbackAidlObjectsFromMap(
	AIBinder_DeathRecipient* death_notifier, const std::string &name,
	std::map<const std::string,
		std::vector<std::shared_ptr<CallbackType>>> &callbacks_map)
{
	if (name.empty())
		return 1;

	auto callback_map_iter = callbacks_map.find(name);
	if (callback_map_iter == callbacks_map.end())
		return 1;

	for (const auto& callback : callback_map_iter->second) {
		binder_status_t status = AIBinder_linkToDeath(
			callback->asBinder().get(), death_notifier, nullptr);
		if (status != STATUS_OK)
			ALOGE("Error deregistering for death "
				"notification for callback object");
	}

	callbacks_map.erase(callback_map_iter);

	return 0;
}

template <class CallbackType>
void removeCallbackAidlObjectFromMap(
	const std::string &name,
	const std::shared_ptr<CallbackType> &callback,
	std::map<const std::string,
		std::vector<std::shared_ptr<CallbackType>>> &callbacks_map)
{
	if (name.empty())
		return;

	auto callback_map_iter = callbacks_map.find(name);
	if (callback_map_iter == callbacks_map.end())
		return;

	auto& cb_list = callback_map_iter->second;
	cb_list.erase(
		std::remove(cb_list.begin(), cb_list.end(),callback),
		cb_list.end());
}

template <class CallbackType>
void callWithEachCallback(const std::string &name,
	const std::function<
		ndk::ScopedAStatus(std::shared_ptr<CallbackType>)> &method,
	const std::map<const std::string,
		std::vector<std::shared_ptr<CallbackType>>> &callbacks_map)
										
{
	if (name.empty())
		return;

	auto callback_map_iter = callbacks_map.find(name);
	if (callback_map_iter == callbacks_map.end())
		return;

	for (const auto& callback : callback_map_iter->second)
		if (!method(callback).isOk())
			ALOGE("Failed to invoke AIDL callback");
}

void onDeath(void* cookie) {
	ALOGI("Client died. Terminating...");
	raise(SIGTERM);
}

}  // namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

AidlRpcManager *AidlRpcManager::instance_ = NULL;

AidlRpcManager::AidlRpcManager() {}

AidlRpcManager *AidlRpcManager::getInstance()
{
	if (!instance_)
		instance_ = new AidlRpcManager();
	return instance_;
}

void AidlRpcManager::destroyInstance()
{
	if (instance_) {
		instance_->stopRpcClient();

		delete instance_;

		instance_ = NULL;
	}

	/* Call onDeath to terminate service */
	onDeath(NULL);
}

int AidlRpcManager::startRpcClient()
{
	ALOGI("RPC client starting");

	if (!SupplicantSomeipClientInit()) {
		ALOGE("RPC client init fail.");
		return -1;
	}

	if (!SupplicantSomeipClientStart()) {
		ALOGE("RPC client start fail.");
		return -1;
	}

	ALOGI("RPC client started");

	return 0;
}

void AidlRpcManager::stopRpcClient()
{
	ALOGI("RPC client stopping");

	SupplicantSomeipClientStop();
	SupplicantSomeipClientDeinit();

	ALOGI("RPC client stopped.");
}

int AidlRpcManager::registerAidlService()
{
	// Create the main aidl service object and register it.
	ALOGI("Starting AIDL supplicant rpc service");
	ALOGI("Interface version: %d", ISupplicant::version);

	supplicant_object_ = ndk::SharedRefBase::make<SupplicantRpc>();
	std::string instance =
		std::string() + ISupplicant::descriptor + "/default";
	if (AServiceManager_addService(
		supplicant_object_->asBinder().get(),
		instance.c_str()) != STATUS_OK)
		return 1;

	// Initialize the death notifier.
	death_notifier_ = AIBinder_DeathRecipient_new(onDeath);
	return 0;
}

#ifdef CONFIG_RPC_VENDOR_AIDL
int AidlRpcManager::registerVendorAidlService()
{
	// Create the main aidl service object and register it.
	ALOGI("Starting vendor AIDL supplicant rpc");
	supplicantvendor_object_ =
		ndk::SharedRefBase::make<SupplicantVendorRpc>();
	std::string instance =
		std::string() + ISupplicantVendor::descriptor + "/default";
	if (AServiceManager_addService(
		supplicantvendor_object_->asBinder().get(),
		instance.c_str()) != STATUS_OK)
		return 1;

	// Initialize the death notifier.
	death_notifier_ = AIBinder_DeathRecipient_new(onDeath);
	return 0;
}
#endif

int AidlRpcManager::registerInterface(IfaceType type,
	const std::string& ifname, int32_t instance_id)
{
#ifdef CONFIG_RPC_P2P
	if (type == IfaceType::P2P) {
		if (addAidlObjectToMap<P2pIface>(
			ifname,
			ndk::SharedRefBase::make<P2pIface>(ifname, instance_id),
			p2p_iface_object_map_)) {
			ALOGE("Failed to register P2P interface %s with "
				"AIDL control", ifname.c_str());
			return 1;
		}
		p2p_iface_callbacks_map_[ifname] =
			std::vector<
				std::shared_ptr<ISupplicantP2pIfaceCallback>>();

		goto out;
	}
#endif

	if (addAidlObjectToMap<StaIfaceRpc>(
		ifname,
		ndk::SharedRefBase::make<StaIfaceRpc>(ifname, instance_id),
		sta_iface_object_map_)) {
		ALOGE("Failed to register STA interface %s with AIDL control.",
			ifname.c_str());
		return 1;
	}
	sta_iface_callbacks_map_[ifname] =
		std::vector<std::shared_ptr<ISupplicantStaIfaceCallback>>();

#ifdef CONFIG_RPC_VENDOR_AIDL
	if (addAidlObjectToMap<VendorStaIfaceRpc>(
		ifname,
		ndk::SharedRefBase::make<VendorStaIfaceRpc>(ifname, instance_id),
		vendor_sta_iface_object_map_)) {
		ALOGE("Failed to register vendor STA interface %s"
			" with AIDL control", ifname.c_str());
		return 1;
	}
	vendor_sta_iface_callbacks_map_[ifname] =
		std::vector<
			std::shared_ptr<ISupplicantVendorStaIfaceCallback>>();
#endif

out:
	// Invoke the |onInterfaceCreated| method on all registered callbacks.
	callWithEachSupplicantCallback(
		std::bind(&ISupplicantCallback::onInterfaceCreated,
			  std::placeholders::_1, ifname));

	return 0;
}

int AidlRpcManager::unregisterInterface(IfaceType type,
	const std::string& ifname)
{
	int ret;

#ifdef CONFIG_RPC_P2P
	if (type == IfaceType::P2P) {
		ret = removeAidlObjectFromMap(ifname, p2p_iface_object_map_);
		if (!ret)
			ret = removeAllCallbackAidlObjectsFromMap(
				death_notifier_, ifname,
				p2p_iface_callbacks_map_);
		goto out;
	}
#endif
	ret = removeAidlObjectFromMap(ifname, sta_iface_object_map_);
	if (!ret)
		ret = removeAllCallbackAidlObjectsFromMap(
				death_notifier_, ifname,
				sta_iface_callbacks_map_);
#ifdef CONFIG_RPC_VENDOR_AIDL
	ret = removeAidlObjectFromMap(ifname, vendor_sta_iface_object_map_);
	if (!ret)
		ret = removeAllCallbackAidlObjectsFromMap(
			death_notifier_, ifname,
			vendor_sta_iface_callbacks_map_);
#endif

out:
	if (ret)
		ALOGE("Failed to unregister interface %s with AIDL control",
			ifname.c_str());

	// Invoke the |onInterfaceRemoved| method on all registered callbacks.
	callWithEachSupplicantCallback(
		std::bind(&ISupplicantCallback::onInterfaceRemoved,
			std::placeholders::_1, ifname));

	return ret;
}

int AidlRpcManager::registerNetwork(IfaceType type,
	const std::string& ifname, int32_t network_id)
{
	// Generate the key to be used to lookup the network.
	const std::string key = getNetworkObjectMapKey(ifname, network_id);

#ifdef CONFIG_RPC_P2P
	if (type == IfaceType::P2P) {
		if (addAidlObjectToMap<P2pNetwork>(key,
			ndk::SharedRefBase::make<P2pNetwork>(ifname, network_id),
			p2p_network_object_map_)) {
			ALOGE("Failed to register P2P network %s "
				"with AIDL control", key.c_str());
			return 1;
		}

		return 0;
	}
#endif

	if (addAidlObjectToMap<StaNetworkRpc>(key,
		ndk::SharedRefBase::make<StaNetworkRpc>(ifname, network_id),
		sta_network_object_map_)) {
		ALOGE("Failed to register STA network %s with AIDL control",
			key.c_str());
		return 1;
	}
	sta_network_callbacks_map_[key] =
		std::vector<std::shared_ptr<ISupplicantStaNetworkCallback>>();

	// Invoke the |onNetworkAdded| method on all registered callbacks.
	callWithEachStaIfaceCallback(ifname,
		std::bind(&ISupplicantStaIfaceCallback::onNetworkAdded,
			std::placeholders::_1, network_id));
	return 0;
}

int AidlRpcManager::unregisterNetwork(IfaceType type,
	const std::string& ifname, int32_t network_id)
{
	// Generate the key to be used to lookup the network.
	const std::string key = getNetworkObjectMapKey(ifname, network_id);

#ifdef CONFIG_RPC_P2P
	if (type == IfaceType::P2P) {
		if (removeAidlObjectFromMap(key, p2p_network_object_map_)) {
			ALOGE("Failed to unregister P2P network %s "
				"with AIDL control", key.c_str());
			return 1;
		}

		return 0;
	}
#endif

	if (removeAidlObjectFromMap(key, sta_network_object_map_)) {
		ALOGE("Failed to unregister STA network %s with AIDL control",
			key.c_str());
		return 1;
	}
	if (removeAllCallbackAidlObjectsFromMap(
		death_notifier_, key, sta_network_callbacks_map_))
		return 1;

	// Invoke the |onNetworkRemoved| method on all registered callbacks.
	callWithEachStaIfaceCallback(ifname,
		std::bind(&ISupplicantStaIfaceCallback::onNetworkRemoved,
			std::placeholders::_1, network_id));
	return 0;
}

int AidlRpcManager::unregisterAllNetworkOnInterface(IfaceType type,
	const std::string& ifname) {
	std::string prefix = ifname + "_";
	for (auto it = sta_network_object_map_.begin(); it != sta_network_object_map_.end();) {
		if (it->first.find(prefix) == 0) {
			std::string key = it->first;
			//remove key from sta_network_object_map_
			it->second->invalidate();
			it = sta_network_object_map_.erase(it);
			ALOGD("unregister STA network: %s", key.c_str());
			if (removeAllCallbackAidlObjectsFromMap(
				death_notifier_, key, sta_network_callbacks_map_)) {
				ALOGE("Failed to remove STA network: %s callback", key.c_str());
				return 1;
			}
		} else {
			++it;
		}
	}
	return 0;
}

void AidlRpcManager::callWithEachSupplicantCallback(
	const std::function<ndk::ScopedAStatus(
		std::shared_ptr<ISupplicantCallback>)> &method)
{
	for (const auto& callback : supplicant_callbacks_)
		if (!method(callback).isOk())
			ALOGE("Failed to invoke AIDL callback");
}

void AidlRpcManager::callWithEachStaIfaceCallback(const std::string &ifname,
	const std::function<ndk::ScopedAStatus(
		std::shared_ptr<ISupplicantStaIfaceCallback>)> &method)
{
	callWithEachCallback(ifname, method, sta_iface_callbacks_map_);
}

void AidlRpcManager::callWithEachStaNetworkCallback(const std::string &name,
	const std::function<::ndk::ScopedAStatus(
		std::shared_ptr<ISupplicantStaNetworkCallback>)> &method)
{
	callWithEachCallback(name, method, sta_network_callbacks_map_);
}

#ifdef CONFIG_RPC_P2P
void AidlRpcManager::callWithEachP2pIfaceCallback(const std::string &ifname,
	const std::function<ndk::ScopedAStatus(
		std::shared_ptr<ISupplicantP2pIfaceCallback>)> &method)
{
	callWithEachCallback(ifname, method, p2p_iface_callbacks_map_);
}

int AidlRpcManager::getP2pIfaceAidlObjectByIfname(
	const std::string &ifname,
	std::shared_ptr<ISupplicantP2pIface> *iface_object)
{
	if (ifname.empty() || !iface_object)
		return 1;

	auto iface_object_iter = p2p_iface_object_map_.find(ifname);
	if (iface_object_iter == p2p_iface_object_map_.end())
		return 1;

	*iface_object = iface_object_iter->second;
	return 0;
}

int AidlRpcManager::getP2pNetworkAidlObjectByIfnameAndNetworkId(
	const std::string &ifname, int32_t network_id,
	std::shared_ptr<ISupplicantP2pNetwork> *network_object)
{
	if (ifname.empty() || network_id < 0 || !network_object)
		return 1;

	const std::string key = getNetworkObjectMapKey(ifname, network_id);

	auto network_object_iter = p2p_network_object_map_.find(key);
	if (network_object_iter == p2p_network_object_map_.end())
		return 1;

	*network_object = network_object_iter->second;
	return 0;
}

int AidlRpcManager::addP2pIfaceCallbackAidlObject(
	const std::string &ifname,
	const std::shared_ptr<ISupplicantP2pIfaceCallback> &callback)
{
	return addCallbackAidlObjectToMap(
		death_notifier_, ifname, callback, p2p_iface_callbacks_map_);
}
#endif

const std::string AidlRpcManager::getStaIfaceNameByInstanceId(
	int32_t instance_id)
{
	for (const auto& [ifname, iface] : sta_iface_object_map_)
		if (iface->getInstanceId() == instance_id)
			return ifname;

	ALOGE("No sta iface found with instance id %d", instance_id);
	return "";
}

const std::string AidlRpcManager::getStaNetworkNameByInstanceId(
	int32_t sta_iface_id, int32_t network_id)
{
	const std::string ifname = getStaIfaceNameByInstanceId(sta_iface_id);
	return getNetworkObjectMapKey(ifname, network_id);
}

int AidlRpcManager::getStaIfaceAidlObjectByIfname(
	const std::string &ifname,
	std::shared_ptr<StaIfaceRpc> *iface_object)
{
	if (ifname.empty() || !iface_object)
		return 1;

	auto iface_object_iter = sta_iface_object_map_.find(ifname);
	if (iface_object_iter == sta_iface_object_map_.end())
		return 1;

	*iface_object = iface_object_iter->second;
	return 0;
}

int AidlRpcManager::getStaNetworkAidlObjectByIfnameAndNetworkId(
	const std::string &ifname, int32_t network_id,
	std::shared_ptr<StaNetworkRpc> *network_object)
{
	if (ifname.empty() || network_id < 0 || !network_object)
		return 1;

	// Generate the key to be used to lookup the network.
	const std::string key = getNetworkObjectMapKey(ifname, network_id);
	auto network_object_iter = sta_network_object_map_.find(key);
	if (network_object_iter == sta_network_object_map_.end())
		return 1;

	*network_object = network_object_iter->second;
	return 0;
}

int AidlRpcManager::addSupplicantCallbackAidlObject(
	const std::shared_ptr<ISupplicantCallback> &callback)
{
	return registerForDeathAndAddCallbackAidlObjectToList<
		ISupplicantCallback>(
			death_notifier_, callback, supplicant_callbacks_);
}

int AidlRpcManager::addStaIfaceCallbackAidlObject(
	const std::string &ifname,
	const std::shared_ptr<ISupplicantStaIfaceCallback> &callback)
{
	return addCallbackAidlObjectToMap(
		death_notifier_, ifname, callback, sta_iface_callbacks_map_);
}

int AidlRpcManager::addStaNetworkCallbackAidlObject(
	const std::string &ifname, int network_id,
	const std::shared_ptr<ISupplicantStaNetworkCallback> &callback)
{
	const std::string key = getNetworkObjectMapKey(ifname, network_id);

	return addCallbackAidlObjectToMap(
		death_notifier_, key, callback, sta_network_callbacks_map_);
}

int AidlRpcManager::registerNonStandardCertCallbackAidlObject(
	const std::shared_ptr<INonStandardCertCallback> &callback)
{
	if (callback == nullptr)
		return 1;

	non_standard_cert_callback_ = callback;
	return 0;
}

#ifdef CONFIG_RPC_CERTIFICATE
std::optional<std::vector<uint8_t>> AidlRpcManager::getCertificate(
	const char* alias)
{
	if (!alias) {
		ALOGE("Null pointer argument was passed to getCertificate");
		return std::nullopt;
	}

	return certificate_utils::getCertificate(alias,
		non_standard_cert_callback_);
}

std::optional<std::vector<std::string>> AidlRpcManager::listAliases(
	const char *prefix)
{
	if (!prefix) {
		ALOGE("Null pointer argument was passed to listAliases");
		return std::nullopt;
	}

	return certificate_utils::listAliases(prefix,
		non_standard_cert_callback_);
}
#endif

#ifdef CONFIG_RPC_VENDOR_AIDL
int AidlRpcManager::getVendorStaIfaceAidlObjectByIfname(
	const std::string &ifname,
	std::shared_ptr<ISupplicantVendorStaIface> *iface_object)
{
	if (ifname.empty() || !iface_object)
		return 1;

	auto iface_object_iter = vendor_sta_iface_object_map_.find(ifname);
	if (iface_object_iter == vendor_sta_iface_object_map_.end())
		return 1;

	*iface_object = iface_object_iter->second;
	return 0;
}

int AidlRpcManager::addVendorStaIfaceCallbackAidlObject(
	const std::string &ifname,
	const std::shared_ptr<ISupplicantVendorStaIfaceCallback> &callback)
{
	return addCallbackAidlObjectToMap(
		death_notifier_, ifname, callback,
		vendor_sta_iface_callbacks_map_);
}

void AidlRpcManager::callWithEachVendorStaIfaceCallback(
	const std::string& ifname,
	const std::function<
		ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantVendorStaIfaceCallback>)
		> &method)
{
	if (!checkForVendorStaIfaceCallback(ifname))
		return;

	callWithEachCallback(ifname, method, vendor_sta_iface_callbacks_map_);
}

#endif

#ifdef CONFIG_RPC_P2P
/**
 * Finds the correct |wpa_supplicant| object for P2P notifications
 *
 * @param wpa_s the |wpa_supplicant| that triggered the P2P event.
 * @return appropriate |wpa_supplicant| object or NULL if not found.
 */
struct wpa_supplicant *AidlRpcManager::getTargetP2pIfaceForGroup(
		struct wpa_supplicant *wpa_group_s)
{
	if (!wpa_group_s || !wpa_group_s->parent)
		return NULL;

	struct wpa_supplicant *target_wpa_s = wpa_group_s->parent;

	// check wpa_supplicant object is a p2p device interface
	if ((wpa_group_s == wpa_group_s->p2pdev) && wpa_group_s->p2p_mgmt) {
		if (p2p_iface_object_map_.find(wpa_group_s->ifname) !=
			p2p_iface_object_map_.end())
			return wpa_group_s;
	}

	if (p2p_iface_object_map_.find(target_wpa_s->ifname) !=
		p2p_iface_object_map_.end())
		return target_wpa_s;

	// try P2P device if available
	if (!target_wpa_s->p2pdev || !target_wpa_s->p2pdev->p2p_mgmt)
		return NULL;

	target_wpa_s = target_wpa_s->p2pdev;
	if (p2p_iface_object_map_.find(target_wpa_s->ifname) !=
		p2p_iface_object_map_.end())
		return target_wpa_s;

	return NULL;
}

/**
 * Removes the provided iface callback aidl object reference from
 * our interface callback list.
 *
 * @param ifname Name of the corresponding interface.
 * @param callback Aidl reference of the callback object.
 */
void AidlRpcManager::removeP2pIfaceCallbackAidlObject(
	const std::string &ifname,
	const std::shared_ptr<ISupplicantP2pIfaceCallback> &callback)
{
	return removeCallbackAidlObjectFromMap(
		ifname, callback, p2p_iface_callbacks_map_);
}
#endif

/**
 * Removes the provided |ISupplicantCallback| aidl object reference
 * from our global callback list.
 *
 * @param callback Aidl reference of the |ISupplicantCallback| object.
 */
void AidlRpcManager::removeSupplicantCallbackAidlObject(
	const std::shared_ptr<ISupplicantCallback> &callback)
{
	supplicant_callbacks_.erase(
		std::remove(
			supplicant_callbacks_.begin(),
			supplicant_callbacks_.end(), callback),
		supplicant_callbacks_.end());
}

/**
 * Removes the provided iface callback aidl object reference from
 * our interface callback list.
 *
 * @param ifname Name of the corresponding interface.
 * @param callback Aidl reference of the callback object.
 */
void AidlRpcManager::removeStaIfaceCallbackAidlObject(
	const std::string &ifname,
	const std::shared_ptr<ISupplicantStaIfaceCallback> &callback)
{
	return removeCallbackAidlObjectFromMap(
		ifname, callback, sta_iface_callbacks_map_);
}

/**
 * Removes the provided network callback aidl object reference from
 * our network callback list.
 *
 * @param ifname Name of the corresponding interface.
 * @param network_id ID of the corresponding network.
 * @param callback Aidl reference of the callback object.
 */
void AidlRpcManager::removeStaNetworkCallbackAidlObject(
	const std::string &ifname, int network_id,
	const std::shared_ptr<ISupplicantStaNetworkCallback> &callback)
{
	const std::string key = getNetworkObjectMapKey(ifname, network_id);
	return removeCallbackAidlObjectFromMap(
		key, callback, sta_network_callbacks_map_);
}

#ifdef CONFIG_RPC_VENDOR_AIDL
/**
 * Removes the provided vendor iface callback aidl object reference from
 * our interface vendor callback list.
 *
 * @param ifname Name of the corresponding interface.
 * @param callback Aidl reference of the vendor callback object.
 */
void AidlRpcManager::removeVendorStaIfaceCallbackAidlObject(
	const std::string &ifname,
	const std::shared_ptr<ISupplicantVendorStaIfaceCallback> &callback)
{
	return removeCallbackAidlObjectFromMap(
		ifname, callback, vendor_sta_iface_callbacks_map_);
}

/**
 * Helper function to check if there is any callback of type
 * ISupplicantVendorStaIfaceCallback is registered for the specified
 * |ifname|.
 *
 * @param ifname Name of the corresponding interface.
 **/
bool AidlRpcManager::checkForVendorStaIfaceCallback(const std::string &ifname)
{
	if (ifname.empty())
		return false;

	auto iter = vendor_sta_iface_callbacks_map_.find(ifname);
	if (iter == vendor_sta_iface_callbacks_map_.end())
		return false;
	const auto &callback_list = iter->second;
	for (const auto &callback : callback_list) {
		auto vendorCallback = callback;
		if (vendorCallback != nullptr)
			return true;
	}
	ALOGE("No VendorStaIfaceCallback is register");
	return false;
}
#endif

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
