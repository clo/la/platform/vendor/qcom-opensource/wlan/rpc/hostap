/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <common_util.h>
#include <supplicant_vendor_message_def.h>
#include <supplicant_vendor_sta_iface_msg.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"
#include "supplicant_rpc_vendor_event.h"

namespace {

using aidl::android::hardware::wifi::supplicant::AidlRpcManager;

template <typename CallbackFuncT, typename... Args>
static void SupplicantRpcCallVendorStaIfaceCallback(int32_t staIfaceId,
	CallbackFuncT&& cbFunc, Args&&... args)
{
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager)
		return;

	const std::string ifname =
		manager->getStaIfaceNameByInstanceId(staIfaceId);
	ALOGD("Supplicant StaIface name: %s instanceId: %d",
		ifname.c_str(), staIfaceId);

	const std::function<
		ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantVendorStaIfaceCallback>)
		> func = std::bind(cbFunc, std::placeholders::_1,
			std::forward<Args>(args)...);

	manager->callWithEachVendorStaIfaceCallback(ifname, func);
}

static void SupplicantRpcVendorStaIfaceCtrlEvent(uint8_t *data, size_t length)
{
	if (!data || length < SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE) {
		ALOGE("rpc vendor event: invalid payload length %d", length);
		return;
	}

	OnCtrlEventIndParam param;
	uint16_t staIfaceId = (uint16_t)data[0] | (((uint16_t)data[1]) << 8);
	if (!SupplicantVendorStaIfaceParseOnCtrlEventInd(
		data + SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE,
		length - SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE, param)) {
		ALOGE("Vendor sta iface ctrl event payload parse fail");
		return;
	}

	SupplicantRpcCallVendorStaIfaceCallback(staIfaceId,
		&ISupplicantVendorStaIfaceCallback::onCtrlEvent,
		param.ifaceName, param.eventStr);
}

} // root namespace

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace supplicant {

bool SupplicantRpcInitVendorEvents()
{
	SupplicantRegisterEventHandler(
		SUPPLICANT_VENDOR_STA_IFACE_ON_CTRL_EVENT_IND,
		&SupplicantRpcVendorStaIfaceCtrlEvent);

	return true;
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl
