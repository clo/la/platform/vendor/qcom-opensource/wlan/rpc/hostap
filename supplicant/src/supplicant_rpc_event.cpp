/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <common_util.h>
#include <supplicant_message_def.h>
#include <supplicant_msg.h>
#include <supplicant_sta_iface_msg.h>
#include <supplicant_sta_network_msg.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"
#include "supplicant_rpc_event.h"

namespace {

using aidl::android::hardware::wifi::supplicant::AidlRpcManager;

/**
 * Event Payload parse functions:
 * | uint16_t   | uint16_t     | length - 4 |
 * | staIfaceId | staNetworkId | data       |
 * data is parsed by proto parser function.
 */
template <typename ProtoFuncT, typename... Args>
static bool SupplicantParseEventPayload(uint8_t* data, size_t length,
	ProtoFuncT&& protoFunc, uint16_t& staIfaceId, uint16_t& networkId,
	Args&&... args)
{
	if (!data || length < SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE) {
		ALOGE("rpc event: invalid payload length %d", length);
		return false;
	}

	staIfaceId = (uint16_t)data[0] | (((uint16_t)data[1]) << 8);
	networkId = ((uint16_t)data[2]) | (((uint16_t)data[3]) << 8);

	return (*protoFunc)(data + SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE,
		length - SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE,
		std::forward<Args>(args)...);
}

template <typename CallbackFuncT, typename... Args>
static void SupplicantRpcCallStaIfaceCallback(uint16_t staIfaceId,
	CallbackFuncT&& cbFunc, Args&&... args)
{
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager)
		return;

	const std::string ifname =
		manager->getStaIfaceNameByInstanceId(staIfaceId);
	ALOGD("Supplicant StaIface name: %s instanceId: %d",
		ifname.c_str(), staIfaceId);

	const std::function<
		ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantStaIfaceCallback>)> func =
		std::bind(cbFunc, std::placeholders::_1,
			std::forward<Args>(args)...);
	manager->callWithEachStaIfaceCallback(ifname, func);
}

template <typename CallbackFuncT, typename... Args>
static void SupplicantRpcCallStaNetworkCallback(uint16_t staIfaceId,
	uint16_t networkId, CallbackFuncT&& cbFunc, Args&&... args)
{
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager)
		return;

	const std::string name =
		manager->getStaNetworkNameByInstanceId(staIfaceId, networkId);
	ALOGD("Supplicant StaNetwork name: %s instanceId: %d",
	      name.c_str(), networkId);

        const std::function<
		ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantStaNetworkCallback>)> func =
		std::bind(cbFunc, std::placeholders::_1,
			std::forward<Args>(args)...);
        manager->callWithEachStaNetworkCallback(name, func);
}

/* Event Handler Functions */
static void SupplicantRpcEventInterfaceCreated(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	std::string ifaceName;

	if (SupplicantParseEventPayload(data, length,
		&SupplicantParseOnInterfaceCreatedInd,
		staIfaceId, networkId, ifaceName))
		ALOGD("Supplicant RPC event interface %s created",
			ifaceName.c_str());
}

static void SupplicantRpcEventInterfaceRemoved(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	std::string ifaceName;
	if (SupplicantParseEventPayload(data, length,
		&SupplicantParseOnInterfaceRemovedInd,
		staIfaceId, networkId, ifaceName))
		ALOGD("Supplicant RPC event interface %s removed",
			ifaceName.c_str());
}

static void SupplicantRpcStaIfaceEventAnqpQueryDone(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	OnAnqpQueryDoneIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnAnqpQueryDoneInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onAnqpQueryDone,
		param.bssid, param.data, param.hs20Data);
}

static void SupplicantRpcStaIfaceEventAssociationRejected(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	AssociationRejectionData param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnAssociationRejectedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onAssociationRejected, param);
}

static void SupplicantRpcStaIfaceEventAuthenticationTimeout(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	std::vector<uint8_t> param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnAuthenticationTimeoutInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onAuthenticationTimeout, param);
}

static void SupplicantRpcStaIfaceEventAuxiliarySupplicantEvent(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	OnAuxiliarySupplicantEventIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnAuxiliarySupplicantEventInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onAuxiliarySupplicantEvent,
		param.eventCode, param.bssid, param.reasonString);
}

static void SupplicantRpcStaIfaceEventBssTmHandlingDone(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	BssTmData param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnBssTmHandlingDoneInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onBssTmHandlingDone, param);
}

static void SupplicantRpcStaIfaceEventBssidChanged(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	OnBssidChangedIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnBssidChangedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onBssidChanged,
		param.reason, param.bssid);
}

static void SupplicantRpcStaIfaceEventDisconnected(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	OnDisconnectedIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnDisconnectedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onDisconnected,
		param.bssid, param.locallyGenerated, param.reasonCode);
}

static void SupplicantRpcStaIfaceEventDppFailure(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	OnDppFailureIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnDppFailureInd,
		staIfaceId, networkId, param))
		return;

	std::vector<char16_t> band_list_vec;
	for (auto item : param.bandList)
		band_list_vec.push_back((char16_t)item);

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onDppFailure,
		param.code, param.ssid, param.channelList, band_list_vec);
}

static void SupplicantRpcStaIfaceEventDppProgress(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	DppProgressCode param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnDppProgressInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onDppProgress, param);
}

static void SupplicantRpcStaIfaceEventDppSuccess(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	DppEventType param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnDppSuccessInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onDppSuccess, param);
}

static void SupplicantRpcStaIfaceEventDppSuccessConfigReceived(uint8_t *data,
	size_t length)
{
	/*
	 * Event onDppSuccessConfigReceived is deprecated from AIDL v2.
	 * Newer HAL should use event onDppConfigReceived instead.
	 */
}

static void SupplicantRpcStaIfaceEventDppConfigReceived(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	DppConfigurationData param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnDppConfigReceivedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onDppConfigReceived, param);
}

static void SupplicantRpcStaIfaceEventDppConnectionStatusResultSent(
	uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	DppStatusErrorCode param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnDppConnectionStatusResultSentInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onDppConnectionStatusResultSent,
		param);
}

static void SupplicantRpcStaIfaceEventDppSuccessConfigSent(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnDppSuccessConfigSentInd,
		staIfaceId, networkId))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onDppSuccessConfigSent);
}

static void SupplicantRpcStaIfaceEventEapFailure(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	OnEapFailureIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnEapFailureInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onEapFailure,
		param.bssid, param.errorCode);
}

static void SupplicantRpcStaIfaceEventExtRadioWorkStart(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	int32_t param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnExtRadioWorkStartInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onExtRadioWorkStart, param);
}

static void SupplicantRpcStaIfaceEventExtRadioWorkTimeout(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	int32_t param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnExtRadioWorkTimeoutInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onExtRadioWorkTimeout, param);
}

static void SupplicantRpcStaIfaceEventHs20DeauthImminentNotice(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	OnHs20DeauthImminentNoticeIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnHs20DeauthImminentNoticeInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onHs20DeauthImminentNotice,
		param.bssid, param.reasonCode, param.reAuthDelayInSec,
		param.url);
}

static void SupplicantRpcStaIfaceEventHs20IconQueryDone(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	OnHs20IconQueryDoneIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnHs20IconQueryDoneInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onHs20IconQueryDone,
		param.bssid, param.fileName, param.data);
}

static void SupplicantRpcStaIfaceEventHs20SubscriptionRemediation(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	OnHs20SubscriptionRemediationIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnHs20SubscriptionRemediationInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onHs20SubscriptionRemediation,
		param.bssid, param.osuMethod, param.url);
}

static void SupplicantRpcStaIfaceEventHs20TermsAndConditionsAcceptanceRequestedNotification(
	uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	OnHs20TermsAndConditionsAcceptanceRequestedNotificationIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnHs20TermsAndConditionsAcceptanceRequestedNotificationInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onHs20TermsAndConditionsAcceptanceRequestedNotification,
		param.bssid, param.url);
}

static void SupplicantRpcStaIfaceEventNetworkAdded(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	int32_t param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnNetworkAddedInd,
		staIfaceId, networkId, param))
		return;

	ALOGD("Supplicant RPC sta iface id %d added network id %d",
		staIfaceId, networkId);
}

static void SupplicantRpcStaIfaceEventNetworkNotFound(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	std::vector<uint8_t> param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnNetworkNotFoundInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onNetworkNotFound, param);
}

static void SupplicantRpcStaIfaceEventNetworkRemoved(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	int32_t param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnNetworkRemovedInd,
		staIfaceId, networkId, param))
		return;

	ALOGD("Supplicant RPC sta iface id %d removed network id %d",
		staIfaceId, networkId);
}

static void SupplicantRpcStaIfaceEventPmkCacheAdded(uint8_t *data,
	size_t length)
{
	/*
	 * Event onPmkCacheAdded is deprecated from AIDL v2.
	 * Newer HAL should use event onPmkCacheSaAdded instead.
	 */
}

static void SupplicantRpcStaIfaceEventStateChanged(uint8_t *data, size_t length)
{
	/*
	 * Event onStateChanged is deprecated from AIDL v2.
	 * Newer HAL should use event onSupplicantStateChanged instead.
	 */
}

static void SupplicantRpcStaIfaceEventWpsFail(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	OnWpsEventFailIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnWpsEventFailInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onWpsEventFail,
		param.bssid, param.configError, param.errorInd);
}

static void SupplicantRpcStaIfaceEventWpsPbcOverlap(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnWpsEventPbcOverlapInd,
		staIfaceId, networkId))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onWpsEventPbcOverlap);
}

static void SupplicantRpcStaIfaceEventWpsSuccess(uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnWpsEventSuccessInd,
		staIfaceId, networkId))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onWpsEventSuccess);
}

static void SupplicantRpcStaIfaceEventQosPolicyReset(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnQosPolicyResetInd,
		staIfaceId, networkId))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onQosPolicyReset);
}

static void SupplicantRpcStaIfaceEventQosPolicyRequest(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	OnQosPolicyRequestIndStaIfaceParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnQosPolicyRequestInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onQosPolicyRequest,
		param.qosPolicyRequestId, param.qosPolicyData);
}

static void SupplicantRpcStaIfaceEventMloLinksInfoChanged(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	ISupplicantStaIfaceCallback::MloLinkInfoChangeReason param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnMloLinksInfoChangedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onMloLinksInfoChanged, param);
}

static void SupplicantRpcStaIfaceEventBssFrequencyChanged(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	int32_t param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnBssFrequencyChangedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onBssFrequencyChanged, param);
}

static void SupplicantRpcStaIfaceEventSupplicantStateChanged(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	SupplicantStateChangeData param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnSupplicantStateChangedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onSupplicantStateChanged, param);
}

static void SupplicantRpcStaIfaceEventQosPolicyResponseForScs(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	std::vector<QosPolicyScsResponseStatus> param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnQosPolicyResponseForScsInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onQosPolicyResponseForScs, param);
}

static void SupplicantRpcStaIfaceEventPmkSaCacheAdded(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	PmkSaCacheData param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaIfaceParseOnPmkSaCacheAddedInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaIfaceCallback(staIfaceId,
		&ISupplicantStaIfaceCallback::onPmkSaCacheAdded, param);
}

/* Supplicant Sta Network Callback events handlers */
static void SupplicantRpcStaNetworkEventEapIdentityRequest(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaNetworkParseOnNetworkEapIdentityRequestInd,
		staIfaceId, networkId))
		return;

	SupplicantRpcCallStaNetworkCallback(staIfaceId, networkId,
		&ISupplicantStaNetworkCallback::onNetworkEapIdentityRequest);
}

static void SupplicantRpcStaNetworkEventEapSimGsmAuthRequest(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	NetworkRequestEapSimGsmAuthParams param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaNetworkParseOnNetworkEapSimGsmAuthRequestInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaNetworkCallback(staIfaceId, networkId,
		&ISupplicantStaNetworkCallback::onNetworkEapSimGsmAuthRequest,
		param);
}

static void SupplicantRpcStaNetworkEventEapSimUmtsAuthRequest(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	NetworkRequestEapSimUmtsAuthParams param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaNetworkParseOnNetworkEapSimUmtsAuthRequestInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaNetworkCallback(staIfaceId, networkId,
		&ISupplicantStaNetworkCallback::onNetworkEapSimUmtsAuthRequest,
		param);
}

static void SupplicantRpcStaNetworkEventTransitionDisable(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	TransitionDisableIndication param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaNetworkParseOnTransitionDisableInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaNetworkCallback(staIfaceId, networkId,
		&ISupplicantStaNetworkCallback::onTransitionDisable, param);
}

static void SupplicantRpcStaNetworkEventServerCertificateAvailable(
	uint8_t *data, size_t length)
{
	uint16_t staIfaceId, networkId;
	OnServerCertificateAvailableIndStaNetworkParam param;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaNetworkParseOnServerCertificateAvailableInd,
		staIfaceId, networkId, param))
		return;

	SupplicantRpcCallStaNetworkCallback(staIfaceId, networkId,
		&ISupplicantStaNetworkCallback::onServerCertificateAvailable,
		param.depth, param.subject, param.certHash, param.certBlob);
}

static void SupplicantRpcStaNetworkEventPermanentIdReqDenied(uint8_t *data,
	size_t length)
{
	uint16_t staIfaceId, networkId;
	if (!SupplicantParseEventPayload(data, length,
		&SupplicantStaNetworkParseOnPermanentIdReqDeniedInd,
		staIfaceId, networkId))
		return;

	SupplicantRpcCallStaNetworkCallback(staIfaceId, networkId,
		&ISupplicantStaNetworkCallback::onPermanentIdReqDenied);
}

} // root namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

bool SupplicantRpcInitEvents()
{
	/* ISupplicantCallback event handler */
	SupplicantRegisterEventHandler(
		SUPPLICANT_ON_INTERFACE_CREATED_IND,
		&SupplicantRpcEventInterfaceCreated);
	SupplicantRegisterEventHandler(
		SUPPLICANT_ON_INTERFACE_REMOVED_IND,
		&SupplicantRpcEventInterfaceRemoved);

	/* ISupplicantStaIfaceCallback event handler */
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_ANQP_QUERY_DONE_IND,
		&SupplicantRpcStaIfaceEventAnqpQueryDone);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_ASSOCIATION_REJECTED_IND,
		&SupplicantRpcStaIfaceEventAssociationRejected);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_AUTHENTICATION_TIMEOUT_IND,
		&SupplicantRpcStaIfaceEventAuthenticationTimeout);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_AUXILIARY_SUPPLICANT_EVENT_IND,
		&SupplicantRpcStaIfaceEventAuxiliarySupplicantEvent);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_BSS_TM_HANDLING_DONE_IND,
		&SupplicantRpcStaIfaceEventBssTmHandlingDone);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_BSSID_CHANGED_IND,
		&SupplicantRpcStaIfaceEventBssidChanged);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DISCONNECTED_IND,
		&SupplicantRpcStaIfaceEventDisconnected);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DPP_FAILURE_IND,
		&SupplicantRpcStaIfaceEventDppFailure);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DPP_PROGRESS_IND,
		&SupplicantRpcStaIfaceEventDppProgress);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DPP_SUCCESS_IND,
		&SupplicantRpcStaIfaceEventDppSuccess);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DPP_SUCCESS_CONFIG_RECEIVED_IND,
		&SupplicantRpcStaIfaceEventDppSuccessConfigReceived);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DPP_SUCCESS_CONFIG_SENT_IND,
		&SupplicantRpcStaIfaceEventDppSuccessConfigSent);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_EAP_FAILURE_IND,
		&SupplicantRpcStaIfaceEventEapFailure);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_EXT_RADIO_WORK_START_IND,
		&SupplicantRpcStaIfaceEventExtRadioWorkStart);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_EXT_RADIO_WORK_TIMEOUT_IND,
		&SupplicantRpcStaIfaceEventExtRadioWorkTimeout);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_HS20_DEAUTH_IMMINENT_NOTICE_IND,
		&SupplicantRpcStaIfaceEventHs20DeauthImminentNotice);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_HS20_ICON_QUERY_DONE_IND,
		&SupplicantRpcStaIfaceEventHs20IconQueryDone);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_HS20_SUBSCRIPTION_REMEDIATION_IND,
		&SupplicantRpcStaIfaceEventHs20SubscriptionRemediation);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_HS20_TERMS_AND_CONDITIONS_ACCEPTANCE_REQUESTED_NOTIFICATION_IND,
		&SupplicantRpcStaIfaceEventHs20TermsAndConditionsAcceptanceRequestedNotification);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_NETWORK_ADDED_IND,
		&SupplicantRpcStaIfaceEventNetworkAdded);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_NETWORK_NOT_FOUND_IND,
		&SupplicantRpcStaIfaceEventNetworkNotFound);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_NETWORK_REMOVED_IND,
		&SupplicantRpcStaIfaceEventNetworkRemoved);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_PMK_CACHE_ADDED_IND,
		&SupplicantRpcStaIfaceEventPmkCacheAdded);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_STATE_CHANGED_IND,
		&SupplicantRpcStaIfaceEventStateChanged);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_WPS_EVENT_FAIL_IND,
		&SupplicantRpcStaIfaceEventWpsFail);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_WPS_EVENT_PBC_OVERLAP_IND,
		&SupplicantRpcStaIfaceEventWpsPbcOverlap);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_WPS_EVENT_SUCCESS_IND,
		&SupplicantRpcStaIfaceEventWpsSuccess);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_QOS_POLICY_RESET_IND,
		&SupplicantRpcStaIfaceEventQosPolicyReset);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_QOS_POLICY_REQUEST_IND,
		&SupplicantRpcStaIfaceEventQosPolicyRequest);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_MLO_LINKS_INFO_CHANGED_IND,
		&SupplicantRpcStaIfaceEventMloLinksInfoChanged);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DPP_CONFIG_RECEIVED_IND,
		&SupplicantRpcStaIfaceEventDppConfigReceived);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_DPP_CONNECTION_STATUS_RESULT_SENT_IND,
		&SupplicantRpcStaIfaceEventDppConnectionStatusResultSent);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_BSS_FREQUENCY_CHANGED_IND,
		&SupplicantRpcStaIfaceEventBssFrequencyChanged);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_SUPPLICANT_STATE_CHANGED_IND,
		&SupplicantRpcStaIfaceEventSupplicantStateChanged);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_QOS_POLICY_RESPONSE_FOR_SCS_IND,
		&SupplicantRpcStaIfaceEventQosPolicyResponseForScs);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_IFACE_ON_PMK_SA_CACHE_ADDED_IND,
		&SupplicantRpcStaIfaceEventPmkSaCacheAdded);

	/* ISupplicantStaNetwork event handler */
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_NETWORK_ON_NETWORK_EAP_IDENTITY_REQUEST_IND,
		&SupplicantRpcStaNetworkEventEapIdentityRequest);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_NETWORK_ON_NETWORK_EAP_SIM_GSM_AUTH_REQUEST_IND,
		&SupplicantRpcStaNetworkEventEapSimGsmAuthRequest);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_NETWORK_ON_NETWORK_EAP_SIM_UMTS_AUTH_REQUEST_IND,
		&SupplicantRpcStaNetworkEventEapSimUmtsAuthRequest);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_NETWORK_ON_TRANSITION_DISABLE_IND,
		&SupplicantRpcStaNetworkEventTransitionDisable);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_NETWORK_ON_SERVER_CERTIFICATE_AVAILABLE_IND,
		&SupplicantRpcStaNetworkEventServerCertificateAvailable);
	SupplicantRegisterEventHandler(
		SUPPLICANT_STA_NETWORK_ON_PERMANENT_ID_REQ_DENIED_IND,
		&SupplicantRpcStaNetworkEventPermanentIdReqDenied);

	return true;
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
