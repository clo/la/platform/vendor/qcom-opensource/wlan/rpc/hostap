/*
 * WPA Supplicant - Supplicant Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <android-base/file.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <supplicant_msg.h>
#include <supplicant_message_def.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#ifdef CONFIG_RPC_P2P
#include "p2p_iface.h"
#endif
#include "supplicant_rpc.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"

namespace {
// Pre-populated interface params for interfaces controlled by wpa_supplicant.
// Note: This may differ for other OEM's. So, modify this accordingly.
constexpr char kIfaceDriverName[] = "nl80211";
constexpr char kStaIfaceConfPath[] =
	"/data/vendor/wifi/wpa/wpa_supplicant.conf";
static const char* kStaIfaceConfOverlayPaths[] = {
    "/apex/com.android.wifi.hal/etc/wifi/wpa_supplicant_overlay.conf",
    "/vendor/etc/wifi/wpa_supplicant_overlay.conf",
};
#ifdef CONFIG_RPC_P2P
constexpr char kP2pIfaceConfPath[] =
	"/data/vendor/wifi/wpa/p2p_supplicant.conf";
static const char* kP2pIfaceConfOverlayPaths[] = {
    "/apex/com.android.wifi.hal/etc/wifi/p2p_supplicant_overlay.conf",
    "/vendor/etc/wifi/p2p_supplicant_overlay.conf",
};
#endif
// Migrate conf files for existing devices.
static const char* kTemplateConfPaths[] = {
    "/apex/com.android.wifi.hal/etc/wifi/wpa_supplicant.conf",
    "/vendor/etc/wifi/wpa_supplicant.conf",
    "/system/etc/wifi/wpa_supplicant.conf",
};
constexpr char kOldStaIfaceConfPath[] = "/data/misc/wifi/wpa_supplicant.conf";
#ifdef CONFIG_RPC_P2P
constexpr char kOldP2pIfaceConfPath[] = "/data/misc/wifi/p2p_supplicant.conf";
#endif
constexpr mode_t kConfigFileMode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;
#if 0
const char* resolvePath(const char* paths[], size_t size)
{
	for (int i = 0; i < size; ++i) {
		if (access(paths[i], R_OK) == 0) {
			return paths[i];
		}
	}
	return nullptr;
}

int copyFile(
	const std::string& src_file_path, const std::string& dest_file_path)
{
	std::string file_contents;
	if (!android::base::ReadFileToString(src_file_path, &file_contents)) {
		wpa_printf(
			MSG_ERROR, "Failed to read from %s. Errno: %s",
			src_file_path.c_str(), strerror(errno));
		return -1;
	}
	if (!android::base::WriteStringToFile(
		file_contents, dest_file_path, kConfigFileMode, getuid(),
		getgid())) {
		wpa_printf(
			MSG_ERROR, "Failed to write to %s. Errno: %s",
			dest_file_path.c_str(), strerror(errno));
		return -1;
	}
	return 0;
}

/**
 * Copy |src_file_path| to |dest_file_path| if it exists.
 *
 * Returns 1 if |src_file_path| does not exist or not accessible,
 * Returns -1 if the copy fails.
 * Returns 0 if the copy succeeds.
 */
int copyFileIfItExists(
	const std::string& src_file_path, const std::string& dest_file_path)
{
	int ret = access(src_file_path.c_str(), R_OK);
	// Sepolicy denial (2018+ device) will return EACCESS instead of ENOENT.
	if ((ret != 0) && ((errno == ENOENT) || (errno == EACCES))) {
		return 1;
	}
	ret = copyFile(src_file_path, dest_file_path);
	if (ret != 0) {
		wpa_printf(
			MSG_ERROR, "Failed copying %s to %s.",
			src_file_path.c_str(), dest_file_path.c_str());
		return -1;
	}
	return 0;
}

/**
 * Ensure that the specified config file pointed by |config_file_path| exists.
 * a) If the |config_file_path| exists with the correct permissions, return.
 * b) If the |config_file_path| does not exist, but |old_config_file_path|
 * exists, copy over the contents of the |old_config_file_path| to
 * |config_file_path|.
 * c) If the |config_file_path| & |old_config_file_path|
 * does not exists, copy over the contents of |template_config_file_path|.
 */
int ensureConfigFileExists(
	const std::string& config_file_path,
	const std::string& old_config_file_path)
{
	int ret = access(config_file_path.c_str(), R_OK | W_OK);
	if (ret == 0) {
		return 0;
	}
	if (errno == EACCES) {
		ret = chmod(config_file_path.c_str(), kConfigFileMode);
		if (ret == 0) {
			return 0;
		} else {
			wpa_printf(
				MSG_ERROR, "Cannot set RW to %s. Errno: %s",
				config_file_path.c_str(), strerror(errno));
			return -1;
		}
	} else if (errno != ENOENT) {
		wpa_printf(
			MSG_ERROR, "Cannot acces %s. Errno: %s",
			config_file_path.c_str(), strerror(errno));
		return -1;
	}
	ret = copyFileIfItExists(old_config_file_path, config_file_path);
	if (ret == 0) {
		wpa_printf(
			MSG_INFO, "Migrated conf file from %s to %s",
			old_config_file_path.c_str(), config_file_path.c_str());
		unlink(old_config_file_path.c_str());
		return 0;
	} else if (ret == -1) {
		unlink(config_file_path.c_str());
		return -1;
	}
	const char* path =
	    resolvePath(kTemplateConfPaths,
	    sizeof(kTemplateConfPaths)/sizeof(kTemplateConfPaths[0]));
	if (path != nullptr) {
		ret = copyFileIfItExists(path, config_file_path);
		if (ret == 0) {
			wpa_printf(
			    MSG_INFO, "Copied template conf file from %s to %s",
			    path, config_file_path.c_str());
			return 0;
		} else if (ret == -1) {
			unlink(config_file_path.c_str());
			return -1;
		}
	}
	// Did not create the conf file.
	return -1;
}
#endif
}  // namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

SupplicantRpc::SupplicantRpc() {}
bool SupplicantRpc::isValid()
{
	// This top level object cannot be invalidated.
	return true;
}

ndk::ScopedAStatus SupplicantRpc::addP2pInterface(
	const std::string& in_name,
	std::shared_ptr<ISupplicantP2pIface>* _aidl_return)
{
#ifdef CONFIG_RPC_P2P
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::addP2pInterfaceInternal, _aidl_return, in_name);
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

ndk::ScopedAStatus SupplicantRpc::getP2pInterface(
	const std::string& in_name,
	std::shared_ptr<ISupplicantP2pIface>* _aidl_return)
{
#ifdef CONFIG_RPC_P2P
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::getP2pInterfaceInternal, _aidl_return, in_name);
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

ndk::ScopedAStatus SupplicantRpc::addStaInterface(
	const std::string& in_name,
	std::shared_ptr<ISupplicantStaIface>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::addStaInterfaceInternal, _aidl_return, in_name);
}

ndk::ScopedAStatus SupplicantRpc::removeInterface(
	const IfaceInfo& in_ifaceInfo)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::removeInterfaceInternal, in_ifaceInfo);
}

ndk::ScopedAStatus SupplicantRpc::getStaInterface(
	const std::string& in_name,
	std::shared_ptr<ISupplicantStaIface>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::getStaInterfaceInternal, _aidl_return, in_name);
}

ndk::ScopedAStatus SupplicantRpc::listInterfaces(
	std::vector<IfaceInfo>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::listInterfacesInternal, _aidl_return);
}

ndk::ScopedAStatus SupplicantRpc::registerCallback(
	const std::shared_ptr<ISupplicantCallback>& in_callback)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::registerCallbackInternal, in_callback);
}

ndk::ScopedAStatus SupplicantRpc::registerNonStandardCertCallback(
	const std::shared_ptr<INonStandardCertCallback>& in_callback)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::registerNonStandardCertCallbackInternal,
		in_callback);
}

ndk::ScopedAStatus SupplicantRpc::setDebugParams(
	DebugLevel in_level, bool in_showTimestamp, bool in_showKeys)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::setDebugParamsInternal, in_level,
		in_showTimestamp, in_showKeys);
}

ndk::ScopedAStatus SupplicantRpc::setConcurrencyPriority(
	IfaceType in_type)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantRpc::setConcurrencyPriorityInternal, in_type);
}

ndk::ScopedAStatus SupplicantRpc::getDebugLevel(DebugLevel* _aidl_return)
{
	GetDebugLevelCfmParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_GET_DEBUG_LEVEL_REQ);
	if (!response || !SupplicantParseGetDebugLevelCfm(response->getData(),
		response->getLength(), param)) {
		ALOGE("RPC: get debug level fail.");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNSUPPORTED);
	}

	*_aidl_return = static_cast<DebugLevel>(param.result);
	return SupplicantParam2NdkStatus(param.status);
}

ndk::ScopedAStatus SupplicantRpc::isDebugShowTimestampEnabled(bool* _aidl_return)
{
	IsDebugShowTimestampEnabledCfmParam param;
        auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_IS_DEBUG_SHOW_TIMESTAMP_ENABLED_REQ);
        if (!response || !SupplicantParseIsDebugShowTimestampEnabledCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get is debug show timestamp enabled fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNSUPPORTED);
	}

	*_aidl_return = param.result ? true : false;
	return SupplicantParam2NdkStatus(param.status);
}

ndk::ScopedAStatus SupplicantRpc::isDebugShowKeysEnabled(bool* _aidl_return)
{
	IsDebugShowKeysEnabledCfmParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_IS_DEBUG_SHOW_KEYS_ENABLED_REQ);
	if (!response || !SupplicantParseIsDebugShowKeysEnabledCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get is debug show keys enabled fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNSUPPORTED);
	}

	*_aidl_return = param.result ? true : false;
	return SupplicantParam2NdkStatus(param.status);
}

ndk::ScopedAStatus SupplicantRpc::terminate()
{
	ALOGI("Terminating...");

	auto status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_TERMINATE_REQ);
	if (status.isOk())
		ALOGI("RPC: Terminated");
	else
		ALOGE("RPC: terminate fail");

	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (manager)
		manager->destroyInstance();

	/* Always successful for local termination */
	return ndk::ScopedAStatus::ok();
}

#ifdef CONFIG_RPC_P2P
std::pair<std::shared_ptr<ISupplicantP2pIface>, ndk::ScopedAStatus>
SupplicantRpc::addP2pInterfaceInternal(const std::string& name)
{
	if (name.empty())
		return {nullptr,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};

	std::vector<uint8_t> payload;
	if (!SupplicantSerializeAddP2pInterfaceReq(name, payload)) {
		ALOGE("Serialize add p2p interface request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	AddP2pInterfaceCfmParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_ADD_P2P_INTERFACE_REQ, payload);
	if (!response || !SupplicantParseAddP2pInterfaceCfm(response->getData(),
		response->getLength(), param)) {
		ALOGE("RPC: add p2p interface fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNSUPPORTED);
	}

	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (manager)
		manager->registerInterface(IfaceType::P2P, name, 0);
	} else
		ALOGE("RPC: add p2p interface fail");

	return getP2pInterfaceInternal(name);
}

std::pair<std::shared_ptr<ISupplicantP2pIface>, ndk::ScopedAStatus>
SupplicantRpc::getP2pInterfaceInternal(const std::string& name)
{
	std::shared_ptr<ISupplicantP2pIface> iface;
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager ||
		manager->getP2pIfaceAidlObjectByIfname(name, &iface)) {
		return {iface,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_UNKNOWN)};
	}

	return {iface, ndk::ScopedAStatus::ok()};
}
#endif

std::pair<std::shared_ptr<ISupplicantStaIface>, ndk::ScopedAStatus>
SupplicantRpc::addStaInterfaceInternal(const std::string& name)
{
	// Check if required |ifname| argument is empty.
	if (name.empty())
		return {nullptr,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};

	// Try to get the wpa_supplicant record for this iface, return
	// the iface object with the appropriate status code if it exists.
	ndk::ScopedAStatus status;
	std::shared_ptr<ISupplicantStaIface> iface;
	std::tie(iface, status) = getStaInterfaceInternal(name);
	if (status.isOk()) {
		ALOGI("Iface already exists, return existing");
		return {iface, ndk::ScopedAStatus::ok()};
	}

	std::vector<uint8_t> payload;
	if (!SupplicantSerializeAddStaInterfaceReq(name, payload)) {
		ALOGE("Serialize Add Sta Interface Req fail");
		return {nullptr,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	AddStaInterfaceCfmParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_ADD_STA_INTERFACE_REQ, payload);
	if (!response || !SupplicantParseAddStaInterfaceCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: add Sta iface fail");
		return {nullptr,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	if (param.result & 0xffff0000) {
		ALOGE("RPC: invalid Sta iface id 0x%x", param.result);
		return {nullptr,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	ALOGI("RPC: Add Sta interface %s with instanceId 0x%x",
		name.c_str(), param.result);

	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (manager)
		manager->registerInterface(
			IfaceType::STA, name, param.result);

	// AidlManager when |wpa_supplicant_add_iface| is called.
	return getStaInterfaceInternal(name);
}

ndk::ScopedAStatus SupplicantRpc::removeInterfaceInternal(
	const IfaceInfo& iface_info)
{
	std::vector<uint8_t> payload;
	if (!SupplicantSerializeRemoveInterfaceReq(iface_info, payload))
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_REMOVE_INTERFACE_REQ, payload);
	if (!status.isOk()) {
		ALOGE("RPC: fail to remove interface name %s",
			iface_info.name.c_str());
		return status;
	}

	AidlRpcManager *manager = AidlRpcManager::getInstance();

	if(!manager ||
		manager->unregisterAllNetworkOnInterface(
			iface_info.type, iface_info.name)) {
		ALOGE("fail to unregister All network on interface %s",
			iface_info.toString().c_str());
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_IFACE_INVALID);
	}

	if (!manager ||
		manager->unregisterInterface(
			iface_info.type, iface_info.name)) {
		ALOGE("fail to unregister interface %s",
			iface_info.toString().c_str());
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_IFACE_INVALID);
	}

	return status;
}

std::pair<std::shared_ptr<ISupplicantStaIface>, ndk::ScopedAStatus>
SupplicantRpc::getStaInterfaceInternal(const std::string& name)
{
	std::shared_ptr<StaIfaceRpc> iface;
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager ||
		manager->getStaIfaceAidlObjectByIfname(name, &iface)) {
		return {iface,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_UNKNOWN)};
	}
	
	return {iface, ndk::ScopedAStatus::ok()};
}

std::pair<std::vector<IfaceInfo>, ndk::ScopedAStatus>
SupplicantRpc::listInterfacesInternal()
{
	ListInterfacesCfmParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_LIST_INTERFACES_REQ);
	if (!response || !SupplicantParseListInterfacesCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: List interface fail");
		return {std::vector<IfaceInfo>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNSUPPORTED)};
	}

	return {std::move(param.result),
		SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus SupplicantRpc::registerCallbackInternal(
	const std::shared_ptr<ISupplicantCallback>& callback)
{
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager ||
		manager->addSupplicantCallbackAidlObject(callback)) {
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNKNOWN);
	}

	return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus SupplicantRpc::registerNonStandardCertCallbackInternal(
	const std::shared_ptr<INonStandardCertCallback>& callback)
{
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager ||
		manager->registerNonStandardCertCallbackAidlObject(callback)) {
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNKNOWN);
	}

	return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus SupplicantRpc::setDebugParamsInternal(
	DebugLevel level, bool show_timestamp, bool show_keys)
{
	ALOGD("Skip RPC set debug request as remote can handle it.");
	return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus SupplicantRpc::setConcurrencyPriorityInternal(IfaceType type)
{
	std::vector<uint8_t> payload;
	if (!SupplicantSerializeSetConcurrencyPriorityReq(type, payload)) {
		ALOGE("Serialize set concurrency priority request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_SET_CONCURRENCY_PRIORITY_REQ, payload);
	if (!status.isOk())
		ALOGE("RPC: set concurrency priority fail");

	return status;
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
