/*
 * WPA Supplicant - Sta Iface Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <aidl/android/hardware/wifi/supplicant/SupplicantStatusCode.h>
#include <common_util.h>
#include <supplicant_message_def.h>
#include <supplicant_sta_iface_msg.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#include "sta_iface_rpc.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"

namespace {
constexpr uint32_t kMaxAnqpElems = 100;
}  // namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

StaIfaceRpc::StaIfaceRpc(const std::string& ifname, int32_t instance_id)
	: ifname_(ifname), instance_id_(instance_id), is_valid_(true)
{}

void StaIfaceRpc::invalidate() { is_valid_ = false; }
bool StaIfaceRpc::isValid()
{
	return is_valid_;
}

int32_t StaIfaceRpc::getInstanceId()
{
	return instance_id_;
}

ndk::ScopedAStatus StaIfaceRpc::getName(
	std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::getNameInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::getType(
	IfaceType* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::getTypeInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::addNetwork(
	std::shared_ptr<ISupplicantStaNetwork>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::addNetworkInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::removeNetwork(
	int32_t in_id)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::removeNetworkInternal, in_id);
}

ndk::ScopedAStatus StaIfaceRpc::filsHlpFlushRequest()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::filsHlpFlushRequestInternal);
}

ndk::ScopedAStatus StaIfaceRpc::filsHlpAddRequest(
	const std::vector<uint8_t>& in_dst_mac,
	const std::vector<uint8_t>& in_pkt)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::filsHlpAddRequestInternal, in_dst_mac, in_pkt);
}

ndk::ScopedAStatus StaIfaceRpc::getNetwork(
	int32_t in_id, std::shared_ptr<ISupplicantStaNetwork>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::getNetworkInternal, _aidl_return, in_id);
}

ndk::ScopedAStatus StaIfaceRpc::listNetworks(
	std::vector<int32_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::listNetworksInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::registerCallback(
	const std::shared_ptr<ISupplicantStaIfaceCallback>& in_callback)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::registerCallbackInternal, in_callback);
}

ndk::ScopedAStatus StaIfaceRpc::reassociate()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::reassociateInternal);
}

ndk::ScopedAStatus StaIfaceRpc::reconnect()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::reconnectInternal);
}

ndk::ScopedAStatus StaIfaceRpc::disconnect()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::disconnectInternal);
}

ndk::ScopedAStatus StaIfaceRpc::setPowerSave(
	bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setPowerSaveInternal, in_enable);
}

ndk::ScopedAStatus StaIfaceRpc::initiateTdlsDiscover(
	const std::vector<uint8_t>& in_macAddress)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::initiateTdlsDiscoverInternal, in_macAddress);
}

ndk::ScopedAStatus StaIfaceRpc::initiateTdlsSetup(
	const std::vector<uint8_t>& in_macAddress)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::initiateTdlsSetupInternal, in_macAddress);
}

ndk::ScopedAStatus StaIfaceRpc::initiateTdlsTeardown(
	const std::vector<uint8_t>& in_macAddress)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::initiateTdlsTeardownInternal, in_macAddress);
}

ndk::ScopedAStatus StaIfaceRpc::initiateAnqpQuery(
	const std::vector<uint8_t>& in_macAddress,
	const std::vector<AnqpInfoId>& in_infoElements,
	const std::vector<Hs20AnqpSubtypes>& in_subTypes)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::initiateAnqpQueryInternal, in_macAddress,
		in_infoElements, in_subTypes);
}

ndk::ScopedAStatus StaIfaceRpc::initiateVenueUrlAnqpQuery(
	const std::vector<uint8_t>& in_macAddress)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::initiateVenueUrlAnqpQueryInternal, in_macAddress);
}

ndk::ScopedAStatus StaIfaceRpc::initiateHs20IconQuery(
	const std::vector<uint8_t>& in_macAddress,
	const std::string& in_fileName)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::initiateHs20IconQueryInternal, in_macAddress,
		in_fileName);
}

ndk::ScopedAStatus StaIfaceRpc::getMacAddress(
	std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::getMacAddressInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::startRxFilter()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::startRxFilterInternal);
}

ndk::ScopedAStatus StaIfaceRpc::stopRxFilter()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::stopRxFilterInternal);
}

ndk::ScopedAStatus StaIfaceRpc::addRxFilter(
	RxFilterType in_type)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::addRxFilterInternal, in_type);
}

ndk::ScopedAStatus StaIfaceRpc::removeRxFilter(
	RxFilterType in_type)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::removeRxFilterInternal, in_type);
}

ndk::ScopedAStatus StaIfaceRpc::setBtCoexistenceMode(
	BtCoexistenceMode in_mode)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setBtCoexistenceModeInternal, in_mode);
}

ndk::ScopedAStatus StaIfaceRpc::setBtCoexistenceScanModeEnabled(
	bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setBtCoexistenceScanModeEnabledInternal,
		in_enable);
}

ndk::ScopedAStatus StaIfaceRpc::setSuspendModeEnabled(
	bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setSuspendModeEnabledInternal, in_enable);
}

ndk::ScopedAStatus StaIfaceRpc::setCountryCode(
	const std::vector<uint8_t>& in_code)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setCountryCodeInternal, in_code);
}

ndk::ScopedAStatus StaIfaceRpc::startWpsRegistrar(
	const std::vector<uint8_t>& in_bssid,
	const std::string& in_pin)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::startWpsRegistrarInternal, in_bssid, in_pin);
}

ndk::ScopedAStatus StaIfaceRpc::startWpsPbc(
	const std::vector<uint8_t>& in_bssid)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::startWpsPbcInternal, in_bssid);
}

ndk::ScopedAStatus StaIfaceRpc::startWpsPinKeypad(
	const std::string& in_pin)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::startWpsPinKeypadInternal, in_pin);
}

ndk::ScopedAStatus StaIfaceRpc::startWpsPinDisplay(
	const std::vector<uint8_t>& in_bssid,
	std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::startWpsPinDisplayInternal, _aidl_return,
		in_bssid);
}

ndk::ScopedAStatus StaIfaceRpc::cancelWps()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::cancelWpsInternal);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsDeviceName(
	const std::string& in_name)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setWpsDeviceNameInternal, in_name);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsDeviceType(
	const std::vector<uint8_t>& in_type)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setWpsDeviceTypeInternal, in_type);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsManufacturer(
	const std::string& in_manufacturer)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setWpsManufacturerInternal, in_manufacturer);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsModelName(
	const std::string& in_modelName)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setWpsModelNameInternal, in_modelName);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsModelNumber(
	const std::string& in_modelNumber)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setWpsModelNumberInternal, in_modelNumber);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsSerialNumber(
	const std::string& in_serialNumber)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setWpsSerialNumberInternal, in_serialNumber);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsConfigMethods(
	WpsConfigMethods in_configMethods)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setWpsConfigMethodsInternal, in_configMethods);
}

ndk::ScopedAStatus StaIfaceRpc::setExternalSim(
	bool in_useExternalSim)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::setExternalSimInternal, in_useExternalSim);
}

ndk::ScopedAStatus StaIfaceRpc::addExtRadioWork(
	const std::string& in_name, int32_t in_freqInMhz,
	int32_t in_timeoutInSec,
	int32_t* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::addExtRadioWorkInternal, _aidl_return, in_name,
		in_freqInMhz, in_timeoutInSec);
}

ndk::ScopedAStatus StaIfaceRpc::removeExtRadioWork(
	int32_t in_id)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::removeExtRadioWorkInternal, in_id);
}

ndk::ScopedAStatus StaIfaceRpc::enableAutoReconnect(
	bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::enableAutoReconnectInternal, in_enable);
}

ndk::ScopedAStatus StaIfaceRpc::getKeyMgmtCapabilities(
	KeyMgmtMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaIfaceRpc::getKeyMgmtCapabilitiesInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::addDppPeerUri(
	const std::string& in_uri, int32_t* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaIfaceRpc::addDppPeerUriInternal, _aidl_return, in_uri);
}

ndk::ScopedAStatus StaIfaceRpc::removeDppUri(
	int32_t in_id)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaIfaceRpc::removeDppUriInternal, in_id);
}

ndk::ScopedAStatus StaIfaceRpc::startDppConfiguratorInitiator(
	int32_t in_peerBootstrapId, int32_t in_ownBootstrapId,
	const std::string& in_ssid, const std::string& in_password,
	const std::string& in_psk, DppNetRole in_netRole,
	DppAkm in_securityAkm, const std::vector<uint8_t>& in_privEcKey,
	std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaIfaceRpc::startDppConfiguratorInitiatorInternal,
		_aidl_return, in_peerBootstrapId,in_ownBootstrapId,
		in_ssid, in_password, in_psk, in_netRole,
		in_securityAkm, in_privEcKey);
}

ndk::ScopedAStatus StaIfaceRpc::startDppEnrolleeInitiator(
	int32_t in_peerBootstrapId, int32_t in_ownBootstrapId)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaIfaceRpc::startDppEnrolleeInitiatorInternal,
		in_peerBootstrapId, in_ownBootstrapId);
}

ndk::ScopedAStatus StaIfaceRpc::stopDppInitiator()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaIfaceRpc::stopDppInitiatorInternal);
}

ndk::ScopedAStatus StaIfaceRpc::getConnectionCapabilities(
	ConnectionCapabilities* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&StaIfaceRpc::getConnectionCapabilitiesInternal,
		_aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::generateDppBootstrapInfoForResponder(
	const std::vector<uint8_t>& in_macAddress,
	const std::string& in_deviceInfo,
	DppCurve in_curve, DppResponderBootstrapInfo* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::generateDppBootstrapInfoForResponderInternal,
		_aidl_return,  in_macAddress, in_deviceInfo, in_curve);
}

ndk::ScopedAStatus StaIfaceRpc::startDppEnrolleeResponder(
	int32_t in_listenChannel)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::startDppEnrolleeResponderInternal,
		in_listenChannel);
}

ndk::ScopedAStatus StaIfaceRpc::stopDppResponder(
	int32_t in_ownBootstrapId)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::stopDppResponderInternal, in_ownBootstrapId);
}

ndk::ScopedAStatus StaIfaceRpc::generateSelfDppConfiguration(
	const std::string& in_ssid, const std::vector<uint8_t>& in_privEcKey)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&StaIfaceRpc::generateSelfDppConfigurationInternal,
		in_ssid, in_privEcKey);
}

ndk::ScopedAStatus StaIfaceRpc::getWpaDriverCapabilities(
	WpaDriverCapabilitiesMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&StaIfaceRpc::getWpaDriverCapabilitiesInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::setMboCellularDataStatus(
	bool in_available)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&StaIfaceRpc::setMboCellularDataStatusInternal, in_available);
}

ndk::ScopedAStatus StaIfaceRpc::setQosPolicyFeatureEnabled(
	bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&StaIfaceRpc::setQosPolicyFeatureEnabledInternal, in_enable);
}

ndk::ScopedAStatus StaIfaceRpc::sendQosPolicyResponse(
	int32_t in_qosPolicyRequestId, bool in_morePolicies,
	const std::vector<QosPolicyStatus>& in_qosPolicyStatusList)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&StaIfaceRpc::sendQosPolicyResponseInternal,
		in_qosPolicyRequestId, in_morePolicies, in_qosPolicyStatusList);
}

ndk::ScopedAStatus StaIfaceRpc::removeAllQosPolicies()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&StaIfaceRpc::removeAllQosPoliciesInternal);
}

ndk::ScopedAStatus StaIfaceRpc::getConnectionMloLinksInfo(
	MloLinksInfo* _aidl_return) {
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&StaIfaceRpc::getConnectionMloLinksInfoInternal, _aidl_return);
}

ndk::ScopedAStatus StaIfaceRpc::getSignalPollResults(
    std::vector<SignalPollResult> *results)
{
	return ValidateAndCall(
	    this, SupplicantStatusCode::FAILURE_UNKNOWN,
	    &StaIfaceRpc::getSignalPollResultsInternal, results);
}

ndk::ScopedAStatus StaIfaceRpc::addQosPolicyRequestForScs(
	const std::vector<QosPolicyScsData>& in_qosPolicyData,
	std::vector<QosPolicyScsRequestStatus>* _aidl_return)
{
	return ValidateAndCall(
	    this, SupplicantStatusCode::FAILURE_UNKNOWN,
	    &StaIfaceRpc::addQosPolicyRequestForScsInternal, _aidl_return,
	    in_qosPolicyData);
}

ndk::ScopedAStatus StaIfaceRpc::removeQosPolicyForScs(
	const std::vector<uint8_t>& in_scsPolicyIds,
	std::vector<QosPolicyScsRequestStatus>* _aidl_return)
{
	return ValidateAndCall(
	    this, SupplicantStatusCode::FAILURE_UNKNOWN,
	    &StaIfaceRpc::removeQosPolicyForScsInternal, _aidl_return,
	    in_scsPolicyIds);
}

std::pair<std::string, ndk::ScopedAStatus> StaIfaceRpc::getNameInternal()
{
	return {ifname_, ndk::ScopedAStatus::ok()};
}

std::pair<IfaceType, ndk::ScopedAStatus> StaIfaceRpc::getTypeInternal()
{
	return {IfaceType::STA, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus StaIfaceRpc::filsHlpFlushRequestInternal()
{
#ifdef CONFIG_RPC_FILS
	return SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_FILS_HLP_FLUSH_REQUEST_REQ,
		std::vector<uint8_t>(), instance_id_);
#else /* CONFIG_RPC_FILS */
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif /* CONFIG_RPC_FILS */
}

ndk::ScopedAStatus StaIfaceRpc::filsHlpAddRequestInternal(
	const std::vector<uint8_t> &dst_mac, const std::vector<uint8_t> &pkt)
{
#ifdef CONFIG_RPC_FILS
	if (dst_mac.size() != ETH_ALEN || !pkt.size())
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeFilsHlpAddRequestReq(
		dst_mac, pkt, payload)) {
		ALOGE("Serialize fils hlp add request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	return SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_FILS_HLP_ADD_REQUEST_REQ,
		payload, instance_id_);
#endif /* CONFIG_RPC_FILS */
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
}

std::pair<std::shared_ptr<ISupplicantStaNetwork>, ndk::ScopedAStatus>
StaIfaceRpc::addNetworkInternal()
{
	AddNetworkCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_ADD_NETWORK_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseAddNetworkCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("Add network request fail");
		return {nullptr, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	if (param.result & 0xffff0000) {
		ALOGE("RPC: invalid network id 0x%x", param.result);
		return {nullptr, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNKNOWN)};
	}

	ALOGI("RPC: Add Network for %s with instanceId 0x%x",
		ifname_.c_str(), param.result);

	AidlRpcManager *aidl_rpc_manager = AidlRpcManager::getInstance();
	if (!aidl_rpc_manager ||
		aidl_rpc_manager->registerNetwork(
			IfaceType::STA, ifname_, param.result))
		return {nullptr,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_UNKNOWN)};

	std::shared_ptr<StaNetworkRpc> network;
	if (aidl_rpc_manager->getStaNetworkAidlObjectByIfnameAndNetworkId(
		ifname_, param.result, &network)) {
		ALOGE("Failed to get Network");
		return {nullptr,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_UNKNOWN)};
	}

	return {network, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus StaIfaceRpc::removeNetworkInternal(int32_t id)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeRemoveNetworkReq(id, payload)) {
		ALOGE("Serialize remove network request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_REMOVE_NETWORK_REQ, payload, instance_id_);
	if (!status.isOk()) {
		ALOGE("RPC: remove network %d fail", id);
		return status;
	}

	AidlRpcManager *aidl_rpc_manager = AidlRpcManager::getInstance();
	if (!aidl_rpc_manager ||
		aidl_rpc_manager->unregisterNetwork(
			IfaceType::STA, ifname_, id)) {
		ALOGE("fail to unregister network with id %d", id);
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN);
	}

	return status;
}

std::pair<std::shared_ptr<ISupplicantStaNetwork>, ndk::ScopedAStatus>
StaIfaceRpc::getNetworkInternal(int32_t id)
{
	std::shared_ptr<StaNetworkRpc> network;
	AidlRpcManager *aidl_rpc_manager = AidlRpcManager::getInstance();
	if (!aidl_rpc_manager ||
		aidl_rpc_manager->getStaNetworkAidlObjectByIfnameAndNetworkId(
			ifname_, id, &network)) {
		ALOGE("Fail to get network with id %d", id);
		return {nullptr, 
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {network, ndk::ScopedAStatus::ok()};
}

std::pair<std::vector<int32_t>, ndk::ScopedAStatus>
StaIfaceRpc::listNetworksInternal()
{
	ListNetworksCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_LIST_NETWORKS_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseListNetworksCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: list networks fail");
		return {std::vector<int32_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus StaIfaceRpc::registerCallbackInternal(
	const std::shared_ptr<ISupplicantStaIfaceCallback> &callback)
{
	AidlRpcManager *aidl_rpc_manager = AidlRpcManager::getInstance();
	if (!aidl_rpc_manager ||
		aidl_rpc_manager->addStaIfaceCallbackAidlObject(
			ifname_, callback)) {
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNKNOWN);
	}
	return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus StaIfaceRpc::reassociateInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_REASSOCIATE_REQ, std::vector<uint8_t>(),
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: reassociate fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::reconnectInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_RECONNECT_REQ, std::vector<uint8_t>(),
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: reconnect fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::disconnectInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_DISCONNECT_REQ, std::vector<uint8_t>(),
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: disconnect fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::setPowerSaveInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetPowerSaveReq(enable, payload)) {
		ALOGE("Serialize set power save request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_POWER_SAVE_REQ, payload, instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set power save fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::initiateTdlsDiscoverInternal(
	const std::vector<uint8_t> &mac_address)
{
	if (mac_address.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeInitiateTdlsDiscoverReq(
		mac_address, payload)) {
		ALOGE("Serialize initiate TDLS Discover request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_INITIATE_TDLS_DISCOVER_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: initiate TDLS Discover fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::initiateTdlsSetupInternal(
	const std::vector<uint8_t> &mac_address)
{
	if (mac_address.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeInitiateTdlsSetupReq(
		mac_address, payload)) {
		ALOGE("Serialize initiate TDLS setup request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_INITIATE_TDLS_SETUP_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: initiate TDLS Setup fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::initiateTdlsTeardownInternal(
	const std::vector<uint8_t> &mac_address)
{
	if (mac_address.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeInitiateTdlsTeardownReq(
		mac_address, payload)) {
		ALOGE("Serialize initiate TDLS Teardown request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_INITIATE_TDLS_TEARDOWN_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: initiate TDLS Teardwon fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::initiateAnqpQueryInternal(
	const std::vector<uint8_t> &mac_address,
	const std::vector<AnqpInfoId> &info_elements,
	const std::vector<Hs20AnqpSubtypes> &sub_types)
{
	if (info_elements.size() > kMaxAnqpElems ||
		mac_address.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeInitiateAnqpQueryReq(
		mac_address, info_elements, sub_types, payload)) {
		ALOGE("Serialize initiate Anqp Query request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_INITIATE_ANQP_QUERY_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: initiate Anqp Query fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::initiateVenueUrlAnqpQueryInternal(
	const std::vector<uint8_t> &mac_address)
{
	if (mac_address.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeInitiateVenueUrlAnqpQueryReq(
		mac_address, payload)) {
		ALOGE("Serialize initiate Venue Url Anqp Query request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_INITIATE_VENUE_URL_ANQP_QUERY_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: initiate Venue Url Anqp Query fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::initiateHs20IconQueryInternal(
	const std::vector<uint8_t> &mac_address, const std::string &file_name)
{
	if (mac_address.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeInitiateHs20IconQueryReq(
		mac_address, file_name, payload)) {
		ALOGE("Serialize initiate HS20 Icon Query request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_INITIATE_HS20_ICON_QUERY_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: initiate HS20 Icon Query fail");

	return status;
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaIfaceRpc::getMacAddressInternal()
{
	GetMacAddressCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_GET_MAC_ADDRESS_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseGetMacAddressCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get mac address fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus StaIfaceRpc::startRxFilterInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_START_RX_FILTER_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!status.isOk())
		ALOGE("RPC: start rx filter fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::stopRxFilterInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_STOP_RX_FILTER_REQ, std::vector<uint8_t>(),
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: stop rx filter fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::addRxFilterInternal(
	RxFilterType type)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeAddRxFilterReq(type, payload)) {
		ALOGE("Serialize add rx filter request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_ADD_RX_FILTER_REQ, payload, instance_id_);
	if (!status.isOk())
		ALOGE("RPC: add rx filter fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::removeRxFilterInternal(
	RxFilterType type)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeRemoveRxFilterReq(type, payload)) {
		ALOGE("Serialize remove rx filter request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_REMOVE_RX_FILTER_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: remove rx filter fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::setBtCoexistenceModeInternal(
	BtCoexistenceMode mode)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetBtCoexistenceModeReq(mode, payload))
	{
		ALOGE("Serialize set btcoex mode request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_BT_COEXISTENCE_MODE_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set btcoex mode fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::setBtCoexistenceScanModeEnabledInternal(
	bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetBtCoexistenceScanModeEnabledReq(
		enable, payload)) {
		ALOGE("Serialize set btcoex scan mode enabled request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_BT_COEXISTENCE_SCAN_MODE_ENABLED_REQ,
		payload, instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set btcoex scan mode enabled fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::setSuspendModeEnabledInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetSuspendModeEnabledReq(
		enable, payload)) {
		ALOGE("Serialize set suspend mode enabled request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_SUSPEND_MODE_ENABLED_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set suspend mode enabled fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::setCountryCodeInternal(
	const std::vector<uint8_t> &code)
{
	if (code.size() != 2)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetCountryCodeReq(code, payload)) {
		ALOGE("Serialize set country code request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_COUNTRY_CODE_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set country code fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::startWpsRegistrarInternal(
	const std::vector<uint8_t> &bssid, const std::string &pin)
{
#ifdef CONFIG_RPC_WPS
	if (bssid.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStartWpsRegistrarReq(
		bssid, pin, payload)) {
		ALOGE("Serialize start WPS registrar request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_START_WPS_REGISTRAR_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: start WPS registrar fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::startWpsPbcInternal(
		const std::vector<uint8_t> &bssid)
{
#ifdef CONFIG_RPC_WPS
	if (bssid.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStartWpsPbcReq(bssid, payload)) {
		ALOGE("Serialize start WPS PBC request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_START_WPS_PBC_REQ, payload, instance_id_);
	if (!status.isOk())
		ALOGE("RPC: start WPS PBC fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::startWpsPinKeypadInternal(
		const std::string &pin)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStartWpsPinKeypadReq(pin, payload)) {
		ALOGE("Serialize start WPS Pin Key Pad request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_START_WPS_PIN_KEYPAD_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: start WPS PIN Keypad fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

std::pair<std::string, ndk::ScopedAStatus>
StaIfaceRpc::startWpsPinDisplayInternal(const std::vector<uint8_t> &bssid)
{
#ifdef CONFIG_RPC_WPS
	if (bssid.size() != ETH_ALEN)
		return {"",
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStartWpsPinDisplayReq(
		bssid, payload)) {
		ALOGE("Serialize start WPS Pin Display request fail");
		return {"",
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	StartWpsPinDisplayCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_START_WPS_PIN_DISPLAY_REQ, payload,
		instance_id_);
	if (!response || !SupplicantStaIfaceParseStartWpsPinDisplayCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: start WPS PIN Display fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
#endif
	return {"",
		SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN)};
}

ndk::ScopedAStatus StaIfaceRpc::cancelWpsInternal()
{
#ifdef CONFIG_RPC_WPS
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_CANCEL_WPS_REQ, std::vector<uint8_t>(),
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: cancel WPS fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsDeviceNameInternal(
	const std::string &name)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetWpsDeviceNameReq(name, payload)) {
		ALOGE("Serialize set WPS Device Name request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_WPS_DEVICE_NAME_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set WPS Device name fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsDeviceTypeInternal(
	const std::vector<uint8_t> &type)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetWpsDeviceTypeReq(type, payload)) {
		ALOGE("Serialize set WPS Device type request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_WPS_DEVICE_TYPE_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set WPS Device type fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsManufacturerInternal(
	const std::string &manufacturer)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetWpsManufacturerReq(
		manufacturer, payload)) {
		ALOGE("Serialize set WPS manufacturer request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_WPS_MANUFACTURER_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set WPS manufacturer fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsModelNameInternal(
	const std::string &model_name)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetWpsModelNameReq(
		model_name, payload)) {
		ALOGE("Serialize set WPS model name request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_WPS_MODEL_NAME_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set WPS model name fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsModelNumberInternal(
	const std::string &model_number)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetWpsModelNumberReq(
		model_number, payload)) {
		ALOGE("Serialize set WPS model number request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_WPS_MODEL_NUMBER_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set WPS model number fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsSerialNumberInternal(
	const std::string &serial_number)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetWpsSerialNumberReq(
		serial_number, payload)) {
		ALOGE("Serialize set WPS serial number request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_WPS_SERIAL_NUMBER_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set WPS serial number fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setWpsConfigMethodsInternal(
		WpsConfigMethods config_methods)
{
#ifdef CONFIG_RPC_WPS
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetWpsConfigMethodsReq(
		config_methods, payload)) {
		ALOGE("Serialize set WPS condig methods request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_WPS_CONFIG_METHODS_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set WPS condig methods fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::setExternalSimInternal(bool useExternalSim)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetExternalSimReq(
		useExternalSim, payload)) {
		ALOGE("Serialize set external sim request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_EXTERNAL_SIM_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set external sim fail");

	return status;
}

std::pair<uint32_t, ndk::ScopedAStatus> StaIfaceRpc::addExtRadioWorkInternal(
	const std::string &name, uint32_t freq_in_mhz, uint32_t timeout_in_sec)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeAddExtRadioWorkReq(
		name, freq_in_mhz, timeout_in_sec, payload)) {
		ALOGE("Serialize add ext radio work request fail");
		return {UINT32_MAX,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	AddExtRadioWorkCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_ADD_EXT_RADIO_WORK_REQ, payload,
		instance_id_);
	if (!response || !SupplicantStaIfaceParseAddExtRadioWorkCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: add ext radio work fail");
		return {UINT32_MAX, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaIfaceRpc::removeExtRadioWorkInternal(uint32_t id)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeRemoveExtRadioWorkReq(id, payload)) {
		ALOGE("Serialize remove ext radio work request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_REMOVE_EXT_RADIO_WORK_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: remove ext radio work fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::enableAutoReconnectInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeEnableAutoReconnectReq(
		enable, payload)) {
		ALOGE("Serialize enable auto reconnect request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_ENABLE_AUTO_RECONNECT_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: enable auto reconnect fail");

	return status;
}

std::pair<uint32_t, ndk::ScopedAStatus>
StaIfaceRpc::addDppPeerUriInternal(const std::string& uri)
{
#ifdef CONFIG_RPC_DPP
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeAddDppPeerUriReq(uri, payload)) {
		ALOGE("Serialize add dpp peer uri request fail");
		return {-1, SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	AddDppPeerUriCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_ADD_DPP_PEER_URI_REQ, payload,
		instance_id_);
	if (!response || !SupplicantStaIfaceParseAddDppPeerUriCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: add dpp peer uri fail");
		return {-1, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
#endif
	return {-1, 
		SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN)};
}

ndk::ScopedAStatus StaIfaceRpc::removeDppUriInternal(uint32_t bootstrap_id)
{
#ifdef CONFIG_RPC_DPP
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeRemoveDppUriReq(
		bootstrap_id, payload)) {
		ALOGE("Serialize remove dpp uri request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_REMOVE_DPP_URI_REQ, payload, instance_id_);
	if (!status.isOk())
		ALOGE("RPC: remove dpp uri fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaIfaceRpc::startDppConfiguratorInitiatorInternal(
	uint32_t peer_bootstrap_id, uint32_t own_bootstrap_id,
	const std::string& ssid, const std::string& password,
	const std::string& psk, DppNetRole net_role, DppAkm security_akm,
	const std::vector<uint8_t> &privEcKey)
{
#ifdef CONFIG_RPC_DPP
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStartDppConfiguratorInitiatorReq(
		peer_bootstrap_id, own_bootstrap_id, ssid, password, psk,
		net_role, security_akm, privEcKey, payload)) {
		ALOGE("Serialize start dpp configurator initiator request fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	StartDppConfiguratorInitiatorCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_START_DPP_CONFIGURATOR_INITIATOR_REQ,
		payload, instance_id_);
	if (!response ||
		!SupplicantStaIfaceParseStartDppConfiguratorInitiatorCfm(
			response->getData(), response->getLength(), param)) {
		ALOGE("RPC: start dpp configurator initiator fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
#endif
	return {std::vector<uint8_t>(),
		SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN)};
}

ndk::ScopedAStatus StaIfaceRpc::startDppEnrolleeInitiatorInternal(
	uint32_t peer_bootstrap_id, uint32_t own_bootstrap_id) {
#ifdef CONFIG_RPC_DPP
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStartDppEnrolleeInitiatorReq(
		peer_bootstrap_id, own_bootstrap_id, payload)) {
		ALOGE("Serialize start dpp enrollee initiator request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_START_DPP_ENROLLEE_INITIATOR_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: start dpp enrollee initiator fail");

	return status;
#endif
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
}

ndk::ScopedAStatus StaIfaceRpc::stopDppInitiatorInternal()
{
#ifdef CONFIG_RPC_DPP
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_STOP_DPP_INITIATOR_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!status.isOk())
		ALOGE("RPC: stop dpp initiator fail");

	return status;
#else
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
#endif
}

std::pair<DppResponderBootstrapInfo, ndk::ScopedAStatus>
StaIfaceRpc::generateDppBootstrapInfoForResponderInternal(
	const std::vector<uint8_t> &mac_address, 
	const std::string& device_info, DppCurve curve)
{
	DppResponderBootstrapInfo bootstrap_info;
#ifdef CONFIG_RPC_DPP
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeGenerateDppBootstrapInfoForResponderReq(
		mac_address, device_info, curve, payload)) {
		ALOGE("Serialize generate dpp bootstrap info fail");
		return {bootstrap_info,
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	GenerateDppBootstrapInfoForResponderCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_GENERATE_DPP_BOOTSTRAP_INFO_FOR_RESPONDER_REQ,
		payload, instance_id_);
	if (!response ||
		!SupplicantStaIfaceParseGenerateDppBootstrapInfoForResponderCfm(
			response->getData(), response->getLength(), param)) {
		ALOGE("RPC: gen dpp bootstrap info for responder fail");
		return {bootstrap_info, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
#else
	return {bootstrap_info,
		SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNSUPPORTED)};
#endif
}

ndk::ScopedAStatus StaIfaceRpc::startDppEnrolleeResponderInternal(
	uint32_t listen_channel)
{
#ifdef CONFIG_RPC_DPP
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStartDppEnrolleeResponderReq(
		listen_channel, payload)) {
		ALOGE("Serialize start dpp enrollee responder request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_START_DPP_ENROLLEE_RESPONDER_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: start dpp enrollee responder fail");

	return status;
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

ndk::ScopedAStatus StaIfaceRpc::stopDppResponderInternal(
	uint32_t own_bootstrap_id)
{
#ifdef CONFIG_RPC_DPP
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeStopDppResponderReq(
		own_bootstrap_id, payload)) {
		ALOGE("Serialize stop DPP responder request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_STOP_DPP_RESPONDER_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: stop DPP responder fail");

	return status;
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

ndk::ScopedAStatus StaIfaceRpc::generateSelfDppConfigurationInternal(
	const std::string& ssid, const std::vector<uint8_t> &privEcKey)
{
#ifdef CONFIG_RPC_DPP
	if (ssid.empty() || privEcKey.empty()) {
		ALOGE("DPP generate self config failed. ssid/key empty");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeGenerateSelfDppConfigurationReq(
		ssid, privEcKey, payload)) {
		ALOGE("Serialize generate self DPP config request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_GENERATE_SELF_DPP_CONFIGURATION_REQ,
		payload, instance_id_);
	if (!status.isOk())
		ALOGE("RPC: generate self DPP config fail");

	return status;
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

std::pair<ConnectionCapabilities, ndk::ScopedAStatus>
StaIfaceRpc::getConnectionCapabilitiesInternal()
{
	GetConnectionCapabilitiesCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_GET_CONNECTION_CAPABILITIES_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseGetConnectionCapabilitiesCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get connection capabilities fail");
		return {ConnectionCapabilities(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<WpaDriverCapabilitiesMask, ndk::ScopedAStatus>
StaIfaceRpc::getWpaDriverCapabilitiesInternal()
{
	GetWpaDriverCapabilitiesCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_GET_WPA_DRIVER_CAPABILITIES_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseGetWpaDriverCapabilitiesCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get wpa driver capabilities fail");
		return {WpaDriverCapabilitiesMask::MBO, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaIfaceRpc::setMboCellularDataStatusInternal(bool available)
{
#ifdef CONFIG_RPC_MBO
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetMboCellularDataStatusReq(
		available, payload)) {
		ALOGE("Serialize set MBO cellular data status request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_MBO_CELLULAR_DATA_STATUS_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set MBO cellular data status fail");

	return status;
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

std::pair<KeyMgmtMask, ndk::ScopedAStatus>
StaIfaceRpc::getKeyMgmtCapabilitiesInternal()
{
	GetKeyMgmtCapabilitiesCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_GET_KEY_MGMT_CAPABILITIES_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseGetKeyMgmtCapabilitiesCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get key mgmt capabilities fail");
		return {KeyMgmtMask::WPA_EAP, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaIfaceRpc::setQosPolicyFeatureEnabledInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSetQosPolicyFeatureEnabledReq(
		enable, payload)) {
		ALOGE("Serialize set Qos policy feature enabled request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SET_QOS_POLICY_FEATURE_ENABLED_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: set Qos policy feature enabled fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::sendQosPolicyResponseInternal(
	int32_t qos_policy_request_id, bool more_policies,
	const std::vector<QosPolicyStatus>& qos_policy_status_list)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeSendQosPolicyResponseReq(
		qos_policy_request_id, more_policies,
		qos_policy_status_list, payload)) {
		ALOGE("Serialize send Qos policy response request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_SEND_QOS_POLICY_RESPONSE_REQ, payload,
		instance_id_);
	if (!status.isOk())
		ALOGE("RPC: send Qos policy response fail");

	return status;
}

ndk::ScopedAStatus StaIfaceRpc::removeAllQosPoliciesInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_IFACE_REMOVE_ALL_QOS_POLICIES_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!status.isOk())
		ALOGE("RPC: remove all Qos policies fail");

	return status;
}

std::pair<MloLinksInfo, ndk::ScopedAStatus>
StaIfaceRpc::getConnectionMloLinksInfoInternal()
{
	GetConnectionMloLinksInfoCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_GET_CONNECTION_MLO_LINKS_INFO_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseGetConnectionMloLinksInfoCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get connection MLO links info fail");
		return {MloLinksInfo(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
        }

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<SignalPollResult>, ndk::ScopedAStatus>
StaIfaceRpc::getSignalPollResultsInternal()
{
	GetSignalPollResultsCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_GET_SIGNAL_POLL_RESULTS_REQ,
		std::vector<uint8_t>(), instance_id_);
	if (!response || !SupplicantStaIfaceParseGetSignalPollResultsCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get signal poll results fail");
		return {std::vector<SignalPollResult>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

/**
 * This is a request to the AP (if it supports the feature) to apply the QoS policy
 * on traffic in the Downlink.
 */
std::pair<std::vector<QosPolicyScsRequestStatus>, ndk::ScopedAStatus>
StaIfaceRpc::addQosPolicyRequestForScsInternal(
	const std::vector<QosPolicyScsData>& qosPolicyData)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeAddQosPolicyRequestForScsReq(
		qosPolicyData, payload)) {
		ALOGE("Serialize add Qos policy req for scs request fail");
		return {std::vector<QosPolicyScsRequestStatus>(),
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	AddQosPolicyRequestForScsCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_ADD_QOS_POLICY_REQUEST_FOR_SCS_REQ,
		payload, instance_id_);
	if (!response || !SupplicantStaIfaceParseAddQosPolicyRequestForScsCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: add Qos policy req for scs fail");
		return {std::vector<QosPolicyScsRequestStatus>(),
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<QosPolicyScsRequestStatus>, ndk::ScopedAStatus>
StaIfaceRpc::removeQosPolicyForScsInternal(
	const std::vector<uint8_t>& scsPolicyIds)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaIfaceSerializeRemoveQosPolicyForScsReq(
		scsPolicyIds, payload)) {
		ALOGE("Serialize remove Qos policy for scs request fail");
		return {std::vector<QosPolicyScsRequestStatus>(),
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID)};
	}

	RemoveQosPolicyForScsCfmStaIfaceParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_IFACE_REMOVE_QOS_POLICY_FOR_SCS_REQ, payload,
		instance_id_);
	if (!response || !SupplicantStaIfaceParseRemoveQosPolicyForScsCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: remove Qos policy for scs fail");
		return {std::vector<QosPolicyScsRequestStatus>(),
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
