/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <android-base/logging.h>
#include <someip_common_def.h>
#include <supplicant_msg_common.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#ifdef CONFIG_RPC_CERTIFICATE
#include "supplicant_rpc_certificate.h"
#endif
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"
#include "supplicant_rpc_event.h"
#ifdef CONFIG_RPC_VENDOR_AIDL
#include "supplicant_rpc_vendor_event.h"
#endif

using aidl::android::hardware::wifi::supplicant::AidlRpcManager;
#ifdef CONFIG_RPC_CERTIFICATE
using aidl::android::hardware::wifi::supplicant::SupplicantRpcInitCertificateRequests;
#endif
using aidl::android::hardware::wifi::supplicant::SupplicantRpcInitEvents;
#ifdef CONFIG_RPC_VENDOR_AIDL
using aidl::vendor::qti::hardware::wifi::supplicant::SupplicantRpcInitVendorEvents;
#endif
using qti::hal::rpc::Someip;
using qti::hal::rpc::SomeipCallback;
using qti::hal::rpc::SomeipClient;
using qti::hal::rpc::SomeipContext;
using qti::hal::rpc::SomeipMessage;

#define SUPPLICANT_INSTANCE_ID		0x3330
#define SUPPLICANT_EVNTGROUP_ID		0xCCC0
#define SUPPLICANT_SOMEIP_CLIENT_NAME	"supplicant_someip_client"

static std::shared_ptr<SomeipClient> client;
static std::vector<uint16_t> events;
static std::map<uint16_t, EventDataHandler> event_handler_map;

static bool SupplicantRpcInitEventHandler()
{
	if (!SupplicantRpcInitEvents()) {
		ALOGE("Supplicant rpc init events fail");
		return false;
	}

#ifdef CONFIG_RPC_VENDOR_AIDL
	if (!SupplicantRpcInitVendorEvents()) {
		ALOGE("Supplicant rpc init vendor events fail");
		return false;
	}
#endif

#ifdef CONFIG_RPC_CERTIFICATE
	if (!SupplicantRpcInitCertificateRequests()) {
		ALOGE("Supplicant rpc init request handler fail");
		return false;
	}
#endif

	return true;
}

static void SupplicantRpcEventHandler(std::shared_ptr<SomeipMessage> event)
{
	EventDataHandler handler = nullptr;
	uint16_t methodId = event->getMethodId();
	auto item = event_handler_map.find(methodId);
	if (item != event_handler_map.end())
		handler = item->second;
	if (!handler) {
		ALOGE("Received unsupported event 0x%x", methodId);
		return;
	}

	ALOGD("Received rpc event 0x%x", methodId);

	handler(event->getData(), event->getLength());
}

static void SupplicantRpcMessageHandler(std::shared_ptr<SomeipMessage> message)
{
	if (!message)
		return;

	if (message->isEvent())
		SupplicantRpcEventHandler(message);
}

static void SupplicantRpcAvailabilityHandler(uint16_t service_id,
	uint16_t instance_id, bool available)
{
	ALOGI("RPC service [%04x:%04x] %s.", service_id, instance_id,
		available ? "available" : "unavailable");

	if (available) {
		AidlRpcManager* manager = AidlRpcManager::getInstance();
		if (!manager || manager->registerAidlService())
			ALOGE("RPC client register aidl interface fail");

#ifdef CONFIG_RPC_VENDOR_AIDL
		if (!manager || manager->registerVendorAidlService())
			ALOGE("RPC client register vendor aidl interface fail");
#endif
	} else {
		ALOGE("wpa_supplicant rpc connection lost.");
		AidlRpcManager::destroyInstance();
	}
}

static std::shared_ptr<SomeipMessage> SupplicantCreateRequest(
	uint16_t method_id, uint16_t staIfaceId, uint16_t networkId,
	const std::vector<uint8_t>& data)
{
	if (!client)
		return nullptr;

	auto request = std::make_shared<SomeipMessage>(
		SomeipMessage::createRequest(WIFI_SUPPLICANT_SERVICE_ID,
			SUPPLICANT_INSTANCE_ID, method_id,
			std::vector<uint8_t>(), true));
	if (request) {
		auto payload = request->createPayload(
			SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE + data.size());
		payload.push_back((uint8_t)(staIfaceId & 0xff));
		payload.push_back((uint8_t)((staIfaceId >> 8) & 0xff));
		payload.push_back((uint8_t)(networkId & 0xff));
		payload.push_back((uint8_t)((networkId >> 8) & 0xff));
		payload.insert(payload.end(), data.begin(), data.end());
		request->setPayload(payload);
	}

	return request;
}

bool SupplicantSomeipClientInit()
{
	SupplicantRpcInitEventHandler();

	SomeipCallback callback(&SupplicantRpcAvailabilityHandler,
		&SupplicantRpcMessageHandler);
	SomeipContext context(WIFI_SUPPLICANT_SERVICE_ID,
		SUPPLICANT_INSTANCE_ID, SUPPLICANT_EVNTGROUP_ID, events);

	Someip::setup(callback);
	client = std::make_shared<SomeipClient>(
		SUPPLICANT_SOMEIP_CLIENT_NAME, context);
	if (!client) {
		ALOGE("Supplicant rpc client init fail");
		Someip::destroy();
		return false;
	}

	return true;
}

void SupplicantSomeipClientDeinit()
{
	if (client) {
		client->deinit();
		client.reset();
		Someip::destroy();
	}

	events.clear();
	event_handler_map.clear();
}

bool SupplicantSomeipClientStart()
{
	if (!client)
		return false;

	return client->start(true);
}

void SupplicantSomeipClientStop()
{
	if (client)
		client->stop();
}

void SupplicantRegisterEventHandler(uint16_t id, EventDataHandler handler)
{
	events.push_back(id);
	event_handler_map[id] = handler;
}

void SupplicantSendRequest(uint16_t methodId,
	const std::vector<uint8_t>& data, uint16_t staIfaceId,
	uint16_t networkId)
{
	if (!client)
		return;

	auto request = SupplicantCreateRequest(methodId, staIfaceId,
		networkId, data);
	client->sendMessage(request);
}

std::shared_ptr<SomeipMessage> SupplicantSendRequestAndRecvReply(
	uint16_t methodId, const std::vector<uint8_t>& data,
	uint16_t staIfaceId, uint16_t networkId)
{
	if (!client)
		return nullptr;

	auto request = SupplicantCreateRequest(methodId, staIfaceId,
		networkId, data);
	return client->sendRequestAndWaitForReply(request);
}

ndk::ScopedAStatus SupplicantSendRequestAndRecvReplyStatus(
	uint16_t method_id, const std::vector<uint8_t>& data,
	uint16_t staIfaceId, uint16_t networkId)
{
	auto response = SupplicantSendRequestAndRecvReply(method_id,
		data, staIfaceId, networkId);
	if (!response) {
		ALOGE("No response message received for 0x%x", method_id);
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_INVALID);
	}

	HalStatusParam param;
	if (!SupplicantParseHalStatus(response->getData(),
		response->getLength(), param)) {
		ALOGE("Received invalid response status.");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	return SupplicantParam2NdkStatus(param);
}
