/*
 * WPA Supplicant - Sta Iface Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <common_util.h>
#include <supplicant_vendor_message_def.h>
#include <supplicant_vendor_sta_iface_msg.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"
#include "vendor_sta_iface_rpc.h"

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace supplicant {

using ::aidl::android::hardware::wifi::supplicant::AidlRpcManager;

VendorStaIfaceRpc::VendorStaIfaceRpc(const std::string& ifname,
	int32_t instance_id) : ifname_(ifname),
	instance_id_(instance_id), is_valid_(true) {}

void VendorStaIfaceRpc::invalidate() { is_valid_ = false; }
bool VendorStaIfaceRpc::isValid()
{
	return is_valid_;
}

::ndk::ScopedAStatus VendorStaIfaceRpc::registerSupplicantVendorStaIfaceCallback(
	const std::shared_ptr<ISupplicantVendorStaIfaceCallback>& in_callback)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&VendorStaIfaceRpc::registerSupplicantVendorStaIfaceCallbackInternal,
		in_callback);
}

::ndk::ScopedAStatus VendorStaIfaceRpc::doDriverCmd(
	const std::string& cmd, std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_UNKNOWN,
		&VendorStaIfaceRpc::doDriverCmdInternal, _aidl_return, cmd);
}

ndk::ScopedAStatus VendorStaIfaceRpc::registerSupplicantVendorStaIfaceCallbackInternal(
	const std::shared_ptr<ISupplicantVendorStaIfaceCallback> &callback)
{
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager ||
		manager->addVendorStaIfaceCallbackAidlObject(ifname_, callback)) {
		return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
	}
	return ndk::ScopedAStatus::ok();
}

std::pair<std::string, ndk::ScopedAStatus>
VendorStaIfaceRpc::doDriverCmdInternal(const std::string& cmd)
{
	std::vector<uint8_t> payload;
	if (!SupplicantVendorStaIfaceSerializeDoDriverCmdReq(cmd, payload)) {
                ALOGE("Serialize do driver cmd request fail");
		return {std::string(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID)};
        }

	ALOGD("doDriverCmd[%s] - '%s'\n", ifname_.c_str(), cmd.c_str());

	DoDriverCmdCfmParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_VENDOR_STA_IFACE_DO_DRIVER_CMD_REQ, payload,
		instance_id_);
	if (!response || !SupplicantVendorStaIfaceParseDoDriverCmdCfm(
		response->getData(), response->getLength(), param)) {
                ALOGE("Vendor Rpc: do driver cmd fail.");
		return {std::string(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNKNOWN)};
	}

        return {param.result, SupplicantParam2NdkStatus(param.status)};
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl
