/*
 * WPA Supplicant - Supplicant Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <android-base/file.h>
#include <fcntl.h>
#include <supplicant_vendor_msg.h>
#include <supplicant_vendor_message_def.h>
#include <sys/stat.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#include "supplicant_rpc.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"
#include "supplicant_vendor_rpc.h"
#ifdef CONFIG_RPC_P2P
#include "p2p_iface.h"
#endif

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace supplicant {
using android::hardware::wifi::supplicant::SupplicantStatusCode;
using android::hardware::wifi::supplicant::AidlRpcManager;
using android::hardware::wifi::supplicant::IfaceInfo;
using android::hardware::wifi::supplicant::IfaceType;

SupplicantVendorRpc::SupplicantVendorRpc() {}

bool SupplicantVendorRpc::isValid()
{
	// This top level object cannot be invalidated.
	return true;
}

::ndk::ScopedAStatus SupplicantVendorRpc::listVendorInterfaces(
	std::vector<IVendorIfaceInfo>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantVendorRpc::listVendorInterfacesInternal,
		_aidl_return);
}

::ndk::ScopedAStatus SupplicantVendorRpc::getVendorInterface(
	const IVendorIfaceInfo& info,
	std::shared_ptr<ISupplicantVendorStaIface>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
		&SupplicantVendorRpc::getVendorInterfaceInternal,
		_aidl_return, info);
}

std::pair<std::vector<IVendorIfaceInfo>, ndk::ScopedAStatus>
SupplicantVendorRpc::listVendorInterfacesInternal()
{
	ListVendorInterfacesCfmParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_VENDOR_LIST_VENDOR_INTERFACES_REQ);
	if (!response || !SupplicantVendorParseListVendorInterfacesCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("Vendor Rpc: list vendor ifaces fail.");
		return {std::vector<IVendorIfaceInfo>(),
			SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::shared_ptr<ISupplicantVendorStaIface>, ndk::ScopedAStatus>
SupplicantVendorRpc::getVendorInterfaceInternal(const IVendorIfaceInfo& info)
{
	AidlRpcManager* manager = AidlRpcManager::getInstance();
	std::shared_ptr<ISupplicantVendorStaIface> iface;
	if (!manager ||
		manager->getVendorStaIfaceAidlObjectByIfname(
			info.name, &iface)) {
		return {iface, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNKNOWN)};
	}
	return {iface, ndk::ScopedAStatus::ok()};
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl
