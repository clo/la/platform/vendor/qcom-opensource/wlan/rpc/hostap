/*
 * WPA Supplicant - Sta network Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <common_util.h>
#include <supplicant_message_def.h>
#include <supplicant_sta_network_msg.h>
#include <utils/Log.h>

#include "aidl_rpc_manager.h"
#include "sta_iface_rpc.h"
#include "sta_network_rpc.h"
#include "supplicant_rpc_client.h"
#include "supplicant_rpc_common.h"

namespace {
using aidl::android::hardware::wifi::supplicant::AuthAlgMask;
using aidl::android::hardware::wifi::supplicant::EapMethod;
using aidl::android::hardware::wifi::supplicant::EapPhase2Method;
using aidl::android::hardware::wifi::supplicant::GroupCipherMask;
using aidl::android::hardware::wifi::supplicant::GroupMgmtCipherMask;
using aidl::android::hardware::wifi::supplicant::KeyMgmtMask;
using aidl::android::hardware::wifi::supplicant::PairwiseCipherMask;
using aidl::android::hardware::wifi::supplicant::ProtoMask;

constexpr uint32_t kAllowedKeyMgmtMask =
	(static_cast<uint32_t>(KeyMgmtMask::NONE) |
	 static_cast<uint32_t>(KeyMgmtMask::WPA_PSK) |
	 static_cast<uint32_t>(KeyMgmtMask::WPA_EAP) |
	 static_cast<uint32_t>(KeyMgmtMask::IEEE8021X) |
	 static_cast<uint32_t>(KeyMgmtMask::FT_EAP) |
	 static_cast<uint32_t>(KeyMgmtMask::FT_PSK) |
	 static_cast<uint32_t>(KeyMgmtMask::OSEN) |
	 static_cast<uint32_t>(KeyMgmtMask::SAE) |
	 static_cast<uint32_t>(KeyMgmtMask::SUITE_B_192) |
	 static_cast<uint32_t>(KeyMgmtMask::OWE) |
	 static_cast<uint32_t>(KeyMgmtMask::WPA_PSK_SHA256) |
	 static_cast<uint32_t>(KeyMgmtMask::WPA_EAP_SHA256) |
	 static_cast<uint32_t>(KeyMgmtMask::WAPI_PSK) |
	 static_cast<uint32_t>(KeyMgmtMask::WAPI_CERT) |
	 static_cast<uint32_t>(KeyMgmtMask::FILS_SHA256) |
	 static_cast<uint32_t>(KeyMgmtMask::FILS_SHA384) |
	 static_cast<uint32_t>(KeyMgmtMask::DPP));
constexpr uint32_t kAllowedProtoMask =
	(static_cast<uint32_t>(ProtoMask::WPA) |
	 static_cast<uint32_t>(ProtoMask::RSN) |
	 static_cast<uint32_t>(ProtoMask::OSEN) |
	 static_cast<uint32_t>(ProtoMask::WAPI));
constexpr uint32_t kAllowedAuthAlgMask =
	(static_cast<uint32_t>(AuthAlgMask::OPEN) |
	 static_cast<uint32_t>(AuthAlgMask::SHARED) |
	 static_cast<uint32_t>(AuthAlgMask::LEAP) |
	 static_cast<uint32_t>(AuthAlgMask::SAE));
constexpr uint32_t kAllowedGroupCipherMask =
	(static_cast<uint32_t>(GroupCipherMask::WEP40) |
	 static_cast<uint32_t>(GroupCipherMask::WEP104) |
	 static_cast<uint32_t>(GroupCipherMask::TKIP) |
	 static_cast<uint32_t>(GroupCipherMask::CCMP) |
	 static_cast<uint32_t>(
	 GroupCipherMask::GTK_NOT_USED) |
	 static_cast<uint32_t>(GroupCipherMask::GCMP_256) |
	 static_cast<uint32_t>(GroupCipherMask::SMS4) |
	 static_cast<uint32_t>(GroupCipherMask::GCMP_128));
constexpr uint32_t kAllowedPairwisewCipherMask =
	(static_cast<uint32_t>(PairwiseCipherMask::NONE) |
	 static_cast<uint32_t>(PairwiseCipherMask::TKIP) |
	 static_cast<uint32_t>(PairwiseCipherMask::CCMP) |
	 static_cast<uint32_t>(
	 PairwiseCipherMask::GCMP_256) |
	 static_cast<uint32_t>(
	 PairwiseCipherMask::SMS4) |
	 static_cast<uint32_t>(PairwiseCipherMask::GCMP_128));
constexpr uint32_t kAllowedGroupMgmtCipherMask =
	(static_cast<uint32_t>(
			GroupMgmtCipherMask::BIP_GMAC_128) |
	 static_cast<uint32_t>(
			 GroupMgmtCipherMask::BIP_GMAC_256) |
	 static_cast<uint32_t>(
			 GroupMgmtCipherMask::BIP_CMAC_256));

constexpr uint32_t kEapMethodMax =
	static_cast<uint32_t>(EapMethod::WFA_UNAUTH_TLS) + 1;
constexpr uint32_t kEapPhase2MethodMax =
	static_cast<uint32_t>(EapPhase2Method::AKA_PRIME) + 1;

#ifdef CONFIG_RPC_WAPI_INTERFACE
std::string dummyWapiCertSuite;
std::vector<uint8_t> dummyWapiPsk;
#endif /* CONFIG_RPC_WAPI_INTERFACE */

}  // namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

StaNetworkRpc::StaNetworkRpc(const std::string& ifname, int32_t network_id)
	: ifname_(ifname), network_id_(network_id),
	  is_valid_(true), sta_iface_id_(-1)
{
	std::shared_ptr<StaIfaceRpc> iface = nullptr;
	AidlRpcManager *manager = AidlRpcManager::getInstance();
        if (manager)
		manager->getStaIfaceAidlObjectByIfname(ifname, &iface);
	if (iface)
		sta_iface_id_ = iface->getInstanceId();

	ALOGI("Sta iface: %s Instance ID: %d created network with ID %d",
		ifname.c_str(), sta_iface_id_, network_id_);
}

void StaNetworkRpc::invalidate()
{
	is_valid_ = false;
}

bool StaNetworkRpc::isValid()
{
	return is_valid_;
}

ndk::ScopedAStatus StaNetworkRpc::getId(int32_t* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getIdInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getInterfaceName(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getInterfaceNameInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getType(IfaceType* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getTypeInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::registerCallback(
	const std::shared_ptr<ISupplicantStaNetworkCallback>& in_callback)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::registerCallbackInternal, in_callback);
}

ndk::ScopedAStatus StaNetworkRpc::setSsid(const std::vector<uint8_t>& in_ssid)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setSsidInternal, in_ssid);
}

ndk::ScopedAStatus StaNetworkRpc::setBssid(const std::vector<uint8_t>& in_bssid)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setBssidInternal, in_bssid);
}

ndk::ScopedAStatus StaNetworkRpc::setDppKeys(const DppConnectionKeys& in_keys)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setDppKeysInternal, in_keys);
}

ndk::ScopedAStatus StaNetworkRpc::setScanSsid(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setScanSsidInternal, in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::setKeyMgmt(KeyMgmtMask in_keyMgmtMask)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setKeyMgmtInternal, in_keyMgmtMask);
}

ndk::ScopedAStatus StaNetworkRpc::setProto(ProtoMask in_protoMask)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setProtoInternal, in_protoMask);
}

ndk::ScopedAStatus StaNetworkRpc::setAuthAlg(AuthAlgMask in_authAlgMask)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setAuthAlgInternal, in_authAlgMask);
}

ndk::ScopedAStatus StaNetworkRpc::setGroupCipher(
	GroupCipherMask in_groupCipherMask)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setGroupCipherInternal, in_groupCipherMask);
}

ndk::ScopedAStatus StaNetworkRpc::setPairwiseCipher(
	PairwiseCipherMask in_pairwiseCipherMask)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setPairwiseCipherInternal,
		in_pairwiseCipherMask);
}

ndk::ScopedAStatus StaNetworkRpc::setPskPassphrase(const std::string& in_psk)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setPskPassphraseInternal, in_psk);
}

ndk::ScopedAStatus StaNetworkRpc::setPsk(const std::vector<uint8_t>& in_psk)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setPskInternal, in_psk);
}

ndk::ScopedAStatus StaNetworkRpc::setWepKey(
	int32_t in_keyIdx, const std::vector<uint8_t>& in_wepKey)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setWepKeyInternal, in_keyIdx, in_wepKey);
}

ndk::ScopedAStatus StaNetworkRpc::setWepTxKeyIdx(int32_t in_keyIdx)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setWepTxKeyIdxInternal, in_keyIdx);
}

ndk::ScopedAStatus StaNetworkRpc::setRequirePmf(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setRequirePmfInternal, in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::setEapMethod(EapMethod in_method)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapMethodInternal, in_method);
}

ndk::ScopedAStatus StaNetworkRpc::setEapPhase2Method(EapPhase2Method in_method)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapPhase2MethodInternal, in_method);
}

ndk::ScopedAStatus StaNetworkRpc::setEapIdentity(
	const std::vector<uint8_t>& in_identity)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapIdentityInternal, in_identity);
}

ndk::ScopedAStatus StaNetworkRpc::setEapEncryptedImsiIdentity(
	const std::vector<uint8_t>& in_identity)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapEncryptedImsiIdentityInternal,
		in_identity);
}

ndk::ScopedAStatus StaNetworkRpc::setStrictConservativePeerMode(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setStrictConservativePeerModeInternal,
		in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::setEapAnonymousIdentity(
	const std::vector<uint8_t>& in_identity)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapAnonymousIdentityInternal, in_identity);
}

ndk::ScopedAStatus StaNetworkRpc::setEapPassword(
	const std::vector<uint8_t>& in_password)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapPasswordInternal, in_password);
}

ndk::ScopedAStatus StaNetworkRpc::setEapCACert(const std::string& in_path)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapCACertInternal, in_path);
}

ndk::ScopedAStatus StaNetworkRpc::setEapCAPath(const std::string& in_path)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapCAPathInternal, in_path);
}

ndk::ScopedAStatus StaNetworkRpc::setEapClientCert(const std::string& in_path)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapClientCertInternal, in_path);
}

ndk::ScopedAStatus StaNetworkRpc::setEapPrivateKeyId(const std::string& in_id)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapPrivateKeyIdInternal, in_id);
}

ndk::ScopedAStatus StaNetworkRpc::setEapSubjectMatch(
	const std::string& in_match)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapSubjectMatchInternal, in_match);
}

ndk::ScopedAStatus StaNetworkRpc::setEapAltSubjectMatch(
	const std::string& in_match)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapAltSubjectMatchInternal, in_match);
}

ndk::ScopedAStatus StaNetworkRpc::setEapEngine(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapEngineInternal, in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::setEapEngineID(const std::string& in_id)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapEngineIDInternal, in_id);
}

ndk::ScopedAStatus StaNetworkRpc::setEapDomainSuffixMatch(
	const std::string& in_match)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapDomainSuffixMatchInternal, in_match);
}

ndk::ScopedAStatus StaNetworkRpc::setProactiveKeyCaching(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setProactiveKeyCachingInternal, in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::setIdStr(const std::string& in_idStr)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setIdStrInternal, in_idStr);
}

ndk::ScopedAStatus StaNetworkRpc::setUpdateIdentifier(int32_t in_id)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setUpdateIdentifierInternal, in_id);
}

ndk::ScopedAStatus StaNetworkRpc::setWapiCertSuite(const std::string& in_suite)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setWapiCertSuiteInternal, in_suite);
}

ndk::ScopedAStatus StaNetworkRpc::setEdmg(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEdmgInternal, in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::getSsid(std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getSsidInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getBssid(std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getBssidInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getScanSsid(
	bool* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getScanSsidInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getKeyMgmt(KeyMgmtMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getKeyMgmtInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getProto(ProtoMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getProtoInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getAuthAlg(AuthAlgMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getAuthAlgInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getGroupCipher(GroupCipherMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getGroupCipherInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getPairwiseCipher(
	PairwiseCipherMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getPairwiseCipherInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getPskPassphrase(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getPskPassphraseInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getPsk(std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getPskInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getSaePassword(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getSaePasswordInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getSaePasswordId(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getSaePasswordIdInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getWepKey(
	int32_t in_keyIdx, std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getWepKeyInternal, _aidl_return, in_keyIdx);
}

ndk::ScopedAStatus StaNetworkRpc::getWepTxKeyIdx(int32_t* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getWepTxKeyIdxInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getRequirePmf(bool* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getRequirePmfInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapMethod(EapMethod* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapMethodInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapPhase2Method(
	EapPhase2Method* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapPhase2MethodInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapIdentity(
	std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapIdentityInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapAnonymousIdentity(
	std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapAnonymousIdentityInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapPassword(
	std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapPasswordInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapCACert(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapCACertInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapCAPath(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapCAPathInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapClientCert(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapClientCertInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapPrivateKeyId(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapPrivateKeyIdInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapSubjectMatch(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapSubjectMatchInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapAltSubjectMatch(
	std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapAltSubjectMatchInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapEngine(bool* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapEngineInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapEngineId(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapEngineIdInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEapDomainSuffixMatch(
	std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEapDomainSuffixMatchInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getIdStr(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getIdStrInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getWpsNfcConfigurationToken(
	std::vector<uint8_t>* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getWpsNfcConfigurationTokenInternal,
		_aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getWapiCertSuite(std::string* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getWapiCertSuiteInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::getEdmg(bool* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getEdmgInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::enable(bool in_noConnect)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::enableInternal, in_noConnect);
}

ndk::ScopedAStatus StaNetworkRpc::disable()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::disableInternal);
}

ndk::ScopedAStatus StaNetworkRpc::select()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::selectInternal);
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimGsmAuthResponse(
	const std::vector<NetworkResponseEapSimGsmAuthParams>& in_params)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::sendNetworkEapSimGsmAuthResponseInternal,
		in_params);
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimGsmAuthFailure()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::sendNetworkEapSimGsmAuthFailureInternal);
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimUmtsAuthResponse(
	const NetworkResponseEapSimUmtsAuthParams& in_params)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::sendNetworkEapSimUmtsAuthResponseInternal,
		in_params);
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimUmtsAutsResponse(
	const std::vector<uint8_t>& in_auts)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::sendNetworkEapSimUmtsAutsResponseInternal,
		in_auts);
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimUmtsAuthFailure()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::sendNetworkEapSimUmtsAuthFailureInternal);
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapIdentityResponse(
	const std::vector<uint8_t>& in_identity,
	const std::vector<uint8_t>& in_encryptedIdentity)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::sendNetworkEapIdentityResponseInternal,
		in_identity, in_encryptedIdentity);
}

ndk::ScopedAStatus StaNetworkRpc::setGroupMgmtCipher(
	GroupMgmtCipherMask in_groupMgmtCipherMask)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setGroupMgmtCipherInternal,
		in_groupMgmtCipherMask);
}

ndk::ScopedAStatus StaNetworkRpc::getGroupMgmtCipher(
	GroupMgmtCipherMask* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getGroupMgmtCipherInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::enableTlsSuiteBEapPhase1Param(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::enableTlsSuiteBEapPhase1ParamInternal,
		in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::enableSuiteBEapOpenSslCiphers()
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::enableSuiteBEapOpenSslCiphersInternal);
}

ndk::ScopedAStatus StaNetworkRpc::setSaePassword(
	const std::string& in_saePassword)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setSaePasswordInternal, in_saePassword);
}

ndk::ScopedAStatus StaNetworkRpc::setSaePasswordId(
	const std::string& in_saePasswordId)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setSaePasswordIdInternal, in_saePasswordId);
}

ndk::ScopedAStatus StaNetworkRpc::setOcsp(OcspType in_ocspType)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setOcspInternal, in_ocspType);
}

ndk::ScopedAStatus StaNetworkRpc::getOcsp(OcspType* _aidl_return)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::getOcspInternal, _aidl_return);
}

ndk::ScopedAStatus StaNetworkRpc::setPmkCache(
	const std::vector<uint8_t>& in_serializedEntry)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setPmkCacheInternal, in_serializedEntry);
}

ndk::ScopedAStatus StaNetworkRpc::setEapErp(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setEapErpInternal, in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::setSaeH2eMode(SaeH2eMode in_mode)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setSaeH2eModeInternal, in_mode);
}

ndk::ScopedAStatus StaNetworkRpc::enableSaePkOnlyMode(bool in_enable)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::enableSaePkOnlyModeInternal, in_enable);
}

ndk::ScopedAStatus StaNetworkRpc::setRoamingConsortiumSelection(
	const std::vector<uint8_t>& in_selectedRcoi)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setRoamingConsortiumSelectionInternal,
		in_selectedRcoi);
}

ndk::ScopedAStatus StaNetworkRpc::setMinimumTlsVersionEapPhase1Param(
	TlsVersion in_tlsVersion)
{
	return ValidateAndCall(
		this, SupplicantStatusCode::FAILURE_NETWORK_INVALID,
		&StaNetworkRpc::setMinimumTlsVersionEapPhase1ParamInternal,
		in_tlsVersion);
}

std::pair<uint32_t, ndk::ScopedAStatus> StaNetworkRpc::getIdInternal()
{
	return {network_id_, ndk::ScopedAStatus::ok()};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getInterfaceNameInternal()
{
	return {ifname_, ndk::ScopedAStatus::ok()};
}

std::pair<IfaceType, ndk::ScopedAStatus> StaNetworkRpc::getTypeInternal()
{
	return {IfaceType::STA, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus StaNetworkRpc::registerCallbackInternal(
	const std::shared_ptr<ISupplicantStaNetworkCallback> &callback)
{
	AidlRpcManager *manager = AidlRpcManager::getInstance();
	if (!manager ||
		manager->addStaNetworkCallbackAidlObject(
			ifname_, network_id_, callback))
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_UNKNOWN);

	return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus StaNetworkRpc::setSsidInternal(
	const std::vector<uint8_t> &ssid)
{
	if (!ssid.size() ||
		ssid.size() > static_cast<uint32_t>(
			ISupplicantStaNetwork::SSID_MAX_LEN_IN_BYTES))
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetSsidReq(ssid, payload)) {
		ALOGE("Serialize set ssid request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_SSID_REQ, payload, sta_iface_id_,
		network_id_);
	if (!status.isOk())
		ALOGE("RPC: set ssid fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setBssidInternal(
	const std::vector<uint8_t> &bssid)
{
	if (bssid.size() != ETH_ALEN)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetBssidReq(bssid, payload)) {
		ALOGE("Serialize set bssid request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_BSSID_REQ, payload, sta_iface_id_,
		network_id_);
	if (!status.isOk())
		ALOGE("RPC: set bssid fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setDppKeysInternal(
	const DppConnectionKeys& keys)
{
#ifdef CONFIG_RPC_DPP
	if (keys.connector.empty() || keys.cSign.empty() ||
		keys.netAccessKey.empty())
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetDppKeysReq(keys, payload)) {
		ALOGE("Serialize set DPP Keys request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_DPP_KEYS_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set DPP Keys fail");

	return status;
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

ndk::ScopedAStatus StaNetworkRpc::setScanSsidInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetScanSsidReq(enable, payload)) {
		ALOGE("Serialize set scan ssid request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_SCAN_SSID_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set scan ssid fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setAuthAlgInternal(AuthAlgMask mask)
{
	if (static_cast<uint32_t>(mask) & ~kAllowedAuthAlgMask)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetAuthAlgReq(mask, payload)) {
		ALOGE("Serialize set auth alg request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_AUTH_ALG_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set auth alg fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEdmgInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEdmgReq(enable, payload)) {
		ALOGE("Serialize set edmg request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EDMG_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set edmg fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setPskPassphraseInternal(
	const std::string &rawPsk)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetPskPassphraseReq(
		rawPsk, payload)) {
		ALOGE("Serialize set psk passphrase request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_PSK_PASSPHRASE_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set psk passphrase fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setPskInternal(
	const std::vector<uint8_t> &psk)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetPskReq(psk, payload)) {
		ALOGE("Serialize set psk request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_PSK_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set psk fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setWepKeyInternal(
	uint32_t key_idx, const std::vector<uint8_t> &wep_key)
{
	if (key_idx >= static_cast<uint32_t>(
		ISupplicantStaNetwork::WEP_KEYS_MAX_NUM))
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	if (wep_key.size() != static_cast<uint32_t>(
		ISupplicantStaNetwork::WEP40_KEY_LEN_IN_BYTES) &&
	    wep_key.size() != static_cast<uint32_t>(
		ISupplicantStaNetwork::WEP104_KEY_LEN_IN_BYTES))
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetWepKeyReq(
		key_idx, wep_key, payload)) {
		ALOGE("Serialize set wep key request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_WEP_KEY_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set psk fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setWepTxKeyIdxInternal(uint32_t key_idx)
{
	if (key_idx >= static_cast<uint32_t>(
		ISupplicantStaNetwork::WEP_KEYS_MAX_NUM))
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetWepTxKeyIdxReq(key_idx, payload)) {
		ALOGE("Serialize set wep tx key idx request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_WEP_TX_KEY_IDX_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set wep tx key idx fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setRequirePmfInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetRequirePmfReq(enable, payload)) {
		ALOGE("Serialize set require pmf request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_REQUIRE_PMF_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set require pmf fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapMethodInternal(
	EapMethod method)
{
	uint32_t eap_method_idx = static_cast<
		std::underlying_type<EapMethod>::type>(method);
	if (eap_method_idx >= kEapMethodMax)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapMethodReq(method, payload)) {
		ALOGE("Serialize set eap method request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_METHOD_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap method fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapPhase2MethodInternal(
	EapPhase2Method method)
{
	uint32_t eap_phase2_method_idx = static_cast<
		std::underlying_type<EapPhase2Method>::type>(method);
	if (eap_phase2_method_idx >= kEapPhase2MethodMax)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapPhase2MethodReq(
		method, payload)) {
		ALOGE("Serialize set eap phase2 method request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_PHASE2_METHOD_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap phase2 method fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapIdentityInternal(
	const std::vector<uint8_t> &identity)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapIdentityReq(
		identity, payload)) {
		ALOGE("Serialize set eap identity request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_IDENTITY_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap identity fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapEncryptedImsiIdentityInternal(
	const std::vector<uint8_t> &identity)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapEncryptedImsiIdentityReq(
		identity, payload)) {
		ALOGE("Serialize set eap encrypted imsi identity request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_ENCRYPTED_IMSI_IDENTITY_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap encrypted imsi identity fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setStrictConservativePeerModeInternal(
	bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetStrictConservativePeerModeReq(
		enable, payload)) {
		ALOGE("Serialize set strict conservative peer mode fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_STRICT_CONSERVATIVE_PEER_MODE_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set strict conservative peer mode fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapAnonymousIdentityInternal(
	const std::vector<uint8_t> &identity)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapAnonymousIdentityReq(
		identity, payload)) {
		ALOGE("Serialize set eap anonymous identity request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_ANONYMOUS_IDENTITY_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap anonymous identity fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapPasswordInternal(
	const std::vector<uint8_t> &password)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapPasswordReq(
		password, payload)) {
		ALOGE("Serialize set eap password request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_PASSWORD_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap password fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapCACertInternal(const std::string &path)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapCACertReq(path, payload)) {
		ALOGE("Serialize set eap CA cert request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_C_A_CERT_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap CA cert fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapCAPathInternal(const std::string &path)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapCAPathReq(path, payload)) {
		ALOGE("Serialize set eap CA path request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_C_A_PATH_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap CA path fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapClientCertInternal(
	const std::string &path)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapClientCertReq(path, payload)) {
		ALOGE("Serialize set eap client cert request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_CLIENT_CERT_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap client cert fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapPrivateKeyIdInternal(
	const std::string &id)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapPrivateKeyIdReq(id, payload)) {
		ALOGE("Serialize set eap private key id request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_PRIVATE_KEY_ID_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap private key id fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapSubjectMatchInternal(
	const std::string &match)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapSubjectMatchReq(
		match, payload)) {
		ALOGE("Serialize set eap subject match request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_SUBJECT_MATCH_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap subject match fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapAltSubjectMatchInternal(
	const std::string &match)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapAltSubjectMatchReq(
		match, payload)) {
		ALOGE("Serialize set eap alt ubject match request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_ALT_SUBJECT_MATCH_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap alt ubject match fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapEngineInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapEngineReq(enable, payload)) {
		ALOGE("Serialize set eap engine request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_ENGINE_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap engine fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapEngineIDInternal(const std::string &id)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapEngineIDReq(id, payload)) {
		ALOGE("Serialize set eap engine ID request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_ENGINE_I_D_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap engine ID fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapDomainSuffixMatchInternal(
	const std::string &match)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapDomainSuffixMatchReq(
		match, payload)) {
		ALOGE("Serialize set eap domain suffix match request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_DOMAIN_SUFFIX_MATCH_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap domain suffix match fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setProactiveKeyCachingInternal(bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetProactiveKeyCachingReq(
		enable, payload)) {
		ALOGE("Serialize set proactive key caching request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_PROACTIVE_KEY_CACHING_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set proactive key caching fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setIdStrInternal(const std::string &id_str)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetIdStrReq(id_str, payload)) {
		ALOGE("Serialize set id str request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_ID_STR_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set id str fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setUpdateIdentifierInternal(uint32_t id)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetUpdateIdentifierReq(id, payload)) {
		ALOGE("Serialize set update identifier request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_UPDATE_IDENTIFIER_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set update identifier fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setWapiCertSuiteInternal(
	const std::string &suite)
{
#ifdef CONFIG_RPC_WAPI_INTERFACE
	dummyWapiCertSuite = suite;
	return ndk::ScopedAStatus::ok();
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNKNOWN, "Not implemented");
#endif
}

ndk::ScopedAStatus StaNetworkRpc::setWapiPskInternal(
	const std::vector<uint8_t> &psk)
{
#ifdef CONFIG_RPC_WAPI_INTERFACE
	// Dummy implementation
	dummyWapiPsk = psk;
	return ndk::ScopedAStatus::ok();
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNKNOWN, "Not implemented");
#endif
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getSsidInternal()
{
	GetSsidCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_SSID_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetSsidCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get ssid fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getBssidInternal()
{
	GetBssidCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_BSSID_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetBssidCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get BSSID fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<bool, ndk::ScopedAStatus> StaNetworkRpc::getScanSsidInternal()
{
	GetScanSsidCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_SCAN_SSID_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetScanSsidCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get scan ssid fail");
		return {false, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<AuthAlgMask, ndk::ScopedAStatus>
StaNetworkRpc::getAuthAlgInternal()
{
	GetAuthAlgCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_AUTH_ALG_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetAuthAlgCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get auth alg fail");
		return {AuthAlgMask::OPEN, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getPskPassphraseInternal()
{
	GetPskPassphraseCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_PSK_PASSPHRASE_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetPskPassphraseCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get psk passphrase fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getPskInternal()
{
	GetPskCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_PSK_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetPskCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get psk fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getSaePasswordInternal()
{
	GetSaePasswordCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_SAE_PASSWORD_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetSaePasswordCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get sae password fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getSaePasswordIdInternal()
{
	GetSaePasswordIdCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_SAE_PASSWORD_ID_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetSaePasswordIdCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get sae password id fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getWepKeyInternal(uint32_t key_idx)
{
	GetWepKeyCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_WEP_KEY_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetWepKeyCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get wep key fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<uint32_t, ndk::ScopedAStatus> StaNetworkRpc::getWepTxKeyIdxInternal()
{
	GetWepTxKeyIdxCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_WEP_TX_KEY_IDX_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetWepTxKeyIdxCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get wep tx key idx fail");
		return {UINT32_MAX, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<bool, ndk::ScopedAStatus> StaNetworkRpc::getRequirePmfInternal()
{
	GetRequirePmfCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_REQUIRE_PMF_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetRequirePmfCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get require pmf fail");
		return {false, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<EapMethod, ndk::ScopedAStatus> StaNetworkRpc::getEapMethodInternal()
{
	GetEapMethodCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_METHOD_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapMethodCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap method fail");
		return {EapMethod::PEAP, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<EapPhase2Method, ndk::ScopedAStatus>
StaNetworkRpc::getEapPhase2MethodInternal()
{
	GetEapPhase2MethodCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_PHASE2_METHOD_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapPhase2MethodCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap phase2 method fail");
		return {EapPhase2Method::NONE, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getEapIdentityInternal()
{
	GetEapIdentityCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_IDENTITY_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapIdentityCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap identity fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getEapAnonymousIdentityInternal()
{
	GetEapAnonymousIdentityCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_ANONYMOUS_IDENTITY_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapAnonymousIdentityCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap anonymous identity fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getEapPasswordInternal()
{
	GetEapPasswordCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_PASSWORD_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapPasswordCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap password fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus> StaNetworkRpc::getEapCACertInternal()
{
	GetEapCACertCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_C_A_CERT_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapCACertCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap CA Cert fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus> StaNetworkRpc::getEapCAPathInternal()
{
	GetEapCAPathCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_C_A_PATH_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapCAPathCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap CA path fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getEapClientCertInternal()
{
	GetEapClientCertCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_CLIENT_CERT_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapClientCertCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap client cert fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getEapPrivateKeyIdInternal()
{
	GetEapPrivateKeyIdCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_PRIVATE_KEY_ID_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapPrivateKeyIdCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap private key id fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getEapSubjectMatchInternal()
{
	GetEapSubjectMatchCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_SUBJECT_MATCH_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapSubjectMatchCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap subject match fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getEapAltSubjectMatchInternal()
{
	GetEapAltSubjectMatchCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_ALT_SUBJECT_MATCH_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapAltSubjectMatchCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap alt subject match fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<bool, ndk::ScopedAStatus> StaNetworkRpc::getEapEngineInternal()
{
	GetEapEngineCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_ENGINE_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapEngineCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap engine fail");
		return {false, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getEapEngineIdInternal()
{
	GetEapEngineIdCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_ENGINE_ID_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapEngineIdCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap engine id fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getEapDomainSuffixMatchInternal()
{
	GetEapDomainSuffixMatchCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EAP_DOMAIN_SUFFIX_MATCH_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEapDomainSuffixMatchCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get eap domain suffix match fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus> StaNetworkRpc::getIdStrInternal()
{
	GetIdStrCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_ID_STR_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetIdStrCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get id str fail");
		return {"", SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<bool, ndk::ScopedAStatus> StaNetworkRpc::getEdmgInternal()
{
	GetEdmgCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_EDMG_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetEdmgCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get edmg fail");
		return {false, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getWpsNfcConfigurationTokenInternal()
{
	GetWpsNfcConfigurationTokenCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_WPS_NFC_CONFIGURATION_TOKEN_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response ||
		!SupplicantStaNetworkParseGetWpsNfcConfigurationTokenCfm(
			response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get WPS NFC configuration token fail");
		return {std::vector<uint8_t>(), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {std::move(param.result), SupplicantParam2NdkStatus(param.status)};
}

std::pair<std::string, ndk::ScopedAStatus>
StaNetworkRpc::getWapiCertSuiteInternal()
{
#ifdef CONFIG_RPC_WAPI_INTERFACE
	// Dummy implementation
	return {dummyWapiCertSuite, ndk::ScopedAStatus::ok()};
#else
	return {"",
		SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN)};
#endif
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
StaNetworkRpc::getWapiPskInternal()
{
#ifdef CONFIG_RPC_WAPI_INTERFACE
	// Dummy implementation
	return {dummyWapiPsk, ndk::ScopedAStatus::ok()};
#else
	return {std::vector<uint8_t>(),
		SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN)};
#endif
}

ndk::ScopedAStatus StaNetworkRpc::enableInternal(bool no_connect)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeEnableReq(no_connect, payload)) {
		ALOGE("Serialize enable request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_ENABLE_REQ, payload, sta_iface_id_,
		network_id_);
	if (!status.isOk())
		ALOGE("RPC: enable fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::disableInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_DISABLE_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: disable fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::selectInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SELECT_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: select fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimGsmAuthResponseInternal(
	const std::vector<NetworkResponseEapSimGsmAuthParams> &vec_params)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSendNetworkEapSimGsmAuthResponseReq(
		vec_params, payload)) {
		ALOGE("Serialize send network eap sim gsm auth response fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SEND_NETWORK_EAP_SIM_GSM_AUTH_RESPONSE_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: send network eap sim gsm auth response fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimGsmAuthFailureInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SEND_NETWORK_EAP_SIM_GSM_AUTH_FAILURE_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: send network eap sim gsm auth failure fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimUmtsAuthResponseInternal(
	const NetworkResponseEapSimUmtsAuthParams &params)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSendNetworkEapSimUmtsAuthResponseReq(
		params, payload)) {
		 ALOGE("Serialize send network eap sim umts auth response fail");
		 return SupplicantCreateStatus(
			 SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SEND_NETWORK_EAP_SIM_UMTS_AUTH_RESPONSE_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: send network eap sim umts auth response fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimUmtsAutsResponseInternal(
	const std::vector<uint8_t> &auts)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSendNetworkEapSimUmtsAutsResponseReq(
		auts, payload)) {
		ALOGE("Serialize send network eap sim umts auts response fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SEND_NETWORK_EAP_SIM_UMTS_AUTS_RESPONSE_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: send network eap sim umts auts resp fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapSimUmtsAuthFailureInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SEND_NETWORK_EAP_SIM_UMTS_AUTH_FAILURE_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: send network eap sim umts auth failure fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::sendNetworkEapIdentityResponseInternal(
	const std::vector<uint8_t> &identity,
	const std::vector<uint8_t> &encrypted_imsi_identity)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSendNetworkEapIdentityResponseReq(
		identity, encrypted_imsi_identity, payload)) {
		ALOGE("Serialize send network eap identity response fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SEND_NETWORK_EAP_IDENTITY_RESPONSE_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: send network eap identity response fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::enableTlsSuiteBEapPhase1ParamInternal(
	bool enable)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeEnableTlsSuiteBEapPhase1ParamReq(
		enable, payload)) {
		ALOGE("Serialize enable tl suiteB eap phase1 param fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_ENABLE_TLS_SUITE_B_EAP_PHASE1_PARAM_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: enable tls suiteB eap phase1 param fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::enableSuiteBEapOpenSslCiphersInternal()
{
	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_ENABLE_SUITE_B_EAP_OPEN_SSL_CIPHERS_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: enable suiteB eap openssl ciphers fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setSaePasswordInternal(
	const std::string &sae_password)
{
	if (sae_password.length() < 1)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetSaePasswordReq(
		sae_password, payload)) {
		ALOGE("Serialize set sae password request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_SAE_PASSWORD_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set sae password fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setSaePasswordIdInternal(
	const std::string &sae_password_id)
{
	if (sae_password_id.length() < 1)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetSaePasswordIdReq(
		sae_password_id, payload)) {
		ALOGE("Serialize set sae password id request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_SAE_PASSWORD_ID_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set sae password id fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setGroupMgmtCipherInternal(
	GroupMgmtCipherMask mask)
{
	uint32_t group_mgmt_cipher_mask = static_cast<uint32_t>(mask);
	if (group_mgmt_cipher_mask & ~kAllowedGroupMgmtCipherMask)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetGroupMgmtCipherReq(
		mask, payload)) {
		ALOGE("Serialize set group mgmt cipher request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_GROUP_MGMT_CIPHER_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set group mgmt cipher fail");

	return status;
}

std::pair<GroupMgmtCipherMask, ndk::ScopedAStatus>
StaNetworkRpc::getGroupMgmtCipherInternal()
{
	GetGroupMgmtCipherCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_GROUP_MGMT_CIPHER_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetGroupMgmtCipherCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get group mgmt cipher fail");
		return {GroupMgmtCipherMask(0), SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaNetworkRpc::setOcspInternal(OcspType ocspType)
{
	if (ocspType < OcspType::NONE ||
		ocspType > OcspType::REQUIRE_ALL_CERTS_STATUS)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetOcspReq(ocspType, payload)) {
		ALOGE("Serialize set ocsp request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_OCSP_REQ, payload, sta_iface_id_,
		network_id_);
	if (!status.isOk())
		ALOGE("RPC: set ocsp fail");

	return status;
}

std::pair<OcspType, ndk::ScopedAStatus> StaNetworkRpc::getOcspInternal()
{
	GetOcspCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_OCSP_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetOcspCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get ocsp fail");
		return {OcspType::NONE, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaNetworkRpc::setPmkCacheInternal(
	const std::vector<uint8_t>& serializedEntry)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetPmkCacheReq(
		serializedEntry, payload)) {
		ALOGE("Serialize set pmk cache request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_PMK_CACHE_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set pmk cache fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setKeyMgmtInternal(KeyMgmtMask mask)
{
	if (static_cast<uint32_t>(mask) & ~kAllowedKeyMgmtMask)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetKeyMgmtReq(mask, payload)) {
		ALOGE("Serialize set key mgmt request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_KEY_MGMT_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set key mgmt fail");

	return status;
}

std::pair<KeyMgmtMask, ndk::ScopedAStatus> StaNetworkRpc::getKeyMgmtInternal()
{
	GetKeyMgmtCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_KEY_MGMT_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetKeyMgmtCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get key mgmt fail");
		return {KeyMgmtMask::WPA_EAP, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaNetworkRpc::setProtoInternal(ProtoMask mask)
{
	if (static_cast<uint32_t>(mask) & ~kAllowedProtoMask)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetProtoReq(mask, payload)) {
		ALOGE("Serialize set proto request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_PROTO_REQ, payload, sta_iface_id_,
		network_id_);
	if (!status.isOk())
		ALOGE("RPC: set proto fail");

	return status;
}

std::pair<ProtoMask, ndk::ScopedAStatus> StaNetworkRpc::getProtoInternal()
{
	GetProtoCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_PROTO_REQ, std::vector<uint8_t>(),
		sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetProtoCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get proto fail");
		return {ProtoMask::WPA, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaNetworkRpc::setGroupCipherInternal(GroupCipherMask mask)
{
	if (static_cast<uint32_t>(mask) & ~kAllowedGroupCipherMask)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetGroupCipherReq(mask, payload)) {
		ALOGE("Serialize set group cipher request fail");
		return SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_GROUP_CIPHER_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set group cipher fail");

	return status;
}

std::pair<GroupCipherMask, ndk::ScopedAStatus>
StaNetworkRpc::getGroupCipherInternal()
{
	GetGroupCipherCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_GROUP_CIPHER_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetGroupCipherCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get group cipher fail");
		return {GroupCipherMask::WEP40, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaNetworkRpc::setPairwiseCipherInternal(
	PairwiseCipherMask mask)
{
	if (static_cast<uint32_t>(mask) & ~kAllowedPairwisewCipherMask)
		return SupplicantCreateStatus(
				SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetPairwiseCipherReq(mask, payload)) {
		ALOGE("Serialize set pairwise cipher request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_PAIRWISE_CIPHER_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set pairwise cipher fail");

	return status;
}

std::pair<PairwiseCipherMask, ndk::ScopedAStatus>
StaNetworkRpc::getPairwiseCipherInternal()
{
	GetPairwiseCipherCfmStaNetworkParam param;
	auto response = SupplicantSendRequestAndRecvReply(
		SUPPLICANT_STA_NETWORK_GET_PAIRWISE_CIPHER_REQ,
		std::vector<uint8_t>(), sta_iface_id_, network_id_);
	if (!response || !SupplicantStaNetworkParseGetPairwiseCipherCfm(
		response->getData(), response->getLength(), param)) {
		ALOGE("RPC: get pairwise cipher fail");
		return {PairwiseCipherMask::NONE, SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_NETWORK_UNKNOWN)};
	}

	return {param.result, SupplicantParam2NdkStatus(param.status)};
}

ndk::ScopedAStatus StaNetworkRpc::setRoamingConsortiumSelectionInternal(
	const std::vector<uint8_t> &selectedRcoi)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetRoamingConsortiumSelectionReq(
		selectedRcoi, payload)) {
		ALOGE("Serialize set roaming consortium selection fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_ROAMING_CONSORTIUM_SELECTION_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set roaming consortium selection fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::setEapErpInternal(bool enable)
{
#ifdef CONFIG_RPC_FILS
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetEapErpReq(enable, payload)) {
		ALOGE("Serialize set eap erp request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_EAP_ERP_REQ, payload, sta_iface_id_,
		network_id_);
	if (!status.isOk())
		ALOGE("RPC: set eap erp fail");

	return status;
#else /* CONFIG_RPC_FILS */
	return SupplicantCreateStatus(SupplicantStatusCode::FAILURE_UNKNOWN);
#endif /* CONFIG_RPC_FILS */
}

ndk::ScopedAStatus StaNetworkRpc::setSaeH2eModeInternal(SaeH2eMode mode)
{
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetSaeH2eModeReq(mode, payload)) {
		ALOGE("Serialize set sae h2e mode request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_SAE_H2E_MODE_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set sae h2e mode fail");

	return status;
}

ndk::ScopedAStatus StaNetworkRpc::enableSaePkOnlyModeInternal(bool enable)
{
#ifdef CONFIG_RPC_SAE_PK
	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeEnableSaePkOnlyModeReq(
		enable, payload)) {
		ALOGE("Serialize enable sae pk only mode request fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_ENABLE_SAE_PK_ONLY_MODE_REQ, payload,
		sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: enable sae pk only mode fail");

	return status;
#else
	return SupplicantCreateStatus(
		SupplicantStatusCode::FAILURE_UNSUPPORTED);
#endif
}

ndk::ScopedAStatus StaNetworkRpc::setMinimumTlsVersionEapPhase1ParamInternal(
	TlsVersion tlsVersion)
{
	if (tlsVersion < TlsVersion::TLS_V1_0 ||
	    tlsVersion > TlsVersion::TLS_V1_3)
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);

	std::vector<uint8_t> payload;
	if (!SupplicantStaNetworkSerializeSetMinimumTlsVersionEapPhase1ParamReq(
		tlsVersion, payload)) {
		ALOGE("Serialize set minimum tls version eap phase1 param fail");
		return SupplicantCreateStatus(
			SupplicantStatusCode::FAILURE_ARGS_INVALID);
	}

	ndk::ScopedAStatus status = SupplicantSendRequestAndRecvReplyStatus(
		SUPPLICANT_STA_NETWORK_SET_MINIMUM_TLS_VERSION_EAP_PHASE1_PARAM_REQ,
		payload, sta_iface_id_, network_id_);
	if (!status.isOk())
		ALOGE("RPC: set minimum tls version eap phase1 param fail");

	return status;
}

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
