/*
 * WPA Supplicant - Supplicant Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef WPA_SUPPLICANT_VENDOR_RPC_H
#define WPA_SUPPLICANT_VENDOR_RPC_H

#include <aidl/vendor/qti/hardware/wifi/supplicant/BnSupplicantVendor.h>
#include <aidl/vendor/qti/hardware/wifi/supplicant/ISupplicantVendorStaIface.h>
#include <aidl/vendor/qti/hardware/wifi/supplicant/IVendorIfaceInfo.h>
#include <android-base/macros.h>

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace supplicant {

/**
 * Implementation of the supplicant aidl object. This aidl
 * object is used core for global control operations on
 * wpa_supplicant.
 */
class SupplicantVendorRpc : public BnSupplicantVendor
{
public:
	SupplicantVendorRpc();
	~SupplicantVendorRpc() override = default;
	bool isValid();

	// Aidl methods exposed.
	::ndk::ScopedAStatus listVendorInterfaces(
		std::vector<IVendorIfaceInfo>* _aidl_return) override;
	::ndk::ScopedAStatus getVendorInterface(
		const IVendorIfaceInfo& info,
		std::shared_ptr<ISupplicantVendorStaIface>* _aidl_return) override;

private:
	// Corresponding worker functions for the AIDL methods.
	std::pair<std::vector<IVendorIfaceInfo>, ndk::ScopedAStatus>
		listVendorInterfacesInternal();
	std::pair<std::shared_ptr<ISupplicantVendorStaIface>, ndk::ScopedAStatus>
		getVendorInterfaceInternal(const IVendorIfaceInfo& info);

	// Driver name to be used for creating interfaces.
	static const char kDriverName[];
	// wpa_supplicant.conf file location on the device.
	static const char kConfigFilePath[];

	DISALLOW_COPY_AND_ASSIGN(SupplicantVendorRpc);
};

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl

#endif  // WPA_SUPPLICANT_VENDOR_RPC_H
