/*
 * WPA Supplicant - Sta Iface Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef WPA_SUPPLICANT_VENDOR_STA_IFACE_RPC_H
#define WPA_SUPPLICANT_VENDOR_STA_IFACE_RPC_H

#include <android-base/macros.h>
#include <array>
#include <vector>

#include <aidl/vendor/qti/hardware/wifi/supplicant/BnSupplicantVendorStaIface.h>
#include <aidl/vendor/qti/hardware/wifi/supplicant/ISupplicantVendorStaIfaceCallback.h>

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace supplicant {

/**
 * Implementation of StaIface aidl object. Each unique aidl
 * object is used for control operations on a specific interface
 * controlled by wpa_supplicant.
 */
class VendorStaIfaceRpc : public BnSupplicantVendorStaIface
{
public:
	VendorStaIfaceRpc(const std::string& ifname, int32_t instance_id);
	~VendorStaIfaceRpc() override = default;
	// AIDL does not provide a built-in mechanism to let the server
	// invalidate a AIDL interface object after creation. If any client
	// process holds onto a reference to the object in their context,
	// any method calls on that reference will continue to be directed to
	// the server.
	// However Supplicant HAL needs to control the lifetime of these
	// objects. So, add a public |invalidate| method to all |Iface| and
	// |Network| objects.
	// This will be used to mark an object invalid when the corresponding
	// iface or network is removed.
	// All AIDL method implementations should check if the object is still
	// marked valid before processing them.
	void invalidate();
	bool isValid();

	// Aidl methods exposed.
	::ndk::ScopedAStatus registerSupplicantVendorStaIfaceCallback(
		const std::shared_ptr<ISupplicantVendorStaIfaceCallback>&
			in_callback) override;
	::ndk::ScopedAStatus doDriverCmd(
		const std::string& cmd, std::string* _aidl_return) override;

private:
	// Corresponding worker functions for the AIDL methods.
	ndk::ScopedAStatus registerSupplicantVendorStaIfaceCallbackInternal(
		const std::shared_ptr<ISupplicantVendorStaIfaceCallback>&
			callback);
	std::pair<std::string, ndk::ScopedAStatus> doDriverCmdInternal(
		const std::string& cmd);

	// Name of the iface this aidl object controls
	const std::string ifname_;
	int32_t instance_id_;
	bool is_valid_;

	DISALLOW_COPY_AND_ASSIGN(VendorStaIfaceRpc);
};

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl

#endif  // WPA_SUPPLICANT_VENDOR_STA_IFACE_RPC_H
