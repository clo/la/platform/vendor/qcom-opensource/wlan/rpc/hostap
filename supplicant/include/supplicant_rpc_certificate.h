/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef WPA_SUPPLICANT_RPC_CERTIFICATE_H
#define WPA_SUPPLICANT_RPC_CERTIFICATE_H

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

bool SupplicantRpcInitCertificateRequests();

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl

#endif /* WPA_SUPPLICANT_RPC_CERTIFICATE_H */
