/*
 * WPA Supplicant - Supplicant Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef WPA_SUPPLICANT_RPC_H
#define WPA_SUPPLICANT_RPC_H

#include <aidl/android/hardware/wifi/supplicant/BnSupplicant.h>
#include <aidl/android/hardware/wifi/supplicant/DebugLevel.h>
#include <aidl/android/hardware/wifi/supplicant/IfaceInfo.h>
#include <aidl/android/hardware/wifi/supplicant/ISupplicantCallback.h>
#include <aidl/android/hardware/wifi/supplicant/ISupplicantP2pIface.h>
#include <aidl/android/hardware/wifi/supplicant/ISupplicantStaIface.h>

#include <android-base/macros.h>

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

/**
 * Implementation of the supplicant aidl object. This aidl
 * object is used core for global control operations on
 * wpa_supplicant.
 */
class SupplicantRpc : public BnSupplicant {
public:
	SupplicantRpc();
	~SupplicantRpc() override = default;
	bool isValid();

	// Aidl methods exposed.
	ndk::ScopedAStatus addP2pInterface(
		const std::string& in_name,
		std::shared_ptr<ISupplicantP2pIface>* _aidl_return) override;
	ndk::ScopedAStatus getP2pInterface(
		const std::string& in_name,
		std::shared_ptr<ISupplicantP2pIface>* _aidl_return) override;
	ndk::ScopedAStatus addStaInterface(
		const std::string& in_name,
		std::shared_ptr<ISupplicantStaIface>* _aidl_return) override;
	ndk::ScopedAStatus removeInterface(
		const IfaceInfo& in_ifaceInfo) override;
	ndk::ScopedAStatus getStaInterface(
		const std::string& in_name,
		std::shared_ptr<ISupplicantStaIface>* _aidl_return) override;
	ndk::ScopedAStatus listInterfaces(
		std::vector<IfaceInfo>* _aidl_return) override;
	ndk::ScopedAStatus registerCallback(
		const std::shared_ptr<ISupplicantCallback>& in_callback) override;
	ndk::ScopedAStatus registerNonStandardCertCallback(
		const std::shared_ptr<INonStandardCertCallback>& in_callback) override;
	ndk::ScopedAStatus setDebugParams(
		DebugLevel in_level, bool in_showTimestamp, bool in_showKeys) override;
	ndk::ScopedAStatus getDebugLevel(DebugLevel* _aidl_return) override;
	ndk::ScopedAStatus isDebugShowTimestampEnabled(bool* _aidl_return) override;
	ndk::ScopedAStatus isDebugShowKeysEnabled(bool* _aidl_return) override;
	ndk::ScopedAStatus setConcurrencyPriority(IfaceType in_type) override;
	ndk::ScopedAStatus terminate() override;

private:
	// Corresponding worker functions for the AIDL methods.
#ifdef CONFIG_RPC_P2P
	std::pair<std::shared_ptr<ISupplicantP2pIface>, ndk::ScopedAStatus>
		addP2pInterfaceInternal(const std::string& name);
	std::pair<std::shared_ptr<ISupplicantP2pIface>, ndk::ScopedAStatus>
		getP2pInterfaceInternal(const std::string& name);
#endif
	std::pair<std::shared_ptr<ISupplicantStaIface>, ndk::ScopedAStatus>
		addStaInterfaceInternal(const std::string& name);
	std::pair<std::shared_ptr<ISupplicantStaIface>, ndk::ScopedAStatus>
		getStaInterfaceInternal(const std::string& name);
	ndk::ScopedAStatus removeInterfaceInternal(const IfaceInfo& iface_info);
	std::pair<std::vector<IfaceInfo>, ndk::ScopedAStatus> listInterfacesInternal();
	ndk::ScopedAStatus registerCallbackInternal(
		const std::shared_ptr<ISupplicantCallback>& callback);
	ndk::ScopedAStatus registerNonStandardCertCallbackInternal(
		const std::shared_ptr<INonStandardCertCallback>& callback);
	ndk::ScopedAStatus setDebugParamsInternal(
		DebugLevel level, bool show_timestamp, bool show_keys);
	ndk::ScopedAStatus setConcurrencyPriorityInternal(IfaceType type);

	// wpa_supplicant.conf file location on the device.
	static const char kConfigFilePath[];

	DISALLOW_COPY_AND_ASSIGN(SupplicantRpc);
};

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl

#endif  // WPA_SUPPLICANT_RPC_H
