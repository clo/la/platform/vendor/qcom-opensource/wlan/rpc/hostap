/*
 * WPA Supplicant - Sta network Aidl interface
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef SUPPLICANT_RPC_COMMON_H
#define SUPPLICANT_RPC_COMMON_H

#include <aidl/android/hardware/wifi/supplicant/SupplicantStatusCode.h>
#include <android/binder_interface_utils.h>

using aidl::android::hardware::wifi::supplicant::SupplicantStatusCode;

#ifndef ETH_ALEN
#define ETH_ALEN	6
#endif

#define SUPPLICANT_MESSAGE_PAYLOAD_HEADER_SIZE	4

inline ndk::ScopedAStatus SupplicantCreateStatus(int32_t code,
	const char* description = NULL)
{
	//Success case
	if (!code)
		return ndk::ScopedAStatus::ok();

	//Fail case
	if (description)
		return ndk::ScopedAStatus::fromServiceSpecificErrorWithMessage(
			code, description);
	else
		return ndk::ScopedAStatus::fromServiceSpecificError(code);
}

inline ndk::ScopedAStatus SupplicantCreateStatus(SupplicantStatusCode code,
	const char* description = NULL)
{
	return SupplicantCreateStatus(static_cast<int32_t>(code), description);
}

#define SupplicantParam2NdkStatus(ParamStatus)	\
	SupplicantCreateStatus((ParamStatus).status, (ParamStatus).info.c_str())

template <typename ObjT, typename WorkFuncT, typename... Args>
ndk::ScopedAStatus ValidateAndCall(
	ObjT* obj, SupplicantStatusCode status_code_if_invalid,
	WorkFuncT&& work, Args&&... args)
{
	if (obj->isValid()) {
		return (obj->*work)(std::forward<Args>(args)...);
	} else {
		return SupplicantCreateStatus(status_code_if_invalid);
	}
}

template <typename ObjT, typename WorkFuncT, typename ReturnT, typename... Args>
ndk::ScopedAStatus ValidateAndCall(
	ObjT* obj, SupplicantStatusCode status_code_if_invalid,
	WorkFuncT&& work, ReturnT* ret_val, Args&&... args)
{
	if (obj->isValid()) {
		auto call_pair = (obj->*work)(std::forward<Args>(args)...);
		*ret_val = call_pair.first;
		return std::forward<ndk::ScopedAStatus>(call_pair.second);
	} else {
		return SupplicantCreateStatus(status_code_if_invalid);
	}
}

#endif /* SUPPLICANT_RPC_COMMON_H */
