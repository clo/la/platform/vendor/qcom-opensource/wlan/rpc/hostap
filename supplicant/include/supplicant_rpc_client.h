/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef SUPPLICANT_RPC_CLIENT_H
#define SUPPLICANT_RPC_CLIENT_H

#include <someip_client.h>

typedef std::function<void(uint8_t*, size_t)> EventDataHandler;

bool SupplicantSomeipClientInit();
void SupplicantSomeipClientDeinit();

bool SupplicantSomeipClientStart();
void SupplicantSomeipClientStop();

void SupplicantRegisterEventHandler(uint16_t id, EventDataHandler handler);

void SupplicantSendRequest(uint16_t methodId,
	const std::vector<uint8_t>& data = {},
	uint16_t staIfaceId = 0, uint16_t networkId = 0);

std::shared_ptr<qti::hal::rpc::SomeipMessage> SupplicantSendRequestAndRecvReply(
	uint16_t methodId, const std::vector<uint8_t>& data = {},
	uint16_t staIfaceId = 0, uint16_t networkId = 0);

ndk::ScopedAStatus SupplicantSendRequestAndRecvReplyStatus(
	uint16_t methodId, const std::vector<uint8_t>& data = {},
	uint16_t staIfaceId = 0, uint16_t networkId = 0);

#endif /* SUPPLICANT_RPC_CLIENT_H */
