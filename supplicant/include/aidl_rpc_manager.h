/*
 * WPA Supplicant - Manager for Aidl interface objects
 * Copyright (c) 2021, Google Inc. All rights reserved.
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef WPA_SUPPLICANT_AIDL_RPC_MANAGER_H
#define WPA_SUPPLICANT_AIDL_RPC_MANAGER_H

#include <map>
#include <string>

#ifdef CONFIG_RPC_P2P
#include <aidl/android/hardware/wifi/supplicant/ISupplicantP2pIfaceCallback.h>
#endif
#include <aidl/android/hardware/wifi/supplicant/ISupplicantStaIfaceCallback.h>
#include <aidl/android/hardware/wifi/supplicant/ISupplicantStaNetworkCallback.h>
#ifdef CONFIG_RPC_VENDOR_AIDL
#include <aidl/vendor/qti/hardware/wifi/supplicant/ISupplicantVendor.h>
#include <aidl/vendor/qti/hardware/wifi/supplicant/ISupplicantVendorStaIface.h>
#include <aidl/vendor/qti/hardware/wifi/supplicant/ISupplicantVendorStaIfaceCallback.h>
#endif

#ifdef CONFIG_RPC_P2P
#include "p2p_iface.h"
#include "p2p_network.h"
#endif
#include "sta_iface_rpc.h"
#include "sta_network_rpc.h"
#include "supplicant_rpc.h"
#ifdef CONFIG_RPC_VENDOR_AIDL
#include "supplicant_vendor_rpc.h"
#include "vendor_sta_iface_rpc.h"
#endif

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {

#ifdef CONFIG_RPC_VENDOR_AIDL
using aidl::vendor::qti::hardware::wifi::supplicant::ISupplicantVendorStaIfaceCallback;
using aidl::vendor::qti::hardware::wifi::supplicant::ISupplicantVendorStaIface;
using aidl::vendor::qti::hardware::wifi::supplicant::ISupplicantVendor;
using aidl::vendor::qti::hardware::wifi::supplicant::VendorStaIfaceRpc;
using aidl::vendor::qti::hardware::wifi::supplicant::SupplicantVendorRpc;
#endif

/**
 * AidlManager is responsible for managing the lifetime of all
 * aidl objects created by wpa_supplicant. This is a singleton
 * class which is created by the supplicant core and can be used
 * to get references to the aidl objects.
 */
class AidlRpcManager
{
public:
	static AidlRpcManager *getInstance();
	static void destroyInstance();

	int startRpcClient();
	void stopRpcClient();

	int registerAidlService();
	int registerInterface(IfaceType type, const std::string& ifname,
		int32_t iface_id);
	int unregisterInterface(IfaceType type, const std::string& ifname);
	int registerNetwork(IfaceType type, const std::string& ifname,
		int32_t network_id);
	int unregisterNetwork(IfaceType type, const std::string& ifname,
		int32_t network_id);
	int unregisterAllNetworkOnInterface(IfaceType type, const std::string& ifname);
	void callWithEachSupplicantCallback(
		const std::function<ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantCallback>)> &method);
	void callWithEachStaIfaceCallback(const std::string &ifname,
		const std::function<ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantStaIfaceCallback>)> &method);
	void callWithEachStaNetworkCallback(const std::string &name,
		const std::function<::ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantStaNetworkCallback>)> &method);
#ifdef CONFIG_RPC_P2P
	void callWithEachP2pIfaceCallback(const std::string &ifname,
		const std::function<ndk::ScopedAStatus(
			std::shared_ptr<ISupplicantP2pIfaceCallback>)> &method);
	int getP2pIfaceAidlObjectByIfname(
		const std::string &ifname,
			std::shared_ptr<ISupplicantP2pIface> *iface_object);
	int getP2pNetworkAidlObjectByIfnameAndNetworkId(
		const std::string &ifname, int network_id,
			std::shared_ptr<ISupplicantP2pNetwork> *network_object);
	int addP2pIfaceCallbackAidlObject(
		const std::string &ifname,
		const std::shared_ptr<ISupplicantP2pIfaceCallback> &callback);
#endif
	const std::string getStaIfaceNameByInstanceId(int32_t instance_id);
	const std::string getStaNetworkNameByInstanceId(int32_t sta_iface_id,
		int32_t network_id);
	int getStaIfaceAidlObjectByIfname(
		const std::string &ifname,
		std::shared_ptr<StaIfaceRpc> *iface_object);
	int getStaNetworkAidlObjectByIfnameAndNetworkId(
		const std::string &ifname, int network_id,
		std::shared_ptr<StaNetworkRpc> *network_object);
	int addSupplicantCallbackAidlObject(
		const std::shared_ptr<ISupplicantCallback> &callback);
	int addStaIfaceCallbackAidlObject(
		const std::string &ifname,
		const std::shared_ptr<ISupplicantStaIfaceCallback> &callback);
	int addStaNetworkCallbackAidlObject(
		const std::string &ifname, int network_id,
		const std::shared_ptr<ISupplicantStaNetworkCallback> &callback);
	int registerNonStandardCertCallbackAidlObject(
		const std::shared_ptr<INonStandardCertCallback> &callback);
#ifdef CONFIG_RPC_CERTIFICATE
	std::optional<std::vector<uint8_t>> getCertificate(const char* alias);
	std::optional<std::vector<std::string>> listAliases(const char *prefix);
#endif
#ifdef CONFIG_RPC_VENDOR_AIDL
	int registerVendorAidlService();
	int getVendorStaIfaceAidlObjectByIfname(
		const std::string &ifname,
		std::shared_ptr<ISupplicantVendorStaIface> *iface_object);
	int addVendorStaIfaceCallbackAidlObject(
		const std::string &ifname,
		const std::shared_ptr<ISupplicantVendorStaIfaceCallback> &callback);
	void callWithEachVendorStaIfaceCallback(
		const std::string &ifname,
		const std::function<ndk::ScopedAStatus(
		std::shared_ptr<ISupplicantVendorStaIfaceCallback>)> &method);
#endif

private:
	AidlRpcManager();
	~AidlRpcManager() = default;
	AidlRpcManager(const AidlRpcManager &) = default;
	AidlRpcManager &operator=(const AidlRpcManager &) = default;

#ifdef CONFIG_RPC_P2P
	struct wpa_supplicant *getTargetP2pIfaceForGroup(
		struct wpa_supplicant *wpa_s);
	void removeP2pIfaceCallbackAidlObject(
		const std::string &ifname,
		const std::shared_ptr<ISupplicantP2pIfaceCallback> &callback);
#endif
	void removeSupplicantCallbackAidlObject(
		const std::shared_ptr<ISupplicantCallback> &callback);
	void removeStaIfaceCallbackAidlObject(
		const std::string &ifname,
		const std::shared_ptr<ISupplicantStaIfaceCallback> &callback);
	void removeStaNetworkCallbackAidlObject(
		const std::string &ifname, int network_id,
		const std::shared_ptr<ISupplicantStaNetworkCallback> &callback);
#ifdef CONFIG_RPC_VENDOR_AIDL
	void removeVendorStaIfaceCallbackAidlObject(
		const std::string &ifname,
		const std::shared_ptr<ISupplicantVendorStaIfaceCallback> &callback);
	bool checkForVendorStaIfaceCallback(const std::string &ifname);
#endif

	// Singleton instance of this class.
	static AidlRpcManager *instance_;
	// Death notifier.
	AIBinder_DeathRecipient* death_notifier_;
	// The main aidl service object.
	std::shared_ptr<SupplicantRpc> supplicant_object_;
#ifdef CONFIG_RPC_P2P
	// Map of all the P2P interface specific aidl objects controlled by
	// wpa_supplicant. This map is keyed in by the corresponding
	// |ifname|.
	std::map<const std::string, std::shared_ptr<P2pIface>>
		p2p_iface_object_map_;
	// Map of all the P2P network specific aidl objects controlled by
	// wpa_supplicant. This map is keyed in by the corresponding
	// |ifname| & |network_id|.
	std::map<const std::string, std::shared_ptr<P2pNetwork>>
		p2p_network_object_map_;
	// Map of all the callbacks registered for P2P interface specific
	// aidl objects controlled by wpa_supplicant.  This map is keyed in by
	// the corresponding |ifname|.
	std::map<
		const std::string,
		std::vector<std::shared_ptr<ISupplicantP2pIfaceCallback>>>
		p2p_iface_callbacks_map_;
#endif
	// Map of all the STA interface specific aidl objects controlled by
	// wpa_supplicant. This map is keyed in by the corresponding
	// |ifname|.
	std::map<const std::string, std::shared_ptr<StaIfaceRpc>>
		sta_iface_object_map_;
	// Map of all the STA network specific aidl objects controlled by
	// wpa_supplicant. This map is keyed in by the corresponding
	// |ifname| & |network_id|.
	std::map<const std::string, std::shared_ptr<StaNetworkRpc>>
		sta_network_object_map_;
	// Callbacks registered for the main aidl service object.
	std::vector<std::shared_ptr<ISupplicantCallback>> supplicant_callbacks_;
	// Map of all the callbacks registered for STA interface specific
	// aidl objects controlled by wpa_supplicant.  This map is keyed in by
	// the corresponding |ifname|.
	std::map<
		const std::string,
		std::vector<std::shared_ptr<ISupplicantStaIfaceCallback>>>
		sta_iface_callbacks_map_;
	// Map of all the callbacks registered for STA network specific
	// aidl objects controlled by wpa_supplicant.  This map is keyed in by
	// the corresponding |ifname| & |network_id|.
	std::map<
		const std::string,
		std::vector<std::shared_ptr<ISupplicantStaNetworkCallback>>>
		sta_network_callbacks_map_;
	// NonStandardCertCallback registered by the client.
	std::shared_ptr<INonStandardCertCallback> non_standard_cert_callback_;

#ifdef CONFIG_RPC_VENDOR_AIDL
	std::shared_ptr<SupplicantVendorRpc> supplicantvendor_object_;
        // Map of all the STA interface specific aidl objects controlled by
        // wpa_supplicant. This map is keyed in by the corresponding
        // |ifname|.
	std::map<const std::string, std::shared_ptr<VendorStaIfaceRpc>>
		vendor_sta_iface_object_map_;
	std::map<const std::string,
		std::vector<std::shared_ptr<ISupplicantVendorStaIfaceCallback>>>
		vendor_sta_iface_callbacks_map_;
#endif
};

}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
#endif  // WPA_SUPPLICANT_AIDL_RPC_MANAGER_H
