/*
 * aidl interface for wpa_hostapd daemon
 * Copyright (c) 2004-2018, Jouni Malinen <j@w1.fi>
 * Copyright (c) 2004-2018, Roshan Pius <rpius@google.com>
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2022, 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <hostapd_vendor_message_def.h>
#include <hostapd_vendor_msg.h>
#include <utils/Log.h>

#include "hostapd_rpc_client.h"
#include "hostapd_rpc_vendor.h"

namespace {

// Method called by death_notifier_ on client death.
void onDeath(void* cookie) {
	ALOGI("Client died. Terminating...");

	HostapdSomeipClientStop();
	HostapdSomeipClientDeinit();

	raise(SIGTERM);
}

}  // namespace

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace hostapd {

HostapdVendorRpc::HostapdVendorRpc()
{
	death_notifier_ = AIBinder_DeathRecipient_new(onDeath);
}

bool HostapdVendorRpc::isValid()
{
	return true;
}

::ndk::ScopedAStatus HostapdVendorRpc::registerHostapdVendorCallback(
	const std::shared_ptr<IHostapdVendorCallback>& callback)
{
	return registerHostapdVendorCallbackInternal(callback);
}

::ndk::ScopedAStatus HostapdVendorRpc::listVendorInterfaces(
	std::vector<std::string>* _aidl_return)
{
	auto result = listVendorInterfacesInternal();
	*_aidl_return = result.first;
	return std::forward<::ndk::ScopedAStatus>(result.second);
}

::ndk::ScopedAStatus HostapdVendorRpc::doDriverCmd(
	const std::string& iface, const std::string& cmd,
	std::string* _aidl_return)
{
	auto result = doDriverCmdInternal(iface, cmd);
	*_aidl_return = result.first;
	return std::forward<::ndk::ScopedAStatus>(result.second);
}

void HostapdVendorRpc::callWithEachCallback(
	const std::function<ndk::ScopedAStatus(
		std::shared_ptr<IHostapdVendorCallback>)> &method)
{
	for (const auto& callback : callbacks_)
		if (!method(callback).isOk())
			ALOGE("Failed to invoke Vendor AIDL callback");
}

::ndk::ScopedAStatus HostapdVendorRpc::registerHostapdVendorCallbackInternal(
	const std::shared_ptr<IHostapdVendorCallback>& callback)
{
	binder_status_t status = AIBinder_linkToDeath(
		callback->asBinder().get(), death_notifier_, this /* cookie */);
	if (status != STATUS_OK) {
		ALOGE("Error registering for death notification for "
			"hostapd vendor callback object");
		return HostapdCreateStatus(HostapdStatusCode::FAILURE_UNKNOWN);
	}
	callbacks_.push_back(callback);
	return ndk::ScopedAStatus::ok();
}

std::pair<std::vector<std::string>, ndk::ScopedAStatus>
	HostapdVendorRpc::listVendorInterfacesInternal()
{
	ListVendorInterfacesCfmParam param;
	ndk::ScopedAStatus status = HostapdSendMessageAndRecvReplyResult(
		HOSTAPD_VENDOR_LIST_VENDOR_INTERFACES_REQ,
		std::vector<uint8_t>(), param,
		&HostapdVendorParseListVendorInterfacesCfm);
	if (!status.isOk()) {
		ALOGE("vendor rpc: list vendor ifaces fail.");
		return {std::vector<std::string>(),
			std::forward<::ndk::ScopedAStatus>(status)};
	}

	return {std::move(param.result),
		std::forward<::ndk::ScopedAStatus>(status)};
}

std::pair<std::string, ndk::ScopedAStatus>
HostapdVendorRpc::doDriverCmdInternal(const std::string& iface_name,
	const std::string& cmd)
{
	std::vector<uint8_t> payload;
	if (!HostapdVendorSerializeDoDriverCmdReq(iface_name, cmd, payload)) {
		ALOGE("Serialize do driver cmd request fail");
		return {"", HostapdCreateStatus(
			HostapdStatusCode::FAILURE_ARGS_INVALID)};
	}

	ALOGD("vendor rpc doDriverCmd[%s] - '%s'\n", iface_name.c_str(),
		cmd.c_str());

	DoDriverCmdCfmParam param;
	ndk::ScopedAStatus status = HostapdSendMessageAndRecvReplyResult(
		HOSTAPD_VENDOR_DO_DRIVER_CMD_REQ, payload, param,
		&HostapdVendorParseDoDriverCmdCfm);
	if (!status.isOk())
		ALOGE("Vendor Rpc: do driver cmd fail.");

	ALOGD("vendor rpc reply = %s", param.result.c_str());

	return {std::move(param.result),
		std::forward<::ndk::ScopedAStatus>(status)};
}

}  // namespace hostapd
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl
