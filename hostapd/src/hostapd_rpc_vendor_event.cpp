/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <hostapd_vendor_msg.h>
#include <hostapd_vendor_message_def.h>
#include <utils/Log.h>

#include "hostapd_rpc_client.h"
#include "hostapd_rpc_vendor.h"
#include "hostapd_rpc_vendor_event.h"

namespace {

using aidl::vendor::qti::hardware::wifi::hostapd::IHostapdVendorCallback;
using aidl::vendor::qti::hardware::wifi::hostapd::HostapdVendorRpc;
using aidl::vendor::qti::hardware::wifi::hostapd::VendorApInfo;

template <typename CallbackFuncT, typename... Args>
static void HostapdVendorInvokeCallback(CallbackFuncT&& cb_func, Args&&... args)
{
	auto hostapd_vendor_service = HostapdGetVendorAidlService();;
	if (!hostapd_vendor_service)
		return;

	const std::function<
		ndk::ScopedAStatus(std::shared_ptr<IHostapdVendorCallback>)
		> func = std::bind(cb_func, std::placeholders::_1,
			std::forward<Args>(args)...);

	hostapd_vendor_service->callWithEachCallback(func);
}

static void HostapdVendorCtrlEvent(const uint8_t* data, size_t length)
{
	OnCtrlEventIndParam param;
	if (!HostapdVendorParseOnCtrlEventInd(data, length, param)) {
		ALOGE("Parse vendor ctrl event fail");
		return;
	}

	HostapdVendorInvokeCallback(&IHostapdVendorCallback::onCtrlEvent,
		param.ifaceName, param.event_str);
}

static void HostapdVendorEventApInstanceInfoChanged(const uint8_t* data,
	size_t length)
{
	VendorApInfo param;
	if (!HostapdVendorParseOnApInstanceInfoChangedInd(
		data, length, param)) {
		ALOGE("Parse vendor ap instance info changed event fail");
		return;
	}

	HostapdVendorInvokeCallback(
		&IHostapdVendorCallback::onApInstanceInfoChanged, param);
}

static void HostapdVendorEventFailure(const uint8_t* data, size_t length)
{
	OnFailureIndParam param;
	if (!HostapdVendorParseOnFailureInd(data, length, param)) {
		ALOGE("Parse vendor failure event fail");
		return;
	}

	HostapdVendorInvokeCallback(&IHostapdVendorCallback::onFailure,
		param.ifname, param.instanceName);
}

} // root namespace

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace hostapd {

bool HostapdRpcInitVendorEvents()
{
        HostapdRegisterEventHandler(
		HOSTAPD_VENDOR_ON_CTRL_EVENT_IND,
		&HostapdVendorCtrlEvent);
        HostapdRegisterEventHandler(
		HOSTAPD_VENDOR_ON_AP_INSTANCE_INFO_CHANGED_IND,
		&HostapdVendorEventApInstanceInfoChanged);
        HostapdRegisterEventHandler(
		HOSTAPD_VENDOR_ON_FAILURE_IND,
                &HostapdVendorEventFailure);

        return true;
}

}  // namespace hostapd
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl
