/*
 * aidl interface for wpa_hostapd daemon
 * Copyright (c) 2004-2018, Jouni Malinen <j@w1.fi>
 * Copyright (c) 2004-2018, Roshan Pius <rpius@google.com>
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <hostapd_message_def.h>
#include <hostapd_msg.h>
#include <utils/Log.h>

#include "hostapd_rpc.h"
#include "hostapd_rpc_client.h"

namespace {

// Method called by death_notifier_ on client death.
void onDeath(void* cookie) {
	ALOGI("Client died. Terminating...");

	HostapdSomeipClientStop();
	HostapdSomeipClientDeinit();

	raise(SIGTERM);
}

}  // namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace hostapd {

HostapdRpc::HostapdRpc()
{
	death_notifier_ = AIBinder_DeathRecipient_new(onDeath);
}

::ndk::ScopedAStatus HostapdRpc::addAccessPoint(
	const IfaceParams& iface_params, const NetworkParams& nw_params)
{
	return addAccessPointInternal(iface_params, nw_params);
}

::ndk::ScopedAStatus HostapdRpc::removeAccessPoint(
	const std::string& iface_name)
{
	return removeAccessPointInternal(iface_name);
}

::ndk::ScopedAStatus HostapdRpc::terminate()
{
	ALOGI("Terminating...");

	// Clear the callback to avoid IPCThreadState shutdown during the
	// callback event.
	callbacks_.clear();

	HostapdSendMessageAndRecvReplyStatus(
		HOSTAPD_TERMINATE_REQ, std::vector<uint8_t>());

	onDeath(NULL);

	return ndk::ScopedAStatus::ok();
}

::ndk::ScopedAStatus HostapdRpc::registerCallback(
	const std::shared_ptr<IHostapdCallback>& callback)
{
	return registerCallbackInternal(callback);
}

::ndk::ScopedAStatus HostapdRpc::forceClientDisconnect(
	const std::string& iface_name,
	const std::vector<uint8_t>& client_address,
	Ieee80211ReasonCode reason_code)
{
	return forceClientDisconnectInternal(iface_name,
		client_address, reason_code);
}

::ndk::ScopedAStatus HostapdRpc::setDebugParams(DebugLevel level)
{
	ALOGD("RPC: hostapd: skip set debug params as remote will handle");
	return ndk::ScopedAStatus::ok();
}

void HostapdRpc::callWithEachCallback(
        const std::function<ndk::ScopedAStatus(
                std::shared_ptr<IHostapdCallback>)> &method)
{
	for (const auto& callback : callbacks_)
		if (!method(callback).isOk())
			ALOGE("Failed to invoke AIDL callback");
}

::ndk::ScopedAStatus HostapdRpc::addAccessPointInternal(
	const IfaceParams& iface_params,
	const NetworkParams& nw_params)
{
	std::vector<uint8_t> payload;
	if (!HostapdSerializeAddAccessPointReq(iface_params, nw_params,
		payload)) {
		ALOGE("Serialize add access pointer request fail");
		return HostapdCreateStatus(
			HostapdStatusCode::FAILURE_ARGS_INVALID);
	}

	return HostapdSendMessageAndRecvReplyStatus(
		HOSTAPD_ADD_ACCESS_POINT_REQ, payload);
}

::ndk::ScopedAStatus HostapdRpc::removeAccessPointInternal(
	const std::string& iface_name)
{
	std::vector<uint8_t> payload;
	if (HostapdSerializeRemoveAccessPointReq(iface_name, payload)) {
		if (!HostapdSendMessageAndRecvReplyStatus(
			HOSTAPD_REMOVE_ACCESS_POINT_REQ, payload).isOk())
			ALOGE("RPC remove ap fail");
	}

	return ndk::ScopedAStatus::ok();
}

::ndk::ScopedAStatus HostapdRpc::registerCallbackInternal(
	const std::shared_ptr<IHostapdCallback>& callback)
{
	binder_status_t status = AIBinder_linkToDeath(
		callback->asBinder().get(), death_notifier_, this /* cookie */);
	if (status != STATUS_OK) {
		ALOGE("Error registering for death notification for "
			"hostapd callback object");
		return HostapdCreateStatus(HostapdStatusCode::FAILURE_UNKNOWN);
	}
	callbacks_.push_back(callback);
	return ndk::ScopedAStatus::ok();
}

::ndk::ScopedAStatus HostapdRpc::forceClientDisconnectInternal(
	const std::string& iface_name,
	const std::vector<uint8_t>& client_address,
	Ieee80211ReasonCode reason_code)
{
	std::vector<uint8_t> payload;
	if (!HostapdSerializeForceClientDisconnectReq(iface_name,
		client_address, reason_code, payload)) {
		ALOGE("Serialize foce client disconnect request fail");
		return HostapdCreateStatus(
			HostapdStatusCode::FAILURE_ARGS_INVALID);
	}

	return HostapdSendMessageAndRecvReplyStatus(
		HOSTAPD_FORCE_CLIENT_DISCONNECT_REQ, payload);
}

::ndk::ScopedAStatus HostapdRpc::setDebugParamsInternal(DebugLevel level)
{
	return ndk::ScopedAStatus::ok();
}

}  // namespace hostapd
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
