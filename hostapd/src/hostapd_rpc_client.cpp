/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <android-base/logging.h>
#include <android/binder_process.h>
#include <android/binder_manager.h>
#include <hostapd_msg_common.h>
#include <someip_common_def.h>
#include <utils/Log.h>

#include "hostapd_rpc_client.h"
#include "hostapd_rpc_event.h"
#ifdef CONFIG_RPC_VENDOR_AIDL
#include "hostapd_rpc_vendor_event.h"
#endif

using aidl::android::hardware::wifi::hostapd::HostapdRpcInitEvents;
#ifdef CONFIG_RPC_VENDOR_AIDL
using aidl::vendor::qti::hardware::wifi::hostapd::HostapdRpcInitVendorEvents;
#endif
using qti::hal::rpc::Someip;
using qti::hal::rpc::SomeipCallback;
using qti::hal::rpc::SomeipClient;
using qti::hal::rpc::SomeipContext;
using qti::hal::rpc::SomeipMessage;

static uint16_t HOSTAPD_INSTANCE_ID_DEFAULT = 0x4440;
static uint16_t HOSTAPD_INSTANCE_ID_CEM = 0x4441;
static uint16_t instance_id;
static uint16_t HOSTAPD_EVNTGROUP_ID = 0xDDD0;
static const std::string HOSTAPD_SOMEIP_CLIENT_NAME =
	"hostapd_someip_client";
static const std::string HOSTAPD_CEM_SOMEIP_CLIENT_NAME =
	"hostapd_cem_someip_client";

static std::shared_ptr<SomeipClient> client;
static std::vector<uint16_t> events;
static std::map<uint16_t, EventDataHandler> event_handler_map;

static std::shared_ptr<HostapdRpc> hostapd_aidl_service;
#ifdef CONFIG_RPC_VENDOR_AIDL
static std::shared_ptr<HostapdVendorRpc> hostapd_vendor_aidl_service;
#endif

static bool HostapdSomeipInitEvents()
{
	if (!HostapdRpcInitEvents())
		return false;

#ifdef CONFIG_RPC_VENDOR_AIDL
	if (!HostapdRpcInitVendorEvents())
		return false;
#endif

	return true;
}

static bool HostapdRegisterAidlService()
{
	hostapd_aidl_service = ndk::SharedRefBase::make<HostapdRpc>();
	if (!hostapd_aidl_service)
		return false;

	ALOGD("Add service");
#ifdef CONFIG_HOSTAPD_RPC_CEM
	std::string instance = std::string() +
		HostapdRpc::descriptor + "/cem";
#else
	std::string instance = std::string() +
		HostapdRpc::descriptor + "/default";
#endif
	if (AServiceManager_addService(
		hostapd_aidl_service->asBinder().get(),
		instance.c_str()) != STATUS_OK) {
		hostapd_aidl_service.reset();
		return false;
	}

	return true;
}

#ifdef CONFIG_RPC_VENDOR_AIDL
static bool HostapdRegisterVendorAidlService()
{
	hostapd_vendor_aidl_service =
		ndk::SharedRefBase::make<HostapdVendorRpc>();
	if (!hostapd_vendor_aidl_service)
		return false;

	ALOGD("Add vendor service");
#ifdef CONFIG_HOSTAPD_RPC_CEM
	std::string vendor_instance = std::string() +
		HostapdVendorRpc::descriptor + "/cem";
#else
	std::string vendor_instance = std::string() +
		HostapdVendorRpc::descriptor + "/default";
#endif
	if (AServiceManager_addService(
		hostapd_vendor_aidl_service->asBinder().get(),
		vendor_instance.c_str()) != STATUS_OK) {
		hostapd_vendor_aidl_service.reset();
		return false;
	}

	return true;
}
#endif

static void HostapdRpcAvailabilityHandler(uint16_t service_id,
	uint16_t instance_id, bool available)
{
	ALOGI("RPC service [%04x:%04x] %s.", service_id, instance_id,
		available ? "available" : "unavailable");

	if (available) {
		if (!HostapdRegisterAidlService())
			ALOGE("Add hostapd aidl service fail.");
#ifdef CONFIG_RPC_VENDOR_AIDL
		if (!HostapdRegisterVendorAidlService())
			ALOGE("Add hostapd vendor aidl service fail.");
#endif
	} else {
		ALOGE("hostapd rpc connection lost.");

		HostapdSomeipClientStop();
		HostapdSomeipClientDeinit();

		if (hostapd_aidl_service)
			hostapd_aidl_service->terminate();
	}
}

static void HostapdRpcProcessEvent(std::shared_ptr<SomeipMessage> event)
{
	EventDataHandler handler = nullptr;
	uint16_t method_id = event->getMethodId();
	auto item = event_handler_map.find(method_id);
	if (item != event_handler_map.end())
		handler = item->second;
	if (!handler) {
		ALOGE("Received unsupported event 0x%x", method_id);
		return;
	}

	ALOGD("Received rpc event 0x%x", method_id);

	handler(event->getData(), event->getLength());
}

static void HostapdRpcMessageHandler(std::shared_ptr<SomeipMessage> message)
{
	if (!message || !message->isEvent())
		return;

	HostapdRpcProcessEvent(message);
}

ndk::ScopedAStatus HostapdCreateStatus(int32_t code, const char* description)
{
	/* Successful case */
	if (!code)
		return ndk::ScopedAStatus::ok();

	/* Fail case */
	if (description)
		return ndk::ScopedAStatus::fromServiceSpecificErrorWithMessage(
			code, description);
	else
		return ndk::ScopedAStatus::fromServiceSpecificError(code);
}

ndk::ScopedAStatus HostapdCreateStatus(HostapdStatusCode code,
	const char* description)
{
	return HostapdCreateStatus(static_cast<int32_t>(code), description);
}

static uint16_t getSomeipInstanceId()
{
#ifdef CONFIG_HOSTAPD_RPC_CEM
	return HOSTAPD_INSTANCE_ID_CEM;
#else
	return HOSTAPD_INSTANCE_ID_DEFAULT;
#endif
}

static std::string getSomeipAppName()
{
#ifdef CONFIG_HOSTAPD_RPC_CEM
	return HOSTAPD_CEM_SOMEIP_CLIENT_NAME;
#else
	return HOSTAPD_SOMEIP_CLIENT_NAME;
#endif
}

bool HostapdSomeipClientInit()
{
	HostapdSomeipInitEvents();

	SomeipCallback callback(&HostapdRpcAvailabilityHandler,
		&HostapdRpcMessageHandler);
	instance_id = getSomeipInstanceId();
	SomeipContext context(WIFI_HOSTAPD_SERVICE_ID,
		instance_id, HOSTAPD_EVNTGROUP_ID, events);

	Someip::setup(callback);
	client = std::make_shared<SomeipClient>(
		getSomeipAppName(), context);
	if (!client) {
		ALOGE("Hostapd rpc client[%s] init fail", getSomeipAppName().c_str());
		Someip::destroy();
		return false;
	}

	return true;
}

void HostapdSomeipClientDeinit()
{
	if (client) {
		client->deinit();
		client.reset();
		Someip::destroy();
	}

	events.clear();
	event_handler_map.clear();
}

bool HostapdSomeipClientStart()
{
	if (!client)
		return false;

	return client->start(true);
}

void HostapdSomeipClientStop()
{
	if (client)
		client->stop();
}

void HostapdRegisterEventHandler(uint16_t id, EventDataHandler handler)
{
	events.push_back(id);
	event_handler_map[id] = handler;
}

std::shared_ptr<HostapdRpc> HostapdGetAidlService()
{
	return hostapd_aidl_service;
}

#ifdef CONFIG_RPC_VENDOR_AIDL
std::shared_ptr<HostapdVendorRpc> HostapdGetVendorAidlService()
{
	return hostapd_vendor_aidl_service;
}
#endif

std::shared_ptr<SomeipMessage> HostapdSendRequestAndRecvReply(
	uint16_t method_id, const std::vector<uint8_t>& payload)
{
	if (!client)
		return nullptr;

	auto request = std::make_shared<SomeipMessage>(
		SomeipMessage::createRequest(WIFI_HOSTAPD_SERVICE_ID,
			instance_id, method_id, payload, true));
	return client->sendRequestAndWaitForReply(request);
}

ndk::ScopedAStatus HostapdSendMessageAndRecvReplyStatus(uint16_t method_id,
	const std::vector<uint8_t>& payload)
{
	auto resp = HostapdSendRequestAndRecvReply(method_id, payload);
	if (!resp)
		return HostapdCreateStatus(HostapdStatusCode::FAILURE_UNKNOWN);

	HalStatusParam param;
	if (!HostapdParseHalStatus(resp->getData(), resp->getLength(), param)) {
		ALOGE("Received invalid response status.");
		return HostapdCreateStatus(
			HostapdStatusCode::FAILURE_ARGS_INVALID);
	}

	return HostapdCreateStatus(param.status, param.info.c_str());
}
