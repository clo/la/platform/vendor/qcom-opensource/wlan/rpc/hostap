/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <hostapd_msg.h>
#include <hostapd_message_def.h>
#include <utils/Log.h>

#include "hostapd_rpc.h"
#include "hostapd_rpc_client.h"

namespace {

using aidl::android::hardware::wifi::hostapd::ApInfo;
using aidl::android::hardware::wifi::hostapd::ClientInfo;
using aidl::android::hardware::wifi::hostapd::IHostapdCallback;

template <typename CallbackFuncT, typename... Args>
static void HostapdInvokeCallback(CallbackFuncT&& cb_func, Args&&... args)
{
	auto hostapd_service = HostapdGetAidlService();
	if (!hostapd_service)
		return;

	const std::function<
		ndk::ScopedAStatus(std::shared_ptr<IHostapdCallback>)> func =
		std::bind(cb_func, std::placeholders::_1,
			std::forward<Args>(args)...);

	hostapd_service->callWithEachCallback(func);
}

static void HostapdEventApInstanceInfoChanged(const uint8_t* data,
	size_t length)
{
	ApInfo info;
	if (!HostapdParseOnApInstanceInfoChangedInd(data, length, info)) {
		ALOGE("Parse AP instance info changed event fail");
		return;
	}

	HostapdInvokeCallback(&IHostapdCallback::onApInstanceInfoChanged, info);
}

static void HostapdEventConnectedClientsChanged(const uint8_t* data,
	size_t length)
{
	ClientInfo info;
	if (!HostapdParseOnConnectedClientsChangedInd(data, length, info)) {
		ALOGE("Parse connected clients changed event fail");
		return;
	}

	HostapdInvokeCallback(
		&IHostapdCallback::onConnectedClientsChanged, info);
}

static void HostapdEventFailure(const uint8_t* data, size_t length)
{
	OnFailureIndParam param;
	if (!HostapdParseOnFailureInd(data, length, param)) {
		ALOGE("Parse failure event fail");
		return;
	}

	HostapdInvokeCallback(&IHostapdCallback::onFailure,
		param.ifaceName, param.instanceName);
}

}

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace hostapd {

bool HostapdRpcInitEvents()
{
	HostapdRegisterEventHandler(
		HOSTAPD_ON_AP_INSTANCE_INFO_CHANGED_IND,
		&HostapdEventApInstanceInfoChanged);
	HostapdRegisterEventHandler(
		HOSTAPD_ON_CONNECTED_CLIENTS_CHANGED_IND,
		&HostapdEventConnectedClientsChanged);
	HostapdRegisterEventHandler(
		HOSTAPD_ON_FAILURE_IND,
		&HostapdEventFailure);

	return true;
}

}  // namespace hostapd
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
