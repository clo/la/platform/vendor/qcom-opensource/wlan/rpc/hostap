/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <utils/Log.h>

#include "hostapd_rpc_api.h"
#include "hostapd_rpc_client.h"

bool HostapdRpcStartClient()
{
	ALOGI("Hostapd rpc client starting");

	if (!HostapdSomeipClientInit()) {
		ALOGE("Hostapd rpc client init fail.");
		return false;
	}

	if (!HostapdSomeipClientStart()) {
		ALOGE("Hostapd rpc client start fail.");
		HostapdSomeipClientDeinit();
		return false;
	}

	ALOGI("Hostapd rpc client started");
	return true;
}

void HostapdRpcStopClient()
{
	ALOGI("Hostapd rpc client stopping");

	HostapdSomeipClientStop();
	HostapdSomeipClientDeinit();

	ALOGI("Hostapd rpc client stopped");
}
