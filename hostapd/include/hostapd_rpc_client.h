/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef HOSTAPD_RPC_CLIENT_H
#define HOSTAPD_RPC_CLIENT_H

#include <aidl/android/hardware/wifi/hostapd/HostapdStatusCode.h>
#include <android/binder_interface_utils.h>
#include <someip_client.h>

#include "hostapd_rpc.h"
#ifdef CONFIG_RPC_VENDOR_AIDL
#include "hostapd_rpc_vendor.h"
#endif

using aidl::android::hardware::wifi::hostapd::HostapdRpc;
using aidl::android::hardware::wifi::hostapd::HostapdStatusCode;
#ifdef CONFIG_RPC_VENDOR_AIDL
using aidl::vendor::qti::hardware::wifi::hostapd::HostapdVendorRpc;
#endif

typedef std::function<void(uint8_t*, size_t)> EventDataHandler;

ndk::ScopedAStatus HostapdCreateStatus(int32_t code,
	const char* description = NULL);
ndk::ScopedAStatus HostapdCreateStatus(HostapdStatusCode code,
        const char* description = NULL);

bool HostapdSomeipClientInit();
void HostapdSomeipClientDeinit();

bool HostapdSomeipClientStart();
void HostapdSomeipClientStop();

void HostapdRegisterEventHandler(uint16_t id, EventDataHandler handler);

std::shared_ptr<HostapdRpc> HostapdGetAidlService();
#ifdef CONFIG_RPC_VENDOR_AIDL
std::shared_ptr<HostapdVendorRpc> HostapdGetVendorAidlService();
#endif

std::shared_ptr<qti::hal::rpc::SomeipMessage>
	HostapdSendRequestAndRecvReply(uint16_t method_id,
		const std::vector<uint8_t>& payload);

ndk::ScopedAStatus HostapdSendMessageAndRecvReplyStatus(uint16_t method_id,
	const std::vector<uint8_t>& payload);

template <typename ParamType>
ndk::ScopedAStatus HostapdSendMessageAndRecvReplyResult(uint16_t method_id,
	const std::vector<uint8_t>& payload, ParamType& param,
	bool (*Parser)(const uint8_t*, size_t, ParamType&))
{
	auto resp = HostapdSendRequestAndRecvReply(method_id, payload);
	if (!resp)
		return HostapdCreateStatus(HostapdStatusCode::FAILURE_UNKNOWN);

	if (!Parser || !Parser(resp->getData(), resp->getLength(), param))
		return HostapdCreateStatus(HostapdStatusCode::FAILURE_UNKNOWN);

	return HostapdCreateStatus(param.status.status,
		param.status.info.c_str());
}

#endif /* HOSTAPD_RPC_CLIENT_H */
