/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef HOSTAPD_RPC_API_H
#define HOSTAPD_RPC_API_H

/* exported APIs for hostapd daemon to start-stop rpc client */
bool HostapdRpcStartClient();
void HostapdRpcStopClient();

#endif
