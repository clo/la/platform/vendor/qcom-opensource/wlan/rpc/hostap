/*
 * aidl interface for wpa_hostapd daemon
 * Copyright (c) 2004-2018, Jouni Malinen <j@w1.fi>
 * Copyright (c) 2004-2018, Roshan Pius <rpius@google.com>
 *
 * This software may be distributed, used, and modified under the terms of
 * BSD license:
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name(s) of the above-listed copyright holder(s) nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2022, 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef HOSTAPD_RPC_VENDOR_H
#define HOSTAPD_RPC_VENDOR_H

#include <map>
#include <string>

#include <android-base/macros.h>

#include <aidl/vendor/qti/hardware/wifi/hostapd/BnHostapdVendor.h>
#include <aidl/vendor/qti/hardware/wifi/hostapd/IHostapdVendor.h>
#include <aidl/vendor/qti/hardware/wifi/hostapd/IHostapdVendorCallback.h>

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace hostapd {

/**
 * Implementation of the hostapd aidl object. This aidl
 * object is used core for global control operations on
 * hostapd.
 */
class HostapdVendorRpc : public BnHostapdVendor
{
public:
	HostapdVendorRpc();
	~HostapdVendorRpc() override = default;
	bool isValid();

	// Aidl methods exposed.
	::ndk::ScopedAStatus listVendorInterfaces(
		std::vector<std::string>* _aidl_return) override;
	::ndk::ScopedAStatus registerHostapdVendorCallback(
	        const std::shared_ptr<IHostapdVendorCallback>&
			callback) override;
	::ndk::ScopedAStatus doDriverCmd(
		const std::string& iface, const std::string& cmd,
		std::string* _aidl_return) override;

	void callWithEachCallback(const std::function<::ndk::ScopedAStatus(
		std::shared_ptr<IHostapdVendorCallback>)> &method);

private:
	// Corresponding worker functions for the AIDL methods.
	::ndk::ScopedAStatus registerHostapdVendorCallbackInternal(
		const std::shared_ptr<IHostapdVendorCallback>& callback);
	std::pair<std::vector<std::string>, ndk::ScopedAStatus>
		listVendorInterfacesInternal();
	std::pair<std::string, ndk::ScopedAStatus> doDriverCmdInternal(
		const std::string& iface, const std::string& cmd);

	// Callbacks registered.
	std::vector<std::shared_ptr<IHostapdVendorCallback>> callbacks_;
	// Death notifier.
	AIBinder_DeathRecipient* death_notifier_;
	DISALLOW_COPY_AND_ASSIGN(HostapdVendorRpc);
};

}  // namespace hostapd
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl

#endif /* HOSTAPD_RPC_VENDOR_H */
