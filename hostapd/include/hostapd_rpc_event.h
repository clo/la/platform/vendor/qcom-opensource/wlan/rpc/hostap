/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef HOSTAPD_RPC_EVENT_H
#define HOSTAPD_RPC_EVENT_H

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
namespace hostapd {

bool HostapdRpcInitEvents();

}  // namespace hostapd
}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl

#endif
